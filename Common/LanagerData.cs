﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Common
{
    public class LanagerData
    {
        public static List<Lanagers> GetLanagerList(string path, string lanagerType)
        {
            List<Lanagers> list = new List<Lanagers>();
            XDocument doc = XDocument.Load(path);
            foreach (XElement element in doc.Element("lanagers").Elements())
            {
                Lanagers l = new Lanagers();
                var key = element.Attribute("value").Value;
                var value = lanagerType == "chinese" ? key : element.Element(lanagerType).Value;
                if (key == "") continue;
                l.key = key;
                l.value = value;
                list.Add(l);
            }
            return list;
        }
        public static Dictionary<string, string> GetLanagerDictionary(string path, string lanagerType)
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            XDocument doc = XDocument.Load(path);
            foreach (XElement element in doc.Element("lanagers").Elements())
            {
                var key = element.Attribute("value").Value;
                var value = lanagerType == "chinese" ? key : element.Element(lanagerType).Value;
                if (key == "") continue;
                list.Add(key, value);
            }
            return list;
        }

    }
    public class Lanagers
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}
