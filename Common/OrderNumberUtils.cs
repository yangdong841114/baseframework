﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// </summary>
    public class OrderNumberUtils
    {
        public static object _lock = new object();


        /// <summary>
        /// 获取订单号
        /// </summary>
        /// <returns></returns>
        public static string GetRandomNumber(string str)
        {
            lock (_lock)
            {
                Random ran = new Random();
                return str + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ran.Next(1000, 9999).ToString();
            }
        }
    }
}
