﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 自定义异常，此异常为校验数据时抛出，不记录在日志中
    /// </summary>
    [Serializable]
    public class ValidateException : ApplicationException
    {
        public ValidateException() { }

        public ValidateException(string message) : base(message){ }

        public ValidateException(string message, Exception inner): base(message, inner){ }
    }
}
