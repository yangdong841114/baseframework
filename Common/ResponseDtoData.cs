﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 单个响应对象，默认为SUCCESS；
    /// </summary>
    public class ResponseDtoData : ResponseData
    {
        public ResponseDtoData()
        {
            base.status = "success";
        }

        public ResponseDtoData(string status)
        {
            base.status = status;
        }

        //单个DtoData极其子类
        public DtoData result { get; set; }
    }
}
