﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Library
{
    public class ModelTo
    {
        public static List<T2> CopyToList<T1, T2>(List<T1> source)
        {

            List<T2> t2List = new List<T2>();

            T2 model = default(T2);

            PropertyInfo[] pi = typeof(T2).GetProperties();

            PropertyInfo[] pi1 = typeof(T1).GetProperties();

            foreach (T1 t1Model in source)
            {

                model = Activator.CreateInstance<T2>();

                for (int i = 0; i < pi.Length; i++)
                {

                    pi[i].SetValue(model, pi1[i].GetValue(t1Model, null), null);

                }

                t2List.Add(model);

            }

            return t2List;

        }

        /// <summary>
        /// 实体类之间的相互转换T1转换成T2
        /// </summary>
        /// <typeparam name="T1">输入类</typeparam>
        /// <typeparam name="T2">输出类</typeparam>
        /// <param name="source">输出类的实体数据</param>
        /// <returns></returns>
        public static T2 CopyToModel<T1, T2>(T1 source)
        {

            T2 model = default(T2);

            PropertyInfo[] pi = typeof(T2).GetProperties();

            PropertyInfo[] pi1 = typeof(T1).GetProperties();


            model = Activator.CreateInstance<T2>();

            for (int i = 0; i < pi.Length; i++)
            {

                pi[i].SetValue(model, pi1[i].GetValue(source, null), null);

            }


            return model;

        }


        /// <summary>
        /// Json实体类之间的相互转换T1转换成T2
        /// </summary>
        /// <typeparam name="T1">输入类</typeparam>
        /// <typeparam name="T2">输出类</typeparam>
        /// <param name="source">输出类的实体数据</param>
        /// <returns></returns>
        public static T2 JsonToModel<T1, T2>(T1 source)
        {
           return  Newtonsoft.Json.JsonConvert.DeserializeObject<T2>(Newtonsoft.Json.JsonConvert.SerializeObject(source));
        }


    }
}
