﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.wyy.Response
{
    public class Video
    {
        /// <summary>
        /// 视频的名称
        /// </summary>
        public string videoName { get; set; }
        /// <summary>
        /// 视频的状态，10表示初始，20表示失败，30表示处理中，40表示成功，50表示屏蔽
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// 视频的描述信息
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 视频播放时长（单位：秒）
        /// </summary>
        public int duration { get; set; }
        /// <summary>
        /// 视频播放时长（单位：毫秒）
        /// </summary>
        public int durationMsec { get; set; }
        /// <summary>
        /// 原始视频的播放地址
        /// </summary>
        public string origUrl { get; set; }

        /// <summary>
        /// 视频封面截图URL地址
        /// </summary>
        public string snapshotUrl { get; set; }
    }
}
