﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class MTransferBLL : BaseBLL<MTransfer>, IMTransferBLL
    {

        private System.Type type = typeof(MTransfer);
        public IBaseDao dao { get; set; }

        public IParamSetBLL paramBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public IDataDictionaryBLL ddBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new MTransfer GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            MTransfer mb = (MTransfer)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new MTransfer GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            MTransfer mb = (MTransfer)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<MTransfer> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<MTransfer> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<MTransfer>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MTransfer)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<MTransfer> GetList(string sql)
        {
            List<MTransfer> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<MTransfer>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MTransfer)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public PageResult<MTransfer> GetListPage(MTransfer model)
        {
            PageResult<MTransfer> page = new PageResult<MTransfer>();
            string sql = "select t.*,row_number() over(order by t.id desc) rownumber from MTransfer t where 1=1 ";
            string countSql = "select count(1) from MTransfer t where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.fromUserId))
                {
                    param.Add(new DbParameterItem("fromUserid", ConstUtil.LIKE, model.fromUserId));
                }
                if (!ValidateUtils.CheckNull(model.toUserId))
                {
                    param.Add(new DbParameterItem("toUserid", ConstUtil.LIKE, model.toUserId));
                }
                if (!ValidateUtils.CheckIntZero(model.fromUid))
                {
                    param.Add(new DbParameterItem("fromUid", ConstUtil.EQ, model.fromUid));
                }
                if (!ValidateUtils.CheckIntZero(model.typeId))
                {
                    param.Add(new DbParameterItem("typeId", ConstUtil.EQ, model.typeId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<MTransfer> list = new List<MTransfer>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MTransfer)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public int SaveMTransfer(MTransfer model, Member current)
        {
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (ValidateUtils.CheckIntZero(model.typeId)) { throw new ValidateException("请选择转账类型"); }
            if (ValidateUtils.CheckDoubleZero(model.epoints)) { throw new ValidateException("请录入转账金额"); }

            DateTime now = DateTime.Now;

            //转出账户为当前登录账户
            model.fromUid = current.id;
            model.fromUserId = current.userId;
            model.fromUserName = current.userName;
            model.fee = 0;

            //默认已审核
            model.flag = 2;
            model.addTime = now;
            model.auditTime = model.addTime;
            model.auditUid = current.id;
            model.auditUser = current.userId;

            //转出会员金额账户
            MemberAccount act = accountBLL.GetModel(model.fromUid.Value);

            //流水帐
            LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
            fromLs.outlay = model.epoints;
            fromLs.addtime = now;
            fromLs.uid = model.fromUid;
            fromLs.userId = model.fromUserId;
            fromLs.tableName = "MTransfer";
            fromLs.addUid = current.id;
            fromLs.addUser = current.userId;

            LiuShuiZhang toLs = new LiuShuiZhang();   //转入流水
            toLs.income = model.epoints;
            toLs.addtime = now;
            toLs.tableName = "MTransfer";
            toLs.addUid = current.id;
            toLs.addUser = current.userId;
            
            string opName = "";

            //转账参数，最小金额、倍数
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes(ConstUtil.TRANSFER_MIN, ConstUtil.TRANSFER_BEI);
            double transferMin = Convert.ToDouble(param[ConstUtil.TRANSFER_MIN].paramValue); //转账金额最小值
            double transferBei = Convert.ToDouble(param[ConstUtil.TRANSFER_BEI].paramValue); //转账金额必须是xx的倍数

            if (model.epoints.Value < transferMin) { throw new ValidateException("转账金额必须>=" + transferMin); }
            if (model.epoints.Value % transferBei != 0) { throw new ValidateException("转账金额必须是" + transferMin + "的倍数"); }

            //自身奖金币转电子币
            if (model.typeId.Value == ConstUtil.TRANSFER_SF_JJ_TO_DZ)
            {
                if (model.epoints > act.agentJj.Value) { throw new ValidateException("奖金币余额不足"); }
                //转入账户为当前账户
                model.toUid = current.id;
                model.toUserId = current.userId;
                model.toUserName = current.userName;
                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_SF_JJ_TO_DZ))[ConstUtil.TRANSFER_SF_JJ_TO_DZ];
                fromLs.accountId = ConstUtil.JOURNAL_JJB;   //转出奖金币
                fromLs.abst = opName + "," + model.toUserId + "转出扣除";
                fromLs.last = act.agentJj.Value - model.epoints.Value;

                toLs.accountId = ConstUtil.JOURNAL_DZB;     //转入电子币
                toLs.abst = opName + "," + model.fromUserId + "转入增加";
                toLs.last = act.agentDz.Value + model.epoints.Value;

                //减少自身奖金币
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentJj = model.epoints;
                accountBLL.UpdateSub(sub);

                //增加自身电子币
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentDz = model.epoints;
                accountBLL.UpdateAdd(add);

            }
            //电子币转其他会员电子币
            else if (model.typeId.Value == ConstUtil.TRANSFER_OT_DZ_TO_DZ)
            {
                //转入账户为model.toUserId
                if (model.epoints > act.agentDz.Value) { throw new ValidateException("电子币余额不足"); }
                if (ValidateUtils.CheckNull(model.toUserId)) { throw new ValidateException("转入账户不能为空"); }
                Member mb = memberBLL.GetModelByUserId(model.toUserId);
                if (mb == null) { throw new ValidateException("转入账户不存在"); }
                if (mb.id.Value == model.fromUid.Value) { throw new ValidateException("不能转给自身"); }
                model.toUid = mb.id;
                model.toUserId = mb.userId;
                model.toUserName = mb.userName;
                //转入会员账户
                MemberAccount inact = accountBLL.GetModel(mb.id.Value);

                //流水
                opName = (ddBLL.GetDictionaryName(ConstUtil.TRANSFER_OT_DZ_TO_DZ))[ConstUtil.TRANSFER_OT_DZ_TO_DZ];
                fromLs.accountId = ConstUtil.JOURNAL_DZB;   //转出电子币
                fromLs.abst = opName + "," + model.toUserId + "转出扣除";
                fromLs.last = act.agentDz.Value - model.epoints.Value;

                toLs.accountId = ConstUtil.JOURNAL_DZB;     //转入电子币
                toLs.abst = opName + "," + model.fromUserId + "转入增加";
                toLs.last = inact.agentDz.Value + model.epoints.Value;

                //减少自身电子币
                MemberAccount sub = new MemberAccount();
                sub.id = model.fromUid;
                sub.agentDz = model.epoints;
                accountBLL.UpdateSub(sub);

                //增加转入会员电子币
                MemberAccount add = new MemberAccount();
                add.id = model.toUid;
                add.agentDz = model.epoints;
                accountBLL.UpdateAdd(add);
            }

            //保存转账记录
            object o = this.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);

            //流水补充
            fromLs.sourceId = newId;
            toLs.sourceId = newId;
            toLs.uid = model.toUid;
            toLs.userId = model.toUserId;
            //保存流水
            liushuiBLL.Save(fromLs);
            liushuiBLL.Save(toLs);

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = "转账：" + opName + ",转出会员：" + model.fromUserId + "，转入会员：" + model.toUserId;
            log.tableName = "MTransfer";
            log.recordName = "会员转账";
            this.SaveOperateLog(log);

            //是否需要短信通知
            MobileNotice bf = noticeBLL.GetModel(ConstUtil.MOBILE_NOTICE_TRANSFER);
            if (bf.flag == 1)
            {
                //发送给会员短信
                string messge = bf.msg + "" + model.epoints.ToString();
                noticeBLL.SendMessage(current.phone, messge);
            }
            return newId;
        }


        public DataTable GetTransferExcel()
        {
            string sql = "select d.name as typeId,t.fromUserId,t.fromUserName,t.toUserId,t.toUserName,t.epoints,t.addTime from MTransfer t left join DataDictionary d on d.id=t.typeId where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }

    }
}
