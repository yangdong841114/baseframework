﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class BaseSetBLL : IBaseSetBLL
    {

        private System.Type type = typeof(BaseSet);
        public IBaseDao dao { get; set; }

        public BaseSet GetModel()
        {
            string sql = "select * from BaseSet where id = 1";
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            BaseSet model = (BaseSet)ReflectionUtil.GetModel(type, row);
            return model;
        }

        public int SaveModel(BaseSet model)
        {
            BaseSet st = GetModel();
            if (st == null)
            {
                dao.Save(model);
            }
            else
            {
                model.id = st.id;
                dao.Update(model);
            }
            return 1;
        }

        public List<Banner> GetBannerList()
        {
            string sql = "select * from Banner";
            DataTable dt = dao.GetList(sql);
            List<Banner> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Banner>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Banner)ReflectionUtil.GetModel(typeof(Banner), row));
                }
            }
            return list;
        }
        public List<Banner> GetBannerList(int str, int end)
        {
            string sql = "select * from Banner where id>=" + str + " and id<=" + end;
            DataTable dt = dao.GetList(sql);
            List<Banner> list = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Banner>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Banner)ReflectionUtil.GetModel(typeof(Banner), row));
                }
            }
            return list;
        }
        public int SaveBanner(Banner bn)
        {
            if (ValidateUtils.CheckIntZero(bn.id)) { throw new ValidateException("广告图ID不能为空"); }
            if (ValidateUtils.CheckNull(bn.imgUrl)) { throw new ValidateException("图片路径不能为空"); }

            string sql = "select count(1) from Banner where id = " + bn.id;
            int count = dao.GetCount(sql);
            int c = 0;
            if (count > 0)
            {
                c = dao.Update(bn);
            }
            else
            {
                c = dao.Save(bn);
            }

            return c;
        }

        public int ClearDataBase()
        {
            return dao.ExecuteBySql(" exec clearData");
        }


        public string BackUpDb(string path)
        {
            DateTime now = DateTime.Now;
            string fileName = BackUpSystem.BackUp(path, now);
            BackModel model = new BackModel();
            model.addTime = now;
            model.fname = fileName;
            dao.Save(model);
            return fileName;
        }

        public bool deleteBak(int id, string path)
        {
            DataRow row = dao.GetOne("select * from BackModel where id = " + id);
            if (row == null) { throw new ValidateException("备份不存在"); }
            string xpath = path + "/" + row["fname"].ToString();
            dao.ExecuteBySql("delete from BackModel where id= " + row["id"]);
            BackUpSystem.DelBackUp(xpath);
            return true;
        }

        public PageResult<BackModel> GetBackModelListPage(BackModel model)
        {
            PageResult<BackModel> page = new PageResult<BackModel>();
            string sql = "select *,row_number() over(order by id desc) rownumber from BackModel m where 1=1 ";
            string countSql = "select count(1) from BackModel where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BackModel> list = new List<BackModel>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((BackModel)ReflectionUtil.GetModel(typeof(BackModel), row));
                }
            }
            page.rows = list;
            return page;
        }
    }
}
