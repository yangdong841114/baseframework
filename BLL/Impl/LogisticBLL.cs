﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class LogisticBLL : ILogisticBLL
    {

        private System.Type type = typeof(Logistic);
        public IBaseDao dao { get; set; }



        public PageResult<Logistic> GetListPage(Logistic model)
        {
            PageResult<Logistic> page = new PageResult<Logistic>();
            string sql = "select m.*,row_number() over(order by m.addTime desc) rownumber from Logistic m where 1=1 ";
            string countSql = "select count(1) from Logistic m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.logName))
                {
                    param.Add(new DbParameterItem("logName", ConstUtil.LIKE, model.logName));
                }
                if (!ValidateUtils.CheckNull(model.logNo))
                {
                    param.Add(new DbParameterItem("logNo", ConstUtil.LIKE, model.logNo));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Logistic> list = new List<Logistic>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Logistic)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public List<Logistic> GetList()
        {
            string sql = "select m.* from Logistic m where 1=1 ";

            DataTable dt = dao.GetList(sql);
            List<Logistic> list = new List<Logistic>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Logistic)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public int Save()
        {
            List<Detail> list = JhInterface.GetLogisticList();
            if (list != null && list.Count > 0)
            {
                dao.ExecuteBySql("delete from Logistic");

                DateTime now = DateTime.Now;

                DataTable dt = new DataTable("Logistic");
                dt.Columns.Add(new DataColumn("logNo", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("logName", Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("addTime", typeof(DateTime)));
                foreach (Detail r in list)
                {
                    DataRow row = dt.NewRow();
                    row["logNo"] = r.no;
                    row["logName"] = r.com;
                    row["addTime"] = now;
                    dt.Rows.Add(row);
                }
                List<DbParameterItem> param = ParamUtil.Get()
                    .Add(new DbParameterItem("logNo", DbType.String, null))
                    .Add(new DbParameterItem("logName", DbType.String, null))
                    .Add(new DbParameterItem("addTime", DbType.DateTime, null))
                    .Result();
                string sql = "insert into Logistic(logNo,logName,addTime) values(@logNo,@logName,@addTime)";
                DataSet set = new DataSet();
                set.Tables.Add(dt);
                return dao.InsertBatchByDataSet(set, "Logistic", sql, param);
            }

            return 1;
        }

    }
}
