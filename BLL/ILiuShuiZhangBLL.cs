﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 流水账业务逻辑接口
    /// </summary>
    public interface ILiuShuiZhangBLL : IBaseBLL<LiuShuiZhang>
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<LiuShuiZhang> GetListPage(LiuShuiZhang model);


        double GetsrMoney(LiuShuiZhang model);
        double GetzcMoney(LiuShuiZhang model);


        /// <summary>
        /// 导出用户流水excel
        /// </summary>
        /// <returns></returns>
        DataTable GetUserLiuShuiExcel(LiuShuiZhang model);
        /// <summary>
        /// 修改记录余额
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int UpdateBalance(LiuShuiZhang model, Member current);
    }
}
