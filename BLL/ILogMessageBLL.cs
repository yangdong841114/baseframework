﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 异常消息业务逻辑接口
    /// </summary>
    public interface ILogMessageBLL
    {

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<LogMessage> GetListPage(LogMessage model);

        /// <summary>
        /// 删除某条记录
        /// </summary>
        /// <param name="id">记录ID</param>
        /// <returns></returns>
        int Delete(int id);

        /// <summary>
        /// 清除所有记录
        /// </summary>
        void RemoveAll();

    }
}
