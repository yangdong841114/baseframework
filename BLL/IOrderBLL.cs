﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 订单管理业务逻辑接口
    /// </summary>
    public interface IOrderBLL
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<OrderHeader> GetListPage(OrderHeader model);

        /// <summary>
        /// 获取列表，不分页
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<OrderHeader> GetList(OrderHeader model);

        /// <summary>
        /// 导出excel数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        DataTable GetExcelList(OrderHeader model);

        /// <summary>
        /// 保存复消订单
        /// </summary>
        /// <param name="oh"></param>
        /// <returns></returns>
        string SaveFxOrder(OrderHeader oh);

        /// <summary>
        /// 保存注册订单
        /// </summary>
        /// <param name="oh"></param>
        /// <returns></returns>
        string SaveRegisterOrder(OrderHeader oh);

        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int DeleteOrder(string id);

        /// <summary>
        /// 订单发货
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        int SaveLogistic(List<OrderHeader> list);
        /// <summary>
        /// 查询物流信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        LogisticResult GetLogisticMsg(string id);
        int SaveSureReceive(OrderHeader order);
        int ChangeAddress(OrderHeader oh);
    }
}
