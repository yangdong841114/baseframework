﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 角色业务逻辑接口
    /// </summary>
    public interface IRoleBLL
    {
        /// <summary>
        /// 分页查询未分配roleId角色的会员
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        PageResult<Role> GetNotRoleMemberPage(Role role, Member current);

        /// <summary>
        /// 分页查询角色会员分配关系
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="current">当前登录用户</param>
        /// <returns></returns>
        PageResult<Role> GetRoleMemberPage(Role model,Member current);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="current">当前登录用户</param>
        /// <returns></returns>
        PageResult<Role> GetListPage(Role model, Member current);

        /// <summary>
        /// 更新角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns>影响行数</returns>
        int Update(Role model);

        /// <summary>
        /// 保存角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ID主键</returns>
        int Save(Role model);

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        string Delete(int id);

        /// <summary>
        /// 删除角色会员分配关系
        /// </summary>
        /// <param name="list">要删除的id</param>
        /// <returns></returns>
        string DeleteRm(List<int> list);

        /// <summary>
        /// 批量保存角色会员关系
        /// </summary>
        /// <param name="list">保存的列表</param>
        /// <returns></returns>
        int SaveRm(List<Role> list);

        /// <summary>
        /// 批量保存角色分配的菜单
        /// </summary>
        /// <param name="list">保存的列表</param>
        /// <returns></returns>
        int SaveRoleResorce(List<Role> list);

    }
}
