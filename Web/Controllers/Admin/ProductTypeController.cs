﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class ProductTypeController : Controller
    {
        public IProductTypeBLL producttypeBLL { get; set; }

        /// <summary>
        /// 分页查询一级类目
        /// </summary>
        /// <returns></returns>
        public JsonResult GetListPage(ProductType model)
        {
            if (model == null) { model = new ProductType(); }
            PageResult<ProductType> page = null;
            model.grade = 1;
            page = producttypeBLL.GetListPage(model, "id,name,grade,parentId,parentNmae,remark,no");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SmGetListPage(ProductType model)
        {
            if (model == null) { model = new ProductType(); }
            PageResult<ProductType> page = null;
            page = producttypeBLL.GetListPage(model, "id,name,grade,parentId,parentNmae,remark,no");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        

        public JsonResult GetDefaultData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                List<ProductType> list = producttypeBLL.GetList("select * from ProductType where grade=1");
              
                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("mlist", list);           

                list = producttypeBLL.GetList("select * from ProductType where grade=2");
                di.Add("mlistSm", list);           
            
                response.map = di;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetModel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                ProductType n = producttypeBLL.GetModel(id);
                response.Success();
                response.result = n;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                producttypeBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存一级类目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveByUeditor(ProductType model)
        {
            if (model == null) { model = new ProductType(); }

            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
               
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                if (model.id != null)
                {
                    producttypeBLL.UpdateProductType(model);
                }
                else
                {
                    producttypeBLL.SaveProductType(model);
                }

                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

       

       
    }
}