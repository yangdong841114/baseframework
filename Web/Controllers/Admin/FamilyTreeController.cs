﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 系谱图Controller
    /// </summary>
    public class FamilyTreeController : Controller
    {
        public IFamilyTreeBLL familyTreeBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 默认系谱图
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetFamilyTree(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                string val = familyTreeBLL.CreateTreeHtml(1, 3, "Register", "MemberInfo", (Dictionary<string, string>)Session["lanager"]);
                response.msg = val;
                //系谱图上线链路
                response.other = memberBLL.GetParentLinkString(1, 1);
                response.status = "success";
            }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 条件查询系谱图
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ceng"></param>
        /// <returns></returns>
        public JsonResult GetFamilyTreeByCondition(string userId, int ceng)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                string val = "";
                if (userId != null && userId.Trim().Length > 0)
                {
                    Member mb = memberBLL.GetModelByUserId(userId);
                    if (mb != null)
                    {
                        val = familyTreeBLL.CreateTreeHtml(mb.id.Value, ceng, "Register", "MemberInfo", (Dictionary<string, string>)Session["lanager"]);
                        //系谱图上线链路
                        response.other = memberBLL.GetParentLinkString(1, mb.id.Value);
                    }
                    else
                    {
                        val = "";
                    }
                }
                else
                {
                    val = familyTreeBLL.CreateTreeHtml(1, ceng, "Register", "MemberInfo", (Dictionary<string, string>)Session["lanager"]);
                    //系谱图上线链路
                    response.other = memberBLL.GetParentLinkString(1, 1);
                }
                response.msg = val;
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// APP默认系谱图
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetAppFamilyTree(string userId)
        {
            ResponseDtoMap<string, Member> response = new ResponseDtoMap<string, Member>("fail");
            try
            {
                Dictionary<string, Member> di = familyTreeBLL.GetMemberList(1);
                response.map = di;
                //系谱图上线链路
                response.other = memberBLL.GetParentLinkString(1, 1);
                response.status = "success";
            }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }


        /// <summary>
        /// 条件查询APP系谱图
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="ceng"></param>
        /// <returns></returns>
        public JsonResult GetAppFamilyTreeByCondition(string userId)
        {
            ResponseDtoMap<string, Member> response = new ResponseDtoMap<string, Member>("fail");
            try
            {
                Dictionary<string, Member> di = null;
                if (userId != null && userId.Trim().Length > 0)
                {
                    Member mb = memberBLL.GetModelByUserId(userId);
                    if (mb != null)
                    {
                        di = familyTreeBLL.GetMemberList(mb.id.Value);
                        //系谱图上线链路
                        response.other = memberBLL.GetParentLinkString(1, mb.id.Value);
                    }
                    else
                    {
                        di = familyTreeBLL.GetMemberList(1);
                    }
                }
                else
                {
                    di = familyTreeBLL.GetMemberList(1);
                    //系谱图上线链路
                    response.other = memberBLL.GetParentLinkString(1, 1);
                }
                response.map = di;
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
    }
}
