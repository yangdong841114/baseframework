﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using Web.Models;

namespace Web.Admin.Controllers
{
    public class ParameterSetController : Controller
    {
        public IParamSetBLL paramBLL { get; set; }

        /// <summary>
        /// 初始化页面，读取已有参数
        /// </summary>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoMap<string, ParameterSet> response = new ResponseDtoMap<string, ParameterSet>();
            response.map = paramBLL.GetAllToDictionary();
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        public JsonResult GetSetCourier()
        {
            Member current = (Member)Session["LoginUser"];
            var list = CourierHelp.GetCourierList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SetCourier()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                CourierHelp.SetCourier();
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }
        
        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="list">参数列表</param>
        /// <returns></returns>
        public JsonResult Save(List<ParameterSet> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                paramBLL.UpdateList(list);
                response.status = "success";
            }
            catch (ValidateException ex){response.msg = ex.Message;}
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
