﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 公告管理Controller
    /// </summary>
    public class ArticleController : Controller
    {
        public INewsBLL newBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <returns></returns>
        public JsonResult GetListPage(News model)
        {
            if (model == null) { model = new News(); }
            model.typeId = ConstUtil.NEWS_GG; //公告管理
            PageResult<News> page = null;
            page = newBLL.GetListPage(model, "id,title,addTime,content,isTop");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetModel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                News n = newBLL.GetModelById(id);
                response.Success();
                response.result = n;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                newBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveByUeditor(News model)
        {
            if (model == null) { model = new News(); }
            
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["LoginUser"];
                model.typeId = ConstUtil.NEWS_GG;
                if (model.id != null)
                {
                    newBLL.Update(model, mb);
                }
                else
                {
                    newBLL.Save(model, mb);
                }
                
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 置顶
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult SaveTop(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                newBLL.SaveTop(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取消置顶
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CancelTop(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                newBLL.SaveCancelTop(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
