﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 已开通会员Controller
    /// </summary>
    public class MemberPassedController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        public IMobileNoticeBLL noticeBLL { get; set; }

        /// <summary>
        /// 群发短信
        /// </summary>
        /// <param name="phones">群发短信的电话列表</param>
        /// <param name="msg">发送信息</param>
        /// <param name="flag">1：勾选群发，2：群发所有</param>
        /// <returns></returns>
        public JsonResult SendMessage(List<string> phones,string msg,int flag)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (flag == 2)
                {
                    List<Member> list = memberBLL.GetList("select phone from Member where id>1");
                    if (list != null && list.Count > 0)
                    {
                        phones = new List<string>();
                        foreach (Member m in list)
                        {
                            phones.Add(m.phone);
                        }
                    }
                }
                noticeBLL.SendMessage(phones, msg);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "发送失败，请联系管理员"; }
            
            return Json(response, JsonRequestBehavior.AllowGet); ;
        } 

        /// <summary>
        /// 分页查询已开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            model.isPay = 1;

            string fields = "id,userId,userName,uLevel,rLevel,regMoney,bankName,bankCard,bankUser,code,phone,address,qq," +
                            "reName,shopName,passTime,byopen,byopenId,empty,isLock,isFt";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 冻结/启用会员
        /// </summary>
        /// <param name="id">会员ID</param>
        /// <param name="isLock">0:启用,1:冻结,2:重置密码</param>
        /// <returns></returns>
        public JsonResult LockMember(List<int> ids, int isLock)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            if (ids == null || ids.Count == 0)
            {
                response.msg = "要操作的会员为空";
            }
            else
            {
                try
                {
                    int c = memberBLL.UpdateLock(ids, isLock);
                    response.msg = "操作成功";
                    response.status = "success";
                }
                catch (ValidateException va) { response.msg = va.Message; }
                catch (Exception)
                {
                    response.msg = "操作失败！请联系管理员";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 登录前台
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult ToForward(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member m = memberBLL.GetModelById(id);
                if (m != null)
                {
                    m.password = null;
                    m.passOpen = null;
                    m.threepass = null;
                    m.answer = null;
                    m.question = null;
                    Session["MemberUser"] = m;
                    response.Success();
                }
                else
                {
                    throw new ValidateException("用户不存在");
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        //导出开通会员excel
        public ActionResult ExportOpenExcel()
        {
            int type = 1;//已开通
            DataTable dt = memberBLL.GetMemberExcelList(type);

            System.Collections.Hashtable htb = new System.Collections.Hashtable();
            htb.Add("userId", "会员编号");
            htb.Add("userName", "会员名称");
            htb.Add("addTime", "注册日期");
            htb.Add("fatherName", "接点人编号");
            htb.Add("reName", "推荐人编号");
            htb.Add("shopName", "报单中心");
            htb.Add("uLevel", "会员等级");
            htb.Add("regMoney", "注册金额");
            htb.Add("phone", "联系电话");
            htb.Add("byopen", "操作人");
            htb.Add("passTime", "开通时间");


            DataToExcel dte = new DataToExcel();
            //string FilePath = dte.DataToExcel1(dt, "a");//
            string FilePath = Server.MapPath("~/Admin/excel/");

            string filename = "";
            try
            {
                //if (dt.Rows.Count > 0)
                //{
                    //CreateExcel(dtexcel, "application/ms-excel", excel);

                    filename = dte.DataExcel(dt, "标题", FilePath, htb);
                //}
                dte.CreateExcel(dt, "application/ms-excel", filename, htb);
                return File(FilePath + filename, "application/javascript", filename);

            }
            catch (Exception)
            {
                dte.KillExcelProcess();
                throw;
            }

        }

    }
}
