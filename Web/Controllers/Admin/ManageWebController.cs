﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Xml.Linq;

namespace Web.Admin.Controllers
{
    public class ManageWebController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public IResourceBLL resourceBLL { get; set; }

        public IDataDictionaryBLL dataDictionaryBLL { get; set; }

        public IBaseSetBLL setBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public IAreaBLL areaBLL { get; set; }


        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMenu(string parentId)
        {
            Member mm = (Member)Session["LoginUser"]; //当前登陆用户
            ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            string strPath = Server.MapPath("~/Config/" + mm.userId + ".txt");
            List<Resource> list = new List<Resource>();
            if (System.IO.File.Exists(strPath))
            {
                var strMenu = System.IO.File.ReadAllText(strPath);
                list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Resource>>(strMenu);
            }
            else
            {
                list = resourceBLL.GetListByUserAndParent(mm.id.Value, parentId);
                var strMenu = Newtonsoft.Json.JsonConvert.SerializeObject(list);
                System.IO.File.WriteAllText(strPath, strMenu);
            }

            response.list = list;

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //获取通知信息
        public JsonResult GetNotic()
        {
            ResponseDtoMap<string, List<SystemMsg>> response = new ResponseDtoMap<string, List<SystemMsg>>();
            try
            {
                Dictionary<string, List<SystemMsg>> di = msgBLL.GetMainData();
                response.map = di;
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载数据失败"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取APP前台一级菜单
        /// </summary>
        /// <returns></returns>
        public JsonResult GetWapMenu()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            //ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["LoginUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByPartentId(mm.id.Value, "103");
            Dictionary<string, object> di = new Dictionary<string, object>();
            //如果当前会员不是报单中心，则过滤掉报单中心管理菜单
            //if (mm.isAgent != 2)
            //{
            //    List<Resource> lit = new List<Resource>();
            //    if (list != null && list.Count > 0)
            //    {
            //        foreach (Resource r in list)
            //        {
            //            if (r.id != ConstUtil.WAP_MENU_SHOP && r.parentResourceId != ConstUtil.WAP_MENU_SHOP)
            //            {
            //                lit.Add(r);
            //            }
            //        }
            //    }
            //    di.Add("frist", lit);
            //}
            //else
            //{
            //    di.Add("frist", list);
            //}
            List<Resource> allList = resourceBLL.GetListByUserAndParent(mm.id.Value, "103");
            di.Add("frist", list);
            di.Add("all", allList);
            response.map = di;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取基础数据
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public JsonResult GetBaseData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                BaseSet set = setBLL.GetModel(); // 基础设置
                Dictionary<string, List<DataDictionary>> result = dataDictionaryBLL.GetAllToDictionary(); //数据字典
                var path = Server.MapPath("~/Config/Lanagers.xml");
                List<Lanagers> lanagerList = LanagerData.GetLanagerList(path, "chinese");//默认英文
                Dictionary<string, string> lanagers = LanagerData.GetLanagerDictionary(path, "chinese");//默认英文
                //List<SystemMsg> mlist = msgBLL.GetList(0); //通知提醒
                response.Success();
                Member cur = (Member)Session["LoginUser"];
                //di.Add("notic", mlist);
                di.Add("id", cur.id);
                di.Add("userId", cur.userId);
                di.Add("baseSet", set);
                di.Add("cacheData", result);
                di.Add("area", areaBLL.GetTreeModelList()); //省市区数据
                di.Add("lanager", lanagers);
                di.Add("lanagerList", lanagerList);
                Session["lanager"] = lanagers;
                response.map = di;
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载基础数据失败"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 验证后台安全密码
        /// </summary>
        /// <param name="pass2"></param>
        /// <returns></returns>
        public JsonResult CheckAPass2(string pass2)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member cur = (Member)Session["LoginUser"];
                response.msg = "安全密码错误";
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass2 = DESEncrypt.EncryptDES(pass2, ConstUtil.SALT);
                    if (pass2 == m.passOpen)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 验证后台交易密码
        /// </summary>
        /// <param name="pass3"></param>
        /// <returns></returns>
        public JsonResult CheckAPass3(string pass3)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = "交易密码错误";
                Member cur = (Member)Session["LoginUser"];
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass3 = DESEncrypt.EncryptDES(pass3, ConstUtil.SALT);
                    if (pass3 == m.threepass)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
