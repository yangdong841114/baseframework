﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 已开通会员Controller
    /// </summary>
    public class UMemberPassedController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 分页查询已开通列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult GetListPage(Member model)
        {
            if (model == null)
            {
                model = new Member();
            }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.isPay = 1;
            model.shopid = mb.id;
            string fields = "id,userId,userName,uLevel,regMoney,phone," +
                            "reName,fatherName,passTime,isLock,addTime,isFt";
            PageResult<Member> page = memberBLL.GetMemberListPage(model, fields);
            return Json(page, JsonRequestBehavior.AllowGet); ;
        }

    }
}
