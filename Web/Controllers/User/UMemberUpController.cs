﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 会员晋升Controller
    /// </summary>
    public class UMemberUpController : Controller
    {
        //会员业务处理接口
        public IMemberBLL memberBLL { get; set; }

        public IMemberUpBLL memberUpBLL { get; set; }


        /// <summary>
        /// 获取注册金额
        /// </summary>
        /// <param name="ulevel"></param>
        /// <returns></returns>
        public JsonResult GetRegMoney(int ulevel)
        {
            ResponseDtoData response = new ResponseDtoData();
            try
            {
                Member current = (Member)Session["MemberUser"];         //当前登录用户
                object[] objs = memberBLL.GetRegMoneyAndNum(ulevel);
                double my = Convert.ToDouble(objs[0]) - current.regMoney.Value;//晋升所需金额
                response.msg = my.ToString();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.status = "fail"; response.msg = "出现错误，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];         //当前登录用户
                Member user = memberBLL.GetModelAndAccountNoPass(current.id.Value); //获取当前用户最新信息
                response.result = user;
                response.status = "success";
            }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="MemberUp">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(MemberUp model)
        {
            if (model == null) { model = new MemberUp(); }
            Member current = (Member)Session["MemberUser"];         //当前登录用户
            model.uid = current.id;
            PageResult<MemberUp> result = memberUpBLL.GetListPage(model);
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存会员晋升
        /// </summary>
        /// <param name="model">保存对象</param>
        /// <returns></returns>
        public JsonResult Save(MemberUp model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                model.userId = current.userId;
                memberUpBLL.SaveMemberUp(model, current, 1);
                response.result = memberBLL.GetModelAndAccountNoPass(current.id.Value);
                response.msg = "晋升成功";
                response.status = "success";
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "晋升失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
