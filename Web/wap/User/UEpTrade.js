
define(['text!UEpTrade.html', 'jquery'], function (UEpTrade, $) {

    var controller = function (name) {

        var isSystem = LoginUser == "system";

        var batchBuyList = [];
        var editList = {};
        //设置标题
        var titles = lanager["交易市场"];
        $("#title").html(titles);
        var pageHtml = UEpTrade;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        if (!isSystem) {
            $("#batchDiv").css("display", "none");
            $("#UEpTradedatalist").removeClass("operationlist");
        }

        //输入框取消按钮
        $(".erase").each(function () {
            var dom = $(this);
            dom.bind("click", function () {
                var prev = dom.prev();
                dom.prev().val("");
            });
        });

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //查询参数
        this.param = utils.getPageData();

        //打开购买窗口
        openBuyDialog = function (sid, waitNum) {
            $("#wtNum").html(waitNum);
            $("#sid").val(sid);
            $("#buyNum").val("");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                var val = $("#buyNum").val();
                if (val == 0 || isNaN(val) || val < 0) {
                    utils.showErrMsg("购买数量格式错误");
                } else {
                    var data = { buyNum: val, sid: $("#sid").val() };
                    utils.AjaxPost("/User/UEpTrade/SaveRecord", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("购买成功");
                            searchMethod();

                        }
                    });
                }
            });
            utils.showOrHiddenPromp();
        }

        //全选按钮
        $("#checkAllBtn").bind("change", function () {
            var checked = this.checked;
            $(".itemcheckedbox").each(function () {
                this.checked = checked;
            });
        });

        //获取选中的记录
        getCheckedIds = function () {
            var rows = [];
            $(".itemcheckedbox").each(function () {
                if (this.checked) {

                    rows.push({ id: $(this).attr("dataId") });
                }
            });
            return rows;
        }
        //取消批量购买
        cancalBatchBuy = function () {
            batchBuyList = [];
            $("#div2").css("display", "none");
            $("#div1").css("display", "block");
        }

        //批量购买
        $("#batchBuyBtn").bind("click", function () {
            var rows = getCheckedIds();
            if (rows.length == 0) {
                utils.showErrMsg("您还没有勾选任何挂卖记录");
            } else {
                batchBuyList = [];
                var html = "";
                for (var i = 0; i < rows.length; i++) {
                    var id = rows[i].id;
                    var dto = editList[id];
                    html += '<li><dl><dt>挂卖单号</dt><dd>' + dto.number + '</dd></dl><dl><dt>卖家编号</dt><dd>' + dto.userId + '</dd></dl>' +
                        '<dl><dt>购买数量</dt><dd><input type="text" required="required" value="' + dto.waitNum + '" class="entrytxt" id="buy' + dto.id + '" placeholder="请输入购买数量" /></dd></dl>' +
                        '<dl><dt>待出售数量</dt><dd>' + dto.waitNum + '</dd></dl>' +
                        '<dl><dt>挂卖日期</dt><dd>' + dto.addTime + '</dd></dl>';
                    batchBuyList.push(dto);
                }
                $("#buyDataList").empty();
                $("#buyDataList").html(html);
                $("#div1").css("display", "none");
                $("#div2").css("display", "block");
            }
        })

        //批量购买保存
        $("#batchBuySaveBtn").bind("click", function () {
            var rows = batchBuyList;
            var checked = true
            var data = {};
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                var id = row.id;
                var val = $("#buy" + id).val();
                if (val == 0 || isNaN(val) || val < 0) {
                    utils.showErrMsg("挂卖单号【" + row.number + "】的购买数量格式错误");
                    checked = false;
                    return;
                } else if (row.waitNum < val) {
                    utils.showErrMsg("挂卖单号【" + row.number + "】的购买数量超出待售数量");
                    checked = false;
                    return;
                }
                data["list[" + i + "].sid"] = row.id;
                data["list[" + i + "].buyNum"] = val;
                data["list[" + i + "].snumber"] = row.number;
            }
            if (checked) {
                utils.AjaxPost("/User/UEpTrade/SaveRecordBatch", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        cancalBatchBuy();
                        utils.showSuccessMsg("批量购买成功");
                        searchMethod();

                    }
                });
            }
        });

        //分页插件
        var dropload = $('#UEpTradedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData('/User/UEpTrade/GetListPage', param, me,
                    function (rows) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            var dto = rows[i];

                            editList[dto.id] = dto;
                            html += '<li>';
                            if (isSystem) {
                                html += '<label><input class="operationche itemcheckedbox" type="checkbox" dataId="' + dto.id + '" /></label>';
                            }
                            html += '<div class="orderbriefly" style="padding-left:2.7rem;" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.waitNum + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                                '<div class="allinfo"><div class="btnbox"><ul class="tga1"><li><button class="smallbtn" onclick=\'openBuyDialog(' + dto.id + ',' + dto.waitNum + ')\'>购买</button></li></ul></div>' +
                                '<dl><dt>挂卖单号</dt><dd>' + dto.number + '</dd></dl><dl><dt>挂卖日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                '<dl><dt>卖家编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>待出售数量</dt><dd>' + dto.waitNum + '</dd></dl>' +
                                '<dl><dt>已出售数量</dt><dd>' + dto.scNum + '</dd></dl><dl><dt>挂卖数量</dt><dd>' + dto.saleNum + '</dd></dl>' +
                                '</div></li>';
                        }
                        $("#UEpTradeitemList").append(html);
                    }, function () {
                        $("#UEpTradeitemList").append('<p class="dropload-noData">' + lanager["暂无数据"] + '</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UEpTradeitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //查询按钮
        $("#searchBtn").bind("click", function () { searchMethod(); })

        controller.onRouteChange = function () {
            //销毁模态窗口
            //$('#dlg').dialog("destroy");
        };
    };

    return controller;
});