
define(['text!UProductShop.html', 'jquery'], function (UProductShop, $) {

    var controller = function (id) {

        var flid = 0;
        //设置标题
        var titles = lanager["商家"];
        $("#title").html(titles);
        if (!id || id == 0) id = 0;
        if (id.indexOf('fl') > -1) {
            flid = id.replace('fl', '');
            id = 0;
        }

        utils.AjaxPostNotLoadding("/User/UProductShop/InitCartCount", { uid: id, producttypeid: flid }, function (result) {

            if (result.status == "success") {
                var pageHtml = UProductShop;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);
                $("#title").html(lanager[result.map.name]);
                $("#uid").val(id);
                $("#flid").val(flid);
                //广告图
                var banners = result.map.banners;
                if (banners && banners.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < banners.length; i++) {
                        banHtml += '<li><img src="' + banners[i].imgUrl + '" /></li>';
                    }
                    $("#slides").html(banHtml);
                }

                //滚动广告图
                $(".main_visual").hover(function () {
                    $("#btn_prev,#btn_next").fadeIn()
                }, function () {
                    $("#btn_prev,#btn_next").fadeOut()
                });

                //$dragBln = false;

                $(".main_image").touchSlider({
                    flexible: true,
                    speed: 500,
                    delayTime: 3000,
                    btn_prev: $("#btn_prev"),
                    btn_next: $("#btn_next"),
                    paging: $(".flicking_con a"),
                    counter: function (e) {
                        $(".flicking_con a").removeClass("on").eq(e.current - 1).addClass("on");
                    }
                });

                //清空查询条件按钮
                $("#clearQueryBtn").bind("click", function () {
                    utils.clearQueryParam();
                })

                var count = parseInt(result.other);
                $("#cartCount").html(count);

                //查询参数
                this.param = utils.getPageData()
                param["uid"] = $("#uid").val();
                param["productTypeId"] = $("#flid").val();

                var dropload = $('#UProductShopdatalist').dropload({
                    scrollArea: window,
                    autoLoad: true,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData("/User/UProductShop/GetListPage", param, me,
                            function (rows, footers) {
                                var html = "";
                                for (var i = 0; i < rows.length; i++) {
                                    var dto = rows[i];
                                    html += '<li><a href="#ProductDetail/' + dto.id + '">' +
                                        '<p><img data-toggle="lightbox" src="' + dto.imgUrl + '"  class="img-thumbnail" alt=""></p>' +
                                        '<h2>' + dto.productName + '</h2><span>￥' + dto.price + '&nbsp;&nbsp;&nbsp;<strike>' + dto.fxPrice + '</strike></span>' +
                                        '</a></li>';
                                }
                                $("#UProductShopitemList").append(html);
                            }, function () {
                                $("#UProductShopitemList").append('<p class="dropload-noData">' + lanager["暂无数据"] + '</p>');
                            });
                    }
                });

                //searchMethod();
                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    $("#UProductShopitemList").empty();
                    param["productName"] = $("#productName").val();
                    param["uid"] = $("#uid").val();
                    param["productTypeId"] = $("#flid").val();

                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }

                //搜索按钮点击事件
                $("#queryProductBtn").bind("click", function () {
                    searchMethod();
                });


            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});