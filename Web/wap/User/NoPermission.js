
define(['text!NoPermission.html', 'jquery'], function (NoPermission, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["无权访问"];
        $("#title").html(titles);
        var pageHtml = NoPermission;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);
        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});