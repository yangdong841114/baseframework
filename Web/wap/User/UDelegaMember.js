
define(['text!UDelegaMember.html', 'jquery'], function (UDelegaMember, $) {

    var controller = function (name) {

        var titles = lanager["账户托管"];
        $("#title").html(titles);
        var pageHtml = UDelegaMember;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //子账户余额转入主账户按钮
        $("#transferBtn").on("click", function () {
            $("#prompTitle").html("您确定所有子账户金额转入主账户吗？");
            $("#sureBtn").html("确定转入");
            //$("#prompCont").html("<font style='color:red'>您确定所有子账户金额转入主账户吗？</font>");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                sureTransfer();
            });
            utils.showOrHiddenPromp();
        });

        //确认子账户余额转入主账户
        sureTransfer = function () {
            utils.AjaxPost("/User/UDelegaMember/Transfer", {}, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showOrHiddenPromp();
                    utils.showSuccessMsg("转入成功");
                    searchMethod();
                }
            });
        }

        //添加按钮
        $("#addBtn").on("click", function () {
            var checked = true;
            if ($("#userId").val() == 0) {
                utils.showErrMsg("请输入托管会员编号");
            } else if ($("#password").val() == 0) {
                utils.showErrMsg("请输入交易密码");
            } else {
                var data = { password: $("#password").val(), delegaUserId: $("#userId").val() };
                utils.AjaxPost("/User/UDelegaMember/Save", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("托管成功！");
                        $("#userId").val("");
                        $("#password").val("");
                        searchMethod();
                    }
                })
            }
        });

        //取消托管
        cancelDelega = function (dataId) {
            $("#prompTitle").html("您确定取消托管吗？");
            $("#sureBtn").html("确定取消");
            //$("#prompCont").html("<font style='color:red'>您确定取消托管吗？</font>");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                sureCancel(dataId);
            });
            utils.showOrHiddenPromp();
        }

        //确定取消
        sureCancel = function (dataId) {
            utils.AjaxPost("/User/UDelegaMember/Delete", { id: dataId }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showOrHiddenPromp();
                    utils.showSuccessMsg("操作成功");
                    searchMethod();
                }
            });
        }

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });

        //输入框取消按钮
        $(".erase").each(function () {
            var dom = $(this);
            dom.bind("click", function () {
                var prev = dom.prev();
                prev.val("");
            });
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UDelegaMemberDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UDelegaMember/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time>';
                            html += '<span class="sum">' + dto.delegaUserId + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><button class="smallbtn" onclick="cancelDelega('+dto.id+')">取消托管</button></div>' +
                            '<dl><dt>托管时间</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>托管会员编号</dt><dd>' + dto.delegaUserId + '</dd></dl>' +
                            '<dl><dt>电子币</dt><dd>' + dto.agentDz + '</dd></dl><dl><dt>奖金币</dt><dd>' + dto.agentJj + '</dd></dl>' +
                            '<dl><dt>购物币</dt><dd>' + dto.agentGw + '</dd></dl><dl><dt>复投币</dt><dd>' + dto.agentFt + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#UDelegaMemberItemList").append(html);
                    }, function () {
                        $("#UDelegaMemberItemList").append('<p class="dropload-noData">' + lanager["暂无数据"] + '</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UDelegaMemberItemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        controller.onRouteChange = function () {
        };
    };

    return controller;
});