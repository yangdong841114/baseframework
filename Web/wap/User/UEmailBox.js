
define(['text!UEmailBox.html', 'jquery'], function (UEmailBox, $) {

    var controller = function (name) {

        var tab2Init = false;
        var tab3Init = false;
        var SendDropload = null;
        var sendDataList = {};
        var receiveDataList = {};
        var bbIit = false;
        var sendEditor = null;
        var receiveEditor = null;

        //发件箱查询参数
        this.SendParam = utils.getPageData();

        //收件箱查询方法
        sendSearchMethod = function () {
            SendParam.page = 1;
            $("#USendItemList").empty();
            //param["startTime"] = $("#startTime").val();
            //param["endTime"] = $("#endTime").val();
            SendDropload.unlock();
            SendDropload.noData(false);
            SendDropload.resetload();
        }

        //变更选项卡
        changeTab = function (index,id) {
            $("#tabli1").removeClass("active");
            $("#tabli2").removeClass("active");
            $("#tabli3").removeClass("active");
            $("#emailTab1").css("display", "none");
            $("#emailTab2").css("display", "none");
            $("#emailTab3").css("display", "none");
            $("#emailTab4").css("display", "none");
            $("#emailTab5").css("display", "none");
            if (index == 1) {
                $("#tabli1").addClass("active");
                $("#emailTab1").css("display", "block");
            } else if (index == 2) {
                $("#tabli2").addClass("active");
                $("#emailTab2").css("display", "block");
                if (!tab2Init) {
                    //发件箱加载控件
                    SendDropload = $('#USendDataList').dropload({
                        scrollArea: window,
                        domDown: { domNoData: '<p class="dropload-noData"></p>' },
                        loadDownFn: function (me) {
                            utils.LoadPageData("/User/UEmailBox/GetSendListPage", SendParam, me,
                                function (rows, footers) {
                                    var html = "";
                                    for (var i = 0; i < rows.length; i++) {
                                        rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                                        var dto = rows[i];
                                        sendDataList[dto.id] = dto;
                                        html += '<li class="new"><a href="javascript:changeTab(5,' + dto.id + ')"><h2>' + dto.toUser + '</h2><time>' + dto.addTime + '</time>' +
                                              '<div class="clear"></div><h5>' + dto.title + '</h5></a></li>';
                                    }
                                    $("#USendItemList").append(html);
                                }, function () {
                                    $("#USendItemList").append('<p class="dropload-noData">' + lanager["暂无数据"] + '</p>');
                                });
                        }
                    });
                    tab2Init = true;
                }

                
            } else if (index == 3) {
                $("#tabli3").addClass("active");
                $("#emailTab3").css("display", "block");
                if (tab3Init == false) {
                    tab3Init = true;
                    //发邮件保存按钮
                    $("#saveSendBtn").on("click", function () {
                        if ($("#stoUser").val() == 0) {
                            utils.showErrMsg("请输入收件人");
                        } else if ($("#stitle").val() == 0) {
                            utils.showErrMsg("请输入主题");
                        } else if (sendEditor.getText() == 0) {
                            utils.showErrMsg("请输入邮件内容");
                        } else {
                            var data = { title: $("#stitle").val(), content: utils.getEditorHtml(sendEditor), toUser: $("#stoUser").val() };
                            utils.AjaxPost("/User/UEmailBox/SaveSend", data, function (result) {
                                if (result.status == "success") {
                                    utils.showSuccessMsg(result.msg);
                                    $("#stoUser").val("");
                                    $("#stitle").val("");
                                    utils.setEditorHtml(sendEditor, "");
                                    changeTab(2);
                                    sendSearchMethod();
                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            });
                        }
                    })
                } //end-tab3Init if
            } else if (index == 4) {
                $("#tabli1").addClass("active");
                var row = receiveDataList[id];
                $("#sourceId").val(row.id);
                $("#sfristId").val(row.id);
                if (row.fristId) {
                    $("#sfristId").val(row.fristId);
                }
                utils.AjaxPost("/User/UEmailBox/GetReceiveList", { fristId: $("#sfristId").val() }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        var list = result.list;
                        var user = result.msg;
                        var html = "";
                        var len = list.length;
                        for (var i = len-1; i >=0 ; i--) {
                            var dto = list[i];
                            var stt = dto.fromUser == user ? "" : "color:red;"
                            var fontcolor = dto.fromUser == user ? "#145ccd" : "red;"
                            if (dto.fromUser == user) {
                                html += '<li class="receive">';
                            } else {
                                html += '<li class="send">';
                            }
                            html += '<h2><span>发件人：</span>' + dto.fromUser + '</h2><time>' + utils.changeDateFormat(dto.addTime) + '</time>' +
                                   '<div class="msgcont">'+dto.content+'</div></li>';
                        }
                        $("#receiveDetailList").html(html);
                        $("#receiveDetailTitle").html(row.title );
                        $("#oldFromUser").val(row.fromUser);
                        $("#receiveDetailContent").empty();
                        $("#emailTab4").css("display", "block");
                    }
                });
                if (bbIit == false) {

                    //隐藏提示框
                    $(".hideprompt").click(function () {
                        utils.showOrHiddenPromp();
                    });
                    utils.CancelBtnBind();
                    receiveEditor = new Quill("#receiveEditor", {
                        modules: {
                            toolbar: utils.getEditorToolbar()
                        },
                        theme: 'snow'
                    });

                    $("#huifuBtn").bind("click", function () {
                        $("#restitle").val("回复【" + $("#receiveDetailTitle").html() + "】")
                        $("#sureBtn").unbind();
                        //发邮件保存按钮
                        $("#sureBtn").bind("click", function () {
                            if ($("#restitle").val() == 0) {
                                utils.showErrMsg("请输入邮件主题");
                            } else if (receiveEditor.getText() == 0) {
                                utils.showErrMsg("请输入邮件内容");
                            } else {
                                var data = {
                                    title: $("#restitle").val(), content: utils.getEditorHtml(receiveEditor), toUser: $("#oldFromUser").val(),
                                    sourceId: $("#sourceId").val(), fristId: $("#sfristId").val()
                                };
                                utils.AjaxPost("/User/UEmailBox/SaveReceive", data, function (result) {
                                    utils.showOrHiddenPromp();
                                    if (result.status == "success") {
                                        utils.showSuccessMsg(result.msg);
                                        changeTab(2);
                                        receiveSearchMethod();
                                    } else {
                                        utils.showErrMsg(result.msg);
                                    }
                                });
                            }
                        });
                        utils.showOrHiddenPromp();
                       
                    });
                    bbIit = true;
                }
                
            } else {
                $("#tabli2").addClass("active");
                var row = sendDataList[id];
                $("#sendDetailTitle").html(row.title);
                $("#sendDetailSjr").html("<span>收件人：</span>"+row.toUser);
                $("#sendDetailTime").html(row.addTime);
                $("#sendDetailContent").empty();
                $("#sendDetailContent").html(row.content);
                $("#emailTab5").css("display", "block");
            }
        }
        var titles = lanager["邮件中心"];
        $("#title").html(titles);
        var pageHtml = UEmailBox;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        sendEditor = new Quill("#editor", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        utils.CancelBtnBind();

        //收件箱查询参数
        this.ReceiveParam = utils.getPageData();

        //收件箱加载控件
        var ReceiveDropload = $('#UReceiveDataList').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UEmailBox/GetReceiveListPage", ReceiveParam, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["read"] = rows[i]["isRead"] == 2 ? "是" : "否";
                            var dto = rows[i];
                            receiveDataList[dto.id] = dto;
                            html += '<li class="new"><a href="javascript:changeTab(4,'+dto.id+')"><h2>' + dto.fromUser + '</h2><time>' + dto.addTime + '</time>' +
                                  '<div class="clear"></div><h5>' + dto.title + '</h5></a></li>';
                        }
                        $("#UReceiveItemList").append(html);
                    }, function () {
                        $("#UReceiveItemList").append('<p class="dropload-noData">' + lanager["暂无数据"] + '</p>');
                    });
            }
        });

        //收件箱查询方法
        receiveSearchMethod = function () {
            ReceiveParam.page = 1;
            $("#UReceiveItemList").empty();
            //param["startTime"] = $("#startTime").val();
            //param["endTime"] = $("#endTime").val();
            ReceiveDropload.unlock();
            ReceiveDropload.noData(false);
            ReceiveDropload.resetload();
        }


        //*************************************************** 收件箱end **************************************************************/


        controller.onRouteChange = function () {
            receiveDataList = null;
            sendDataList = null;
        };
    };

    return controller;
});