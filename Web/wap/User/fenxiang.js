
define(['text!fenxiang.html', 'jquery'], function (fenxiang, $) {

    var controller = function (name) {
        var titles = lanager["推广分享"];
        $("#title").html(titles);
        $("body").addClass("shareqr");
        var pageHtml = fenxiang;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        utils.AjaxPostNotLoadding("/User/UserWeb/GetMainData", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                if (map) {
                    ////推广链接
                    var siteUrl = map.siteUrl;
                    var qrcodeUrl = map.fxqrcodeUrl;
                    //$("#siteUrl").attr("href", siteUrl);
                    //$("#siteUrl").html(siteUrl);

                    ////复制链接
                    //var clip = new ZeroClipboard(document.getElementById("copyUrl"));

                    //二维码
                    var qrcode = map.qrcode;
                    $("#qrcode").attr("src", "/UpLoad/qrcode/" + qrcode);
                    $("#userName").html(map.userName);
                    window._bd_share_config = {
                        common: {
                            bdText: '推广分享',
                            bdDesc: '推广分享',
                            bdUrl: siteUrl,
                            bdPic: qrcodeUrl
                        },
                        share: [{
                            "bdSize": 16
                        }],

                        image: [{
                            viewType: 'list',
                            viewPos: 'top',
                            viewColor: 'black',
                            viewSize: '16',
                            viewList: ['qzone', 'tsina', 'huaban', 'tqq', 'renren']
                        }],
                        selectShare: [{
                            "bdselectMiniList": ['qzone', 'tqq', 'kaixin001', 'bdxc', 'tqf']
                        }]
                    }
                    with (document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src = 'http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion=' + ~(-new Date() / 36e5)];

                }



            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $("body").removeClass("shareqr");
        };
    };

    return controller;
});