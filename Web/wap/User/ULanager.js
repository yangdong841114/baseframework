﻿
define(['text!ULanager.html', 'jquery'], function (ULanager, $) {

    var controller = function () {

        //设置标题
        var titles = lanager["选择语言"];
        $("#title").html(titles);


        //加载会员信息
        utils.AjaxPostNotLoadding("/User/UMemberInfo/GetLanager", null, function (result) {
            if (result.status == "fail") {
                //utils.showErrMsg(result.msg);
            } else {
                var pageHtml = ULanager;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);

                var lanagerLists = [{ "id": "chinese", "value": "简体中文" }, { "id": "simplify", "value": "繁体中文" }, { "id": "english", "value": "英语" }];
                //初始化银行帐号下拉框
                utils.InitMobileSelect('#lanagerList', '' + lanager["选择语言"] + '', lanagerLists, null, [0], null, function (indexArr, data) {
                    $("#lanagerList").val(data[0].value);
                    $("#lanagerValue").val(data[0].id);
                });
                var dto = result.result;
                $("#lanagerList").val(dto.chineseLanager);
                $("#lanagerValue").val(dto.lanager);
                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        $("#lanagerList").val("");
                        dom.prev().val("");
                    });
                });


                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var fs = { "lanagerName": $("#lanagerList").val(), "lanagerValue": $("#lanagerValue").val() };
                    utils.AjaxPost("/User/UMemberInfo/EditLanager", fs, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg("保存成功！");
                            location.reload();
                        }
                    });
                });
            }
        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});