
define(['text!UArticDetail.html', 'jquery'], function (UArticDetail, $) {

    var controller = function (articId) {

        var titles = lanager["公告详情"];
        $("#title").html(titles);
        
        utils.AjaxPostNotLoadding("/User/UArticle/GetModel", { id: articId }, function (result) {
            if (result.status == "success") {
                var pageHtml = UArticDetail;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);
                var dto = result.result;
                $("#addTime").html(utils.changeDateFormat(dto.addTime));
                $("#contTitle").html(dto.title);
                $("#cont").html(dto.content);
            } else {
                utils.showErrMsg(result.msg);
            }
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});