
define(['text!UCharge.html', 'jquery'], function (UCharge, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期选择框
                    utils.initCalendar(["startTime", "endTime"]);

                    //清空查询条件按钮
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //查询按钮
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加载数据
                searchMethod();
            }
        }
        var titles = lanager["账户充值"];
        $("#title").html(titles);
        var pageHtml = UCharge;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);


        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UChargeDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UCharge/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = rows[i].ispay == 1 ? "待审核" : "已通过";

                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">+' + dto.epoints + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>充值日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>汇出银行</dt><dd>' + dto.fromBank + '</dd></dl><dl><dt>充值金额</dt><dd>' + dto.epoints + '</dd></dl>' +
                                  '<dl><dt>汇款时间</dt><dd>' + dto.bankTime + '</dd></dl><dl><dt>' +
                                  '汇款凭证</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="汇款凭证" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>' +
                                  '<dl><dt>汇入银行</dt><dd>' + dto.toBank + '</dd></dl><dl><dt>银行卡号</dt><dd>' + dto.bankCard + '</dd></dl>' +
                                  '<dl><dt>开户名</dt><dd>' + dto.bankUser + '</dd></dl><dl><dt>状态</dt><dd>' + dto.status + '</dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UChargeItemList").append(html);

                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UChargeItemList").append('<p class="dropload-noData">' + lanager["暂无数据"] + '</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UChargeItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //设置表单默认数据
        setDefaultValue = function () {
            $("#sysBankName").val("");
            $("#sysBankId").val("0");
            $("#toBankCard").empty();
            $("#toBankUser").empty();
            $("#fromBank").val("");
            $("#epoints").val("");
            $("#bankTime").val("");
            $("#file").val("");
        }

        //充值金额离开焦点
        $("#epoints").bind("blur", function () {
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            var dom = $(this);
            if (g.test(dom.val())) {
                var intVal = parseInt(dom.val());
                var val = intVal + parseFloat((Math.random()).toFixed(2));
                dom.val(val);
            }
        })

        //加载会员信息
        utils.AjaxPostNotLoadding("/User/UCharge/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                var list = result.list;

                //初始化汇入银行下拉框
                var bankData = [];
                if (list && list.length > 0) {
                    for (var i = 0; i < list.length; i++) {
                        bankData.push({ id: list[i].id + "|" + list[i].bankCard + "|" + list[i].bankUser, value: list[i].bankName });
                    }
                }
                //汇入银行选择框
                utils.InitMobileSelect('#sysBankName', '汇入银行', bankData, null, [0], null, function (indexArr, data) {
                    var val = data[0].id;
                    if (val != 0) {
                        var v = val.split("|");
                        $("#toBankCard").html(v[1]);
                        $("#toBankUser").html(v[2]);
                    } else {
                        $("#toBankCard").empty();
                        $("#toBankUser").empty();
                    }
                    $("#sysBankId").val(val);
                    $("#sysBankName").val(data[0].value);
                })

                //初始化汇款时间
                utils.initCalendar(["bankTime"]);

                //初始默认值
                setDefaultValue();

                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "sysBankName") {
                            $("#sysBankId").val("");
                            $("#toBankCard").empty();
                            $("#toBankUser").empty();
                        }

                        dom.prev().val("");
                    });
                });


                //*****************************************************银行汇款 start **********************************************************//

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();

                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#sysBankId").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    var isChecked = true;
                    if (val == 0) {
                        utils.showErrMsg("请选择汇入银行");
                    } else if ($("#fromBank").val() == 0) {
                        utils.showErrMsg("请录入汇出银行");
                    } else if (!g.test($("#epoints").val())) {
                        utils.showErrMsg("充值金额格式不正确");
                    } else if ($("#bankTime").val() == 0) {
                        utils.showErrMsg("请选择汇款时间");
                    } else if ($("#file").val() == 0) {
                        utils.showErrMsg("请上传汇款凭证");
                    } else {
                        $("#czhryh").html($("#sysBankName").val());
                        $("#czkh").html($("#toBankCard").html());
                        $("#czkhm").html($("#toBankUser").html());
                        $("#czje").html($("#epoints").val());
                        utils.showOrHiddenPromp();
                    }
                });

                //确认保存
                $("#sureBtn").bind("click", function () {
                    var formdata = new FormData();
                    var bankId = $("#sysBankId").val().split("|")[0];
                    formdata.append("sysBankId", bankId);
                    formdata.append("fromBank", $("#fromBank").val());
                    formdata.append("epoints", $("#epoints").val());
                    formdata.append("bankTime", $("#bankTime").val());
                    formdata.append("img", $("#file")[0].files[0]);
                    utils.AjaxPostForFormData("/User/UCharge/SaveCharge", formdata, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            dto = result.result;
                            setDefaultValue(dto);
                            //grid.datagrid("reload");
                        }
                    });
                });

                //*****************************************************银行汇款 end **********************************************************//
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});