
define(['text!messageCenter.html', 'jquery'], function (main, $) {

    var controller = function (parentId) {
        //设置标题
        var titles = lanager["信息中心"];
        $("#title").html(titles);
        var pageHtml = main;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        var items = menuItem[parentId];
        if (items && items.length > 0) {
            var banHtml = "";
            for (var i = 0; i < items.length; i++) {
                var node = items[i];
                var url = (node.curl + "").replace("User/", "");
                if (node.isShow == 0) {
                    banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + ($.trim(lanager[node.resourceName]) == "" || typeof (lanager[node.resourceName]) == "undefined" ? node.resourceName : lanager[node.resourceName]) + '</p></a></li>';
                }
            }
            var qqHtml = '<li><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin='
                + baseset.qq1
                + '&menu=yes"> <img border="0" src="/Content/images/qq.png" /><p>'
                + lanager[baseset.qName1]
                + '</p></a></li>'
                + '<li><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin='
                + baseset.qq2
                + '&menu=yes"> <img border="0" src="/Content/images/qq.png" /><p>'
                + lanager[baseset.qName2]
                + '</p></a></li>'
            $("#messageCenteritemCont").html(banHtml + qqHtml);
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#UAricledatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UArticle/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            html += '<li><a href="#UArticDetail/' + dto.id + '">' + dto.title + '</a><time>' + dto.addTime + '</time></li>';
                        }
                        $("#UAricleitemList").append(html);
                    }, function () {
                        $("#UAricleitemList").append('<p class="dropload-noData">' + lanager["暂无数据"] + '</p>');
                    });
            }
        });
        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#UAricleitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});