
define(['text!UWealthscheme.html', 'jquery'], function (UWealthscheme, $) {

    var controller = function (id) {

        //设置标题
        var titles = lanager["财富方案"];
        $("#title").html(titles);
        var pageHtml = UWealthscheme;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);
        //$("#title").html("财富方案");
        //appView.html(UWealthscheme);


        utils.AjaxPostNotLoadding("/User/UWealthscheme/InitView", {}, function (result) {
            if (result.status == "success") {
                var dto = result.result;
                //$("#addTime").html(utils.changeDateFormat(dto.addTime));
                $("#contTitle").html(dto.title);
                $("#cont").html(dto.content);
            } else {
                utils.showErrMsg(result.msg);
            }
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});