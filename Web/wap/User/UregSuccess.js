
define(['text!UregSuccess.html', 'jquery'], function (UregSuccess, $) {

    var controller = function (name) {

        var titles = lanager["注册成功"];
        $("#title").html(titles);
        var pageHtml = UregSuccess;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        $("#userId").html("<span>会员编号：</span>" + regSuccess.userId);
        $("#regmoney").html("<span>注册金额：</span>￥" + regSuccess.regMoney);
        $("#ulevel").html("<span>注册级别：</span>" + cacheMap["ulevel"][regSuccess.uLevel]);

        controller.onRouteChange = function () {
        };
    };

    return controller;
});