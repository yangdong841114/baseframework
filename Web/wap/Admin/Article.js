
define(['text!Article.html', 'jquery'], function (Article, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("公告管理")
        appView.html(Article);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        var editor = null;

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //保存或更新
        saveArticle = function () {
            if ($("#artTitle").val() == 0) {
                utils.showErrMsg("请输入主题");
            }
            else if (editor.getText() == 0) {
                utils.showErrMsg("请输入内容");
            } else {
                utils.AjaxPost("/Admin/Article/SaveByUeditor", { content: utils.getEditorHtml(editor), title: $("#artTitle").val(), id: $("#id").val() == 0 ? "" : $("#id").val() }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            }
        }

        //发布公告按钮
        $("#deployArticleBtn").bind("click", function () {
            $("#prompTitle").html("发布公告");
            $("#prompCont").empty();
            var contHtml = '<ul><li><dl><dt>主题</dt><dd><input type="text" required="required" class="entrytxt" id="artTitle" placeholder="请输入主题" /><span class="erase"><i class="fa fa-times-circle-o"></i></span>' +
            '</dd></dl><div style="width:98%;padding-left:1%;"><div id="editor" style="height:25rem;" ></div></div>' +
            '</li></ul><input type="hidden" id="id"/>';
            $("#prompCont").html(contHtml);
            utils.CancelBtnBind();
            editor = new Quill("#editor", {
                modules: {
                    toolbar: utils.getEditorToolbar()
                },
                theme: 'snow'
            });
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                saveArticle();
            })
            utils.showOrHiddenPromp();
        });

        //编辑公告
        editRecord = function (id) {
            var dto = detailList[id];
            $("#prompTitle").html("编辑公告");
            $("#prompCont").empty();
            var contHtml = '<ul><li><dl><dt>主题</dt><dd><input type="text" required="required" value="' + dto.title+ '" class="entrytxt" id="artTitle" placeholder="请输入主题" /><span class="erase"><i class="fa fa-times-circle-o"></i></span>' +
            '</dd></dl><div style="width:98%;padding-left:1%;"><div id="editor" style="height:25rem;" ></div></div>' +
            '</li></ul><input type="hidden" id="id" value="'+id+'"/>';
            $("#prompCont").html(contHtml);
            utils.CancelBtnBind();
            editor = new Quill("#editor", {
                modules: {
                    toolbar: utils.getEditorToolbar()
                },
                theme: 'snow'
            });
            utils.setEditorHtml(editor, dto.content);
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                saveArticle();
            })
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();

        //置顶
        topMethod = function (id) {
            utils.AjaxPost("/Admin/Article/SaveTop", { id: id }, function (result) {
                if (result.status == "success") {
                    utils.showSuccessMsg("置顶成功");
                    searchMethod();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        //删除
        deleteRecord = function (id) {
            $("#prompTitle").html("确定删除吗");
            $("#prompCont").empty();
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/Article/Delete", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        //关闭按钮
        $("#articBackBtn").bind("click", function () {
            $("#detailDiv").css("display", "none");
            $("#mainDiv").css("display", "block"); 
        })

        //详情
        toDetail = function (id) {
            $("#mainDiv").css("display", "none");
            $("#detailDiv").css("display", "block");
            var dto = detailList[id];
            $("#contTitle").empty();
            $("#contAddTime").empty();
            $("#cont").empty();
            $("#contTitle").html(dto.title);
            $("#contAddTime").html(dto.addTime);
            $("#cont").html(dto.content);
        }

        var detailList = {};

        var dropload = $('#Articledatalist').dropload({
            scrollArea: window,
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Article/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"  style="overflow: hidden"><time>' + dto.addTime + '</time><span class="sum">' + dto.title + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga4">' +
                            '<li><button class="sdelbtn" onclick="editRecord(\'' + dto.id + '\')">编辑</button></li>' +
                            '<li><button class="seditbtn" onclick=\'topMethod(' + dto.id + ')\'>置顶</button></li>' +
                            '<li><button class="sdelbtn" onclick="deleteRecord(\'' + dto.id + '\')">删除</button></li>' +
                            '<li><button class="smallbtn" onclick="toDetail(\'' + dto.id + '\')">详情预览</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>主题</dt><dd>' + dto.title + '</dd></dl><dl><dt>发布日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '</div></li>';
                            detailList[dto.id] = dto;
                        }
                        $("#ArticleitemList").append(html);
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ArticleitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["title"] = $("#title2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        ////初始化编辑器
        //var editor = new baidu.editor.ui.Editor({
        //    UEDITOR_HOME_URL: '/Content/ueditor/',//配置编辑器路径
        //    iframeCssUrl: '/Content/ueditor/themes/iframe.css',//样式路径
        //    //initialContent: '欢迎使用ueditor',//初始化编辑器内容
        //    autoHeightEnabled: true,//高度自动增长
        //    minFrameHeight: 350, //最小高度
        //    initialFrameHeight: 350,    // 高度
        //    elementPathEnabled: false,
        //    zIndex:10000,
        //    autoHeightEnabled: false,
        //    //initialContent: cont,
        //    saveInterval: 5000          //自动保存时间间隔
        //});

        //editor.render('editor');


        ////保存按钮
        //$("#saveBtn").bind("click", function () {
        //    if ($("#title").val() == 0) {
        //        utils.showErrMsg("请输入主题");
        //    }
        //    else if (!editor.hasContents()) {
        //        utils.showErrMsg("请输入内容");
        //    } else {
        //        utils.AjaxPost("Article/SaveByUeditor", { content: editor.getContent(), title: $("#title").val(), id: $("#id").val() == 0 ? "" : $("#id").val() }, function (result) {
        //            if (result.status == "success") {
        //                utils.showSuccessMsg("操作成功");
        //                $('#dlg').dialog("close");
        //                queryGrid();
        //            } else {
        //                utils.showErrMsg(result.msg);
        //            }
        //        });
        //    }
        //})

        ////打开明细
        //openDetailDialog = function (row) {
        //    $("#title").val(row.title);
        //    $("#id").val(row.id);
        //    editor.execCommand('cleardoc');
        //    editor.setContent(row.content);
        //    $('#dlg').dialog({ title: '修改公告' }).dialog("open");
        //}

        ////初始化表格
        //var grid = utils.newGrid("dg", {
        //    columns: [[
        //     {
        //         field: '_title', title: '主题', width: '70%', formatter: function (val, row, index) {
        //             return '<a href="javascript:void(0);" rowIndex ="' + index + '" class="titleOpen" >' + row.title + '</a>';
        //         }
        //     },
        //     { field: 'addTime', title: '发布日期', width: '15%' },
        //     {
        //         field: '_deleteRecord', title: '' + cz + '', width: '15%', formatter: function (val, row, index) {
        //             var cont = "";
        //             if (row.isTop == 1) {
        //                 cont = '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordTop" >置顶</a>&nbsp;&nbsp;';
        //             } else {
        //                 cont = '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordCancelTop" >取消置顶</a>&nbsp;&nbsp;';
        //             }
        //             cont += '<a href="javascript:void(0);" rowIndex ="' + index + '" class="recordEdit" >' + bj + '</a>&nbsp;&nbsp;' +
        //                    '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >' + sc + '</a>';
        //             return cont;
        //         }
        //     }
        //    ]],
        //    toolbar: [{
        //        text:"发布文章",
        //        iconCls: 'icon-add',
        //        handler: function () {
        //            //清空表单数据
        //            $("#title").val("");
        //            $("#id").val("");
        //            editor.execCommand('cleardoc');
        //            $('#dlg').dialog({ title: '发布公告' }).dialog("open");
        //        }
        //    }
        //    ],
        //    url: "Article/GetListPage"
        //}, null, function (data) {
        //    if (data && data.rows) {
        //        for (var i = 0; i < data.rows.length; i++) {
        //            data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
        //        }
        //    }
        //    return data;
        //}, function () {

        //    //置顶
        //    $(".recordTop").each(function (index, ele) {
        //        var dom = $(this);
        //        dom.on("click", function () {
        //            utils.confirm("确定置顶吗？", function () {
        //                var dataId = dom.attr("dataId");
        //                utils.AjaxPost("Article/SaveTop", { id: dataId }, function (result) {
        //                    if (result.status == "success") {
        //                        utils.showSuccessMsg("置顶成功");
        //                        queryGrid();
        //                    } else {
        //                        utils.showErrMsg(result.msg);
        //                    }
        //                });
        //            });
        //        })
        //    })

        //    //取消置顶
        //    $(".recordCancelTop").each(function (index, ele) {
        //        var dom = $(this);
        //        dom.on("click", function () {
        //            utils.confirm("确定取消置顶吗？", function () {
        //                var dataId = dom.attr("dataId");
        //                utils.AjaxPost("Article/CancelTop", { id: dataId }, function (result) {
        //                    if (result.status == "success") {
        //                        utils.showSuccessMsg("取消成功");
        //                        queryGrid();
        //                    } else {
        //                        utils.showErrMsg(result.msg);
        //                    }
        //                });
        //            });
        //        })
        //    })

        //    //点击标题打开文章明细
        //    $(".titleOpen").each(function (index, ele) {
        //        var dom = $(this);
        //        dom.on("click", function () {
        //            var rows = grid.datagrid("getRows");
        //            var index = dom.attr("rowIndex");
        //            var row = rows[index];
        //            openDetailDialog(row);
        //        })
        //    })

        //    //点击编辑按钮编辑
        //    $(".recordEdit").each(function (index, ele) {
        //        var dom = $(this);
        //        dom.on("click", function () {
        //            var rows = grid.datagrid("getRows");
        //            var index = dom.attr("rowIndex");
        //            var row = rows[index];
        //            openDetailDialog(row);
        //        })
        //    })

        //    //删除
        //    $(".recordDelete").each(function (index, ele) {
        //        var dom = $(this);
        //        dom.on("click", function () {
        //            utils.confirm("您确定要删除该记录？", function () {
        //                var dataId = dom.attr("dataId");
        //                utils.AjaxPost("Article/Delete", { id: dataId }, function (result) {
        //                    if (result.status == "success") {
        //                        utils.showSuccessMsg(result.msg);
        //                        queryGrid();
        //                    } else {
        //                        utils.showErrMsg(result.msg);
        //                    }
        //                });
        //            });
        //        })
        //    })
        //})



        controller.onRouteChange = function () {
        };
    };

    return controller;
});