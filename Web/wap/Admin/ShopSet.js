
define(['text!ShopSet.html', 'jquery'], function (ShopSet, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("设置报单中心")
        appView.html(ShopSet);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        utils.CancelBtnBind();

        //绑定添加按钮
        $("#setShopBtn").bind("click", function () {
            $("#userName").val("");
            $("#userId").val("");
            //会员编号绑定鼠标离开事件
            $("#userId").unbind();
            $("#userId").bind("blur", function () {
                $("#userName").val("");
                if ($("#userId").val()!=0) {
                    utils.AjaxPostNotLoadding("/Admin/ShopSet/GetModelMsg", { userId: $("#userId").val() }, function (result) {
                        if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                            $("#userName").val(result.msg);
                        } else {
                            $("#userName").val(result.map["userName"]);
                        }
                    });
                }
            })

            $("#sureBtn").unbind();
            //保存按钮
            $("#sureBtn").on("click", function () {
                if ($("#userId").val() == 0) {
                    utils.showErrMsg("请输入会员编号");
                }else {
                    var data = { userId: $("#userId").val()};
                    utils.AjaxPost("/Admin/ShopSet/Save", data, function (result) {
                        if (result.status == "success") {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg(result.msg);
                            searchMethod();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                }
            });
            utils.showOrHiddenPromp();
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ShopSetdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/ShopSet/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>设置日期</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.createUser + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#ShopSetitemList").append(html);
                    }, function () {
                        $("#ShopSetitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ShopSetitemList").empty();
            param["userId"] = $("#userId2").val();
            param["userName"] = $("#userName2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});