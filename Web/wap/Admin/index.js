
'use strict';

(function (win) {
    //配置baseUrl
    //var baseUrl = document.getElementById('main').getAttribute('data-baseurl');

    /*
     * 文件依赖
     */
    var config = {
        //baseUrl: baseUrl,           //依赖相对路径
        waitSeconds: 30, //加载js超时时间，30秒
        urlArgs: "r=0.02",
        map: {
            '*': {
                'css': '../../Content/js/css.min',
                'pcss': '../../Content/APP/css/font-awesome.min'
            }
        },
        paths: {                    //如果某个前缀的依赖不是按照baseUrl拼接这么简单，就需要在这里指出
            director: '../../Content/js/director.min',
            jquery: '../../Content/js/jquery-3.2.1.min',
            jquery_event_drag: '../../Content/APP/js/jquery.event.drag-1.5.min',
            touchSlider: '../../Content/APP/js/jquery.touchSlider',
            underscore: '../../Content/js/underscore',
            j_easyui: '../../Content/easyui/jquery.easyui.min',
            ztree: '../../Content/ztree/js/jquery.ztree.all.min',
            zui: '../../Content/js/zui',
            dropload: '../../Content/dropload/dropload.min',
            mobileSelect: '../../Content/APP/mobileSelect/mobileSelect',
            calendar: '../../Content/APP/js/LCalendar',
            quill: '../../Content/quill/quill',
            text: '../../Content/js/text'             //用于requirejs导入html类型的依赖
        },
        shim: {                     //引入没有使用requirejs模块写法的类库。
            underscore: {
                exports: '_'
            },
            jquery: {
                exports: '$'
            },
            director: {
                exports: 'Router'
            },
            ztree: {
                exports: 'ztree',
                deps: ['jquery',
                    'css!../../Content/ztree/css/zTreeStyle/zTreeStyleAdmin.css'
                ]
            },
            jquery_event_drag: {
                exports: 'jquery_event_drag',
                deps: ['jquery']
            },
            touchSlider: {
                exports: 'touchSlider',
                deps: ['jquery']
            },
            zui: {
                exports: 'zui',
                deps: ['jquery', 'css!../../Content/APP/css/zui_ui.css']
            },
            calendar: {
                exports: 'calendar',
                deps: ['jquery', 'css!../../Content/APP/css/LCalendar.css']
            },
            dropload: {
                exports: 'dropload',
                deps: ['jquery', 'css!../../Content/dropload/dropload.css']
            },
            mobileSelect: {
                exports: 'mobileSelect',
                deps: ['css!../../Content/APP/mobileSelect/mobileSelect.css']
            },
            quill: {
                exports: 'quill',
                deps: ['jquery', 'css!../../Content/quill/quill.css']
            },
            j_easyui: {
                exports: 'j_easyui',
                deps: ['jquery',
                    'css!../../Content/easyui/themes/material/easyui.css',
                    'css!../../Content/easyui/themes/icon.css',
                ]
            },
        }
    };

    require.config(config);
    require(['jquery', 'router', '../../Content/APP/js/CommonApp', 'touchSlider', 'mobileSelect', 'quill','j_easyui', 'zui',
             'css!../../Content/APP/css/font-awesome.min.css', 'calendar', 'dropload'], function ($, router, util, touchSlider, mobileSelect, quill) {

                 win.appView = $('#center');      //用于各个模块控制视图变化
                 win.$ = $;                          //暴露必要的全局变量，没必要拘泥于requirejs的强制模块化
                 win._ = _;
                 win.Quill = quill;
                 win.utils = util;
                 win.cacheMap = null;
                 win.cacheList = null;
                 win.pass2 = undefined;
                 win.pass3 = undefined;
                 win.mobileSelect = mobileSelect;
                 win.menuPass = {};
                 window.touchSlider = touchSlider;
                 window.router = router;
                 win.areaData = null;
                 win.routerMap = {};
                 win.regModel = {};
                 win.menuItem = {};

                 //注册404路由
                 router.toUrl("NotFound", "NotFound.js");
                 //注册无权限路由
                 router.toUrl("NoPermission", "NoPermission.js");
                 //注册安全密码路由
                 router.toUrl("Password2/:page", "Password2.js");
                 //注册交易密码路由
                 router.toUrl("Password3/:page", "Password3.js");
                 //注册公告详细
                 router.toUrl("ArticDetail/:id", "ArticDetail.js");
                 //注册角色已分配会员列表页面
                 router.toUrl("RoleMemberList/:id", "RoleMemberList.js");
                 //注册角色分配会员页面
                 router.toUrl("RoleAddMemberList/:id", "RoleAddMemberList.js");
                 router.toUrl("regSuccess", "regSuccess.js");

                 router.configure({
                     //未匹配到URL
                     notfound: function () { location.href = "#NotFound"; },
                     before: function () {
                         //util.DestoryAllPopover();
                         win.appView.empty();
                         win.appView.html('<div style="font-size:10rem;text-align:center;padding-top:40%;color:#999999;"><i class="fa  fa-cog fa-spin" aria-hidden="true"></i></div>');

                         //获取请求地址
                         var hash = window.location.hash;
                         hash = hash.substring(1, hash.length);
                         var param = undefined;
                         if (hash.indexOf("/") != -1) {
                             param = hash.substring(hash.indexOf("/") + 1, hash.length);
                             hash = hash.substring(0, hash.indexOf("/"));
                         }
                         //是否需要校验安全密码
                         if (win.menuPass && win.menuPass[hash] == 2 && !win.pass2 && hash.indexOf("Password2") == -1 && hash.indexOf("Password3") == -1) {
                             var url = "#Password2/" + hash;
                             if (param) { url = url + "." + param; }
                             location.href = url;
                             return false;
                         }
                             //是否需要校验交易密码
                         else if (win.menuPass && win.menuPass[hash] == 3 && !win.pass3 && hash.indexOf("Password2") == -1 && hash.indexOf("Password3") == -1) {
                             var url = "#Password3/" + hash;
                             if (param) { url = url + "." + param; }
                             location.href = url;
                             return false;
                         }
                     },
                 });

                 //返回按钮
                 //$("#backBtn").on('click', function () {
                 //    history.go(-1);
                 //});


                 util.AjaxPostNotLoadding("/Admin/ManageWeb/GetBaseData", {}, function (result) {
                     if (result.status == "fail") {
                         util.utils.showErrMsg("加载基础数据失败");
                         return;
                     }
                     $("#header_loginUserId").html("欢迎您：" + result.map["userId"]);
                     var baseset = result.map["baseSet"];      //基础设置
                     var areaMap = result.map["area"];
                     if (areaMap) {
                         win.areaData = areaMap[0];
                         for (var i = 0; i < win.areaData.length; i++) {
                             var dto = win.areaData[i];
                             if (areaMap[dto.id]) {
                                 dto["childs"] = areaMap[dto.id];
                                 for (var j = 0; j < dto["childs"].length; j++) {
                                     var child = dto["childs"][j];
                                     if (areaMap[child.id]) {
                                         child["childs"] = areaMap[child.id];
                                     }
                                 }
                             }
                         }
                     }
                    

                     //copyright
                     //$("#copyright").html(baseset.copyright);
                     //网站名称
                     $(document).attr("title", baseset.sitename);

                     var dataCache = result.map["cacheData"];  //数据字典缓存数据
                     win.cacheList = dataCache; //list形式缓存
                     //map形式缓存
                     if (dataCache != null) {
                         win.cacheMap = {};
                         for (name in dataCache) {
                             win.cacheMap[name] = {};
                             var list = dataCache[name];
                             if (list != null && list.length > 0) {
                                 for (var i = 0; i < list.length; i++) {
                                     var obj = list[i];
                                     win.cacheMap[name][obj.id] = obj.name;
                                 }
                             }
                         }
                     }

                     //初始化菜单start
                     util.AjaxPostNotLoadding("/Admin/ManageWeb/GetWapMenu", {}, function (result) {
                         if (result.status == "success") {
                             //注册主页路由
                             router.toUrl("main", "main.js");
                             var di = result.map;
                             var list = di.frist;
                             var allList = di.all;
                             var html = "";
                             var count = 0;
                             for (var i = 0; i < list.length; i++) {
                                 var node = list[i];
                                 var url = node.curl && node.curl != 'null' ? (node.curl + "").replace("Admin/", "") : "";
                                 menuItem[node.id] = [];
                                 count++;
                                 html += '<li class="menuTop" id="menuTop' + i + '"><a href="javascript:utils.adminMenuClick(\'#' + url + '/' + node.id + '\',' + i + ')">' + node.resourceName + '</a></li>';
                             }
                             //注册路由
                             for (var i = 0; i < allList.length; i++) {
                                 var node = allList[i];
                                 if (menuItem[node.parentResourceId]) {
                                     menuItem[node.parentResourceId].push(node);
                                 }
                                 var url = node.curl && node.curl != 'null' ? (node.curl + "").replace("Admin/", "") : "";
                                 if (url != "") {
                                     win.menuPass[url] = node.pass;
                                     router.toUrl(url, url + ".js");
                                     router.toUrl(url + "/:id", url + ".js");
                                 }
                             }
                             $("#buttomMenu").addClass("tga"+count);
                             $("#buttomMenu").html(html);

                             

                             location.href = "#main";
                         }
                     });

                 });

             });

})(window);