
define(['text!MTransfer.html', 'jquery'], function (MTransfer, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("转账查询")
        appView.html(MTransfer);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //初始化转账类型下拉框
        var typeList = $.extend(true, [], cacheList["MTransfer"]);
        typeList.splice(0, 0, { id: "", name: '全部' });
        utils.InitMobileSelect('#typeIdName', '' + hyjb + '', typeList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#typeIdName").val(data[0].name);
            $("#typeId").val(data[0].id);
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#MTransferdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/MTransfer/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"],'minute');
                            rows[i]["typeId"] = cacheMap["MTransfer"][rows[i].typeId];

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" style="overflow:hidden;" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.typeId + '</span>';
                            html += '<span class="sum">' + dto.epoints + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>转账类型</dt><dd>' + dto.typeId + '</dd></dl><dl><dt>转出账户</dt><dd>' + dto.fromUserId + '</dd></dl>' +
                            '<dl><dt>会员姓名</dt><dd>' + dto.fromUserName + '</dd></dl><dl><dt>转入账户</dt><dd>' + dto.toUserId + '</dd></dl>' +
                            '<dl><dt>会员姓名</dt><dd>' + dto.toUserName + '</dd></dl><dl><dt>转账金额</dt><dd>' + dto.epoints + '</dd></dl>' +
                            '<dl><dt>转账日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#MTransferitemList").append(html);
                    }, function () {
                        $("#MTransferitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#MTransferitemList").empty();
            param["fromUserId"] = $("#fromUserId").val();
            param["toUserId"] = $("#toUserId").val();
            param["typeId"] = $("#typeId").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/MTransfer/ExportTransferExcel";
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});