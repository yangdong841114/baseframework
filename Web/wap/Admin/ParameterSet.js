
define(['text!ParameterSet.html', 'jquery'], function (ParameterSet, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("参数设置")
        var inputs = {};

        utils.AjaxPostNotLoadding("/Admin/ParameterSet/InitView", {}, function (result) {
            appView.html(ParameterSet);
            //初始化赋值
            var rt = result.map;
            $(".entryinfo input").each(function () {
                inputs[this.id] = "";
            })
            if (rt != null) {
                for (name in rt) {
                    if (inputs[name] != undefined) {
                        var obj = rt[name];
                        if (name == "isRegister") {
                            var isRegisterList = [{ id: 1, value: "是" }, { id: 0, value: "否" }];
                            utils.InitMobileSelect('#isRegister', '是否开通会员注册', isRegisterList, null, [0], null, function (indexArr, data) {
                                $("#isRegister").val(data[0].value);
                            });
                        } else if (name == "isMoney") {
                            var isMoneyList = [{ id: 1, value: "是" }, { id: 0, value: "否" }];
                            utils.InitMobileSelect('#isMoney', '是否开通会员提现', isMoneyList, null, [0], null, function (indexArr, data) {
                                $("#isMoney").val(data[0].value);
                            });
                        } else if (name == "isMobileMessage") {
                            var isMobileMessageList = [{ id: 1, value: "是" }, { id: 0, value: "否" }];
                            utils.InitMobileSelect('#isMobileMessage', '是否开启短信验证', isMobileMessageList, null, [0], null, function (indexArr, data) {
                                $("#isMobileMessage").val(data[0].value);
                            });
                        }
                        $("#" + name).val(obj.paramValue);
                    }
                }
            }

            //保存
            $("#saveParamBtn").on('click', function () {
                var dt = {}
                var checked = true;
                var i = 0;
                $(".entryinfo input").each(function () {
                    var dom = $(this);
                    dt["list[" + i + "].paramCode"] = this.id;
                    dt["list[" + i + "].paramValue"] = dom.val();
                    if (dom.val() == "" || $.trim(dom.val()).length == 0) {
                        if (dom.attr("name") != "userIdPrefix") {
                            utils.showErrMsg("请输入参数");
                            dom.focus();
                            checked = false;
                            return false;
                        }
                    }
                    i++;
                })
                //提交表单
                if (checked) {
                    utils.AjaxPost("/Admin/ParameterSet/Save", dt, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg("保存成功！");
                        }
                    });

                } 

            })
        });

        

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});