
define(['text!AccountUpdate.html', 'jquery'], function (AccountUpdate, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("账户充值")
        appView.html(AccountUpdate);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        utils.CancelBtnBind();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //账户类型
        var journalClassList = $.extend(true, [], cacheList["JournalClass"]);
        //journalClassList.splice(0, 0, { id: "", name: '全部' });
        utils.InitMobileSelect('#accountTypeName', '选择账户类型', journalClassList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#accountTypeName").val(data[0].name);
            $("#accountType").val(data[0].id);
        });
        
        //会员编号绑定鼠标离开事件
        $("#userId").bind("blur", function () {
            $("#userName").val("");
            if ($("#userId").val() != 0) {
                utils.AjaxPostNotLoadding("/Admin/AccountUpdate/GetModelMsg", { userId: $("#userId").val() }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#userName").val(result.msg);
                    } else {
                        $("#userName").val(result.map["userName"]);
                    }
                });
            }
        })

        //确定提交按钮
        $("#sureBtn").bind("click", function () {
            if ($("#userId").val() == 0) {
                utils.showErrMsg("请输入会员编号");
            } else if ($("#accountType").val() == 0) {
                utils.showErrMsg("请选择账户类型");
            } else if ($("#epoints").val() == 0) {
                utils.showErrMsg("请输入充值金额");
            }else {
                var data = { userId: $("#userId").val(), accountType: $("#accountType").val(), epoints: $("#epoints").val(), remark: $("#remark").val(), isBackRound: 0 };
                utils.AjaxPost("/Admin/AccountUpdate/Save", data, function (result) {
                    if (result.status == "fail") {

                        if (result.msg.indexOf("将变成负数") != -1) {
                            //确认后再提交一次
                            utils.confirm(result.msg, function () {
                                data = { userId: $("#userId").val(), accountType: $("#accountType").val(), epoints: $("#epoints").val(), remark: $("#remark").val(), isBackRound: 0, isFs: "y" };
                                utils.AjaxPost("/Admin/AccountUpdate/Save", data, function (result) {
                                    if (result.status == "fail") {
                                        utils.showErrMsg(result.msg);
                                    }
                                    else {
                                        utils.showOrHiddenPromp();
                                        utils.showSuccessMsg(result.msg);
                                        searchMethod();
                                    }
                                });
                                //确认后再提交一次
                            });
                        }
                        else
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    }
                });
            }
        })

        //添加按钮
        $("#addAccountBtn").bind("click", function () {
            utils.showOrHiddenPromp();
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#AccountUpdatedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/AccountUpdate/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["accountType"] = cacheMap["JournalClass"][rows[i].accountType];
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            dto.remark = (dto.remark && dto.remark != "null") ? dto.remark : "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '<span class="sum">' + dto.epoints + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>充值金额</dt><dd>' + dto.epoints + '</dd></dl><dl><dt>账户类型</dt><dd>' + dto.accountType + '</dd></dl>' +
                            '<dl><dt>充值日期</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.addUser + '</dd></dl>' +
                            '<dl><dt>备注</dt><dd>' + dto.remark + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#AccountUpdateitemList").append(html);
                    }, function () {
                        $("#AccountUpdateitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#AccountUpdateitemList").empty();
            param["userId"] = $("#userId2").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/AccountUpdate/ExportAccountUpdateExcel";
        })


        controller.onRouteChange = function () {
        };
    };

    return controller;
});