
define(['text!ReMemberEdit.html', 'jquery'], function (ReMemberEdit, $) {

    var controller = function (name) {

        //切换选项卡
        changeTab = function (index) {
            $("#tabLi1").removeClass("active");
            $("#tabLi2").removeClass("active");
            $("#tabDiv1").css("display", "none");
            $("#tabDiv2").css("display", "none");
            $("#tabDiv" + index).css("display", "block");
            $("#tabLi" + index).addClass("active");
        }

        //设置标题
        $("#title").html("修改推荐关系");
        appView.html(ReMemberEdit);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        utils.CancelBtnBind();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //添加记录按钮
        $("#addRecordBtn").bind("click", function () {
            //会员编号绑定鼠标离开事件
            $("#userId").unbind();
            $("#userId").bind("blur", function () {
                $("#userName").val("");
                $("#oldReName").val("");
                if($("#userId").val()!=0){
                    utils.AjaxPostNotLoadding("/Admin/ReMemberEdit/GetModelMsg", { userId: $("#userId").val() }, function (result) {
                        if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                            $("#userName").val(result.msg);
                        } else {
                            $("#userName").val(result.map["userName"]);
                            $("#oldReName").val(result.map["reName"]);
                        }
                    });
                }
            })

            //新推荐人编号绑定鼠标离开事件
            $("#newReName").unbind();
            $("#newReName").bind("blur", function () {
                $("#newReUserName").val("");
                if ($("#newReName").val()!=0) {
                    utils.AjaxPostNotLoadding("/Admin/ReMemberEdit/GetModelMsg", { userId: $("#newReName").val() }, function (result) {
                        if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                            $("#newReUserName").val(result.msg);
                        } else {
                            $("#newReUserName").val(result.map["userName"]);
                        }
                    });
                }
            })

            //确认按钮点击事件
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                if ($("#userId").val() == 0) {
                    utils.showErrMsg("请输入会员编号");
                } else if ($("#newReName").val() == 0) {
                    utils.showErrMsg("请输入新推荐人编号");
                } else {
                    var data = { userId: $("#userId").val(), newReName: $("#newReName").val()};
                    utils.AjaxPost("/Admin/ReMemberEdit/Save", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg(result.msg);
                            searchMethod();
                        }
                    });
                }
            });
            utils.showOrHiddenPromp();
        })

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ReMemberEditDataList').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/ReMemberEdit/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            dto.regMoney = dto.regMoney ? dto.regMoney : 0;
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '&nbsp;<span class="sum">' + dto.newReName + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>旧推荐人</dt><dd>' + dto.oldReName + '</dd></dl><dl><dt>新推荐人</dt><dd>' + dto.newReName + '</dd></dl>' +
                            '<dl><dt>修改日期</dt><dd>' + dto.addTime + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.createUser + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#ReMemberEditItemList").append(html);
                    }, function () {
                        $("#ReMemberEditItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ReMemberEditItemList").empty();
            param["userId"] = $("#userId2").val();
            param["userName"] = $("#userName2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});