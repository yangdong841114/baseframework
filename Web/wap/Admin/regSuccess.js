
define(['text!regSuccess.html', 'jquery'], function (regSuccess, $) {

    var controller = function (name) {

        $("#title").html("注册成功");
        appView.html(regSuccess);

        $("#userId").html("<span>会员编号：</span>" + regModel.userId);
        $("#regmoney").html("<span>注册金额：</span>￥" + regModel.regMoney);
        $("#ulevel").html("<span>注册级别：</span>" + cacheMap["ulevel"][regModel.uLevel]);

        controller.onRouteChange = function () {
        };
    };

    return controller;
});