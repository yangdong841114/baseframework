
define(['text!UArticleDetail.html', 'jquery', 'j_easyui', 'zui'], function (UArticleDetail, $) {

    var controller = function (id) {

        appView.html(UArticleDetail);


        utils.AjaxPostNotLoadding("UArticle/GetModel", { id: id }, function (result) {
            if (result.status == "success") {
                var dto = result.result;
                $("#addTime").html(utils.changeDateFormat(dto.addTime));
                $("#title").html(dto.title);
                $("#cont").html(dto.content);
            } else {
                utils.showErrMsg(result.msg);
            }
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});