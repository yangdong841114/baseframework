
define(['text!ULoginLog.html', 'jquery', 'j_easyui', 'zui'], function (ULoginLog, $) {

    var controller = function (name) {

        appView.html(ULoginLog);

        var flagObj = { "0": "成功", "1": "失败（帐号不存在）", "2": "失败（密码验证失败）", "3": "失败（帐号未开通）", "4": "失败（帐号被冻结）", "5": "失败（帐号被登录锁定）" };

        //初始化表格
        var grid = utils.newGrid("dg", {
            title:"登录日志",
            columns: [[
             { field: 'userId', title: '登录帐号', width: '13%' },
             { field: 'userName', title: '姓名', width: '13%' },
             { field: 'loginIp', title: 'IP', width: '13%' },
             { field: 'flag', title: '状态', width: '16%' },
             { field: 'loginTime', title: '登录时间', width: '16%' },
             { field: 'mulx', title: '描述', width: '30%' },
            ]],
            url: "ULoginLog/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["loginTime"] = utils.changeDateFormat(data.rows[i]["loginTime"]);
                    data.rows[i]["flag"] = flagObj[data.rows[i]["flag"]];
                }
            }
            return data;
        })



        controller.onRouteChange = function () {
        };
    };

    return controller;
});