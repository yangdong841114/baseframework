
define(['text!Charge.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Charge, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "充值查询");
        appView.html(Charge);

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#Charge", toUid: 0 }, function () { });

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '100' },
                {
                    field: '_operate', title: '操作', width: '100', align: 'center', formatter: function (val, row, index) {
                        if (row.ispay == 1) {
                            return '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildEdit" >确认</a>&nbsp;&nbsp;' +
                                    '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildTo" >删除</a>';
                        } else {
                            return "";
                        }
                    }
                }]],
            columns: [[
                { field: 'userName', title: '会员姓名', width: '100' },
                { field: 'fromBank', title: '汇出银行', width: '100' },
                { field: 'epoints', title: '充值金额', width: '80' },
                { field: 'bankTime', title: '汇款时间', width: '130' },
                {
                    field: '_img', title: '汇款凭证', width: '100', formatter: function (val, row, index) {
                        return '<img data-toggle="lightbox" src="' + row.imgUrl + '" data-image="' + row.imgUrl + '" data-caption="汇款凭证" class="img-thumbnail" alt="" width="100">';
                    }   
                },
                { field: 'toBank', title: '汇入银行', width: '100' },
                { field: 'bankCard', title: '银行卡号/支付订单号', width: '160' },
                { field: 'bankUser', title: '开户名/支付帐号', width: '140' },
                { field: 'addTime', title: '充值日期', width: '130' },
                { field: 'status', title: '状态', width: '130' }
            ]],
            url: "Charge/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    //data.rows[i]["bankTime"] = utils.changeDateFormat(data.rows[i]["bankTime"]);
                    data.rows[i]["status"] = data.rows[i].ispay == 1 ? "待审核" : "已通过";
                }
                if (data && data.footer)
                {
                    var html_totalepoints = " 已审核总金额:XX.XX 待审核总金额:XX.XX";
                    for (var i = 0; i < data.footer.length; i++) {
                        var dto = data.footer[i];
                        html_totalepoints = '已审核总金额:' + dto.epointsPay + ' &nbsp;  待审核总金额:' + dto.epointsNotpay + '';
                    }
                    $("#totalepoints").html(html_totalepoints);
                }
            }
            return data;
        }, function () {
            $(".img-thumbnail").each(function (index, ele) {
                $(this).lightbox();
            })

            //行确认按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定审核通过吗？",function(){
                        utils.AjaxPost("Charge/AuditTakeCash", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
            //行删除按钮
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定删除该记录吗？",function(){
                        utils.AjaxPost("Charge/CancelTakeCash", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("操作成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "Charge/ExportChargeExcel";
        })

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});