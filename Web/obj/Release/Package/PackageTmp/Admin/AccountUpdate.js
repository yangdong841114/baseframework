
define(['text!AccountUpdate.html', 'jquery', 'j_easyui', 'datetimepicker'], function (AccountUpdate, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "账户充值");
        appView.html(AccountUpdate);

        //初始化表单区域
        $('#dlg').dialog({
            title: '添加充值记录',
            width: 500,
            height: 400,
            cache: false,
            modal: true
        })

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });


        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //初始化账户类型select
        var cList = cacheList["JournalClass"];
        if (cList && cList.length > 0) {
            $("#accountType").empty();
            $("#accountType").append("<option value='0'>--请选择--</option>");
            for (var i = 0; i < cList.length; i++) {
                $("#accountType").append("<option value='" + cList[i].id + "'>" + cList[i].name + "</option>");
            }
        }

        //清除错误样式
        $(".form-control").each(function () {
            var dom = $(this);
            dom.on("focus", function () {
                dom.removeClass("inputError");
            })
        });

        //添加表单-会员编号绑定鼠标离开事件
        $("#userId").on("blur", function () {
            $("#userName").val("");
            if ($("#userId").val()!=0) {
                utils.AjaxPostNotLoadding("AccountUpdate/GetModelMsg", { userId: $("#userId").val() }, function (result) {
                    if (result.status == "fail" && result.msg == "会员不存在") {
                        $("#userName").val(result.msg);
                    } else {
                        $("#userName").val(result.map["userName"]);
                    }
                });
            }

        })

        //保存按钮
        $("#saveBtn").bind("click", function () {
            var checked = true;
            $(".form-control").each(function () {
                var dom = $(this);
                if (dom.val() == 0 && dom[0].id != "remark") {
                    dom.addClass("inputError");
                    checked = false;
                }
            });
            if (checked) {
                var data = { userId: $("#userId").val(), accountType: $("#accountType").val(), epoints: $("#epoints").val(), remark: $("#remark").val(), isBackRound: 0 };
                utils.AjaxPost("AccountUpdate/Save", data, function (result) {
                    if (result.status == "fail") {
                        if (result.msg.indexOf("将变成负数")!=-1)
                        {
                            //确认后再提交一次
                            utils.confirm(result.msg, function () {
                                data = { userId: $("#userId").val(), accountType: $("#accountType").val(), epoints: $("#epoints").val(), remark: $("#remark").val(), isBackRound: 0, isFs: "y" };
                                utils.AjaxPost("AccountUpdate/Save", data, function (result) {
                                    if (result.status == "fail") {
                                        utils.showErrMsg(result.msg);
                                    }
                                    else {
                                        utils.showSuccessMsg(result.msg);
                                        //关闭弹出框
                                        $('#dlg').dialog("close");
                                        //重刷grid
                                        queryGrid();
                                    }
                                });
                                //确认后再提交一次
                            });
                        }
                        else
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        //关闭弹出框
                        $('#dlg').dialog("close");
                        //重刷grid
                        queryGrid();
                    }
                });
            }

            return false;
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '13%' }]],
            columns: [[
             { field: 'userName', title: '会员姓名', width: '13%' },
             { field: 'epoints', title: '充值金额', width: '13%' },
             { field: 'accountType', title: '账户类型', width: '13%' },
             { field: 'addTime', title: '充值日期', width: '13%' },
             { field: 'addUser', title: '操作人', width: '13%' },
             { field: 'remark', title: '备注', width: '22%' }
            ]],
            toolbar: [{
                text:"添加",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $('#editModalForm').form('reset');
                    $('#dlg').dialog({ title: '添加记录' }).dialog("open");
                }
            }
            ],
            url: "AccountUpdate/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["accountType"] = cacheMap["JournalClass"][data.rows[i].accountType];
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    var name = name == "userId2" ? "userId" : name;
                    queryObj[name] = val;
                }
            });
            grid.datagrid("options").queryParams = queryObj;
            grid.datagrid("reload");
        }


        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "AccountUpdate/ExportAccountUpdateExcel";
        })

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});