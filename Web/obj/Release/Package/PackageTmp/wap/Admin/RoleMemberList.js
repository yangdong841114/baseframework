
define(['text!RoleMemberList.html', 'jquery'], function (RoleMemberList, $) {

    var controller = function (roleId) {
        //设置标题
        $("#title").html("已分配会员")
        appView.html(RoleMemberList);
        $("#roleMemberRoleId").val(roleId);

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //全选按钮
        $("#roleMemberCheckAllBtn").bind("change", function () {
            var checked = this.checked;
            $(".roleMemberCheckBox").each(function () {
                this.checked = checked;
            });
        });

        //返回按钮
        $("#roleMemberBackBtn").bind("click", function () {
            history.go(-1);
        })

        //删除按钮
        $("#delRoleMemberBtn").bind("click", function () {

            var i = 0;
            var data = {};
            $(".roleMemberCheckBox").each(function () {
                var dom = $(this);
                if (this.checked) {
                    data["list[" + i + "]"] = dom.attr("dataId");
                    i++;
                }
            });
            if (i == 0) { utils.showErrMsg("请选择需要删除的记录！"); }
            else {
                $("#sureBtn").unbind();
                $("#sureBtn").bind("click", function () {
                    utils.AjaxPost("/Admin/RoleManage/DeleteRm", data, function (result) {
                        utils.showOrHiddenPromp();
                        if (result.msg == "success") {
                            utils.showSuccessMsg("删除成功");
                            searchMethod();
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                })
                utils.showOrHiddenPromp();
            }
        });

        this.param = utils.getPageData();

        //初始化角色会员列表
        param["id"] = $("#roleMemberRoleId").val();
        var dropload = $('#roleMemberDataList').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/RoleManage/GetRoleMemberPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["roleType"] = rows[i].roleType == 1 ? "前台角色" : "后台角色";

                            var dto = rows[i];
                            html += '<li><label><input class="operationche roleMemberCheckBox" type="checkbox" dataId="' + dto.rmId + '"/></label>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.userName + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>角色ID</dt><dd>' + dto.id + '</dd></dl><dl><dt>角色名称</dt><dd>' + dto.roleName + '</dd></dl>' +
                            '<dl><dt>角色类型</dt><dd>' + dto.roleType + '</dd></dl><dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl>' +
                            '<dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#roleMemberItemList").append(html);
                    }, function () {
                        $("#roleMemberItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });


        //查询方法
        searchMethod = function () {
            param.page = 1;
            param["id"] = $("#roleMemberRoleId").val();
            param["userId"] = $("#roleMemberUserId").val();
            $("#roleMemberItemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {

        };
    };

    return controller;
});