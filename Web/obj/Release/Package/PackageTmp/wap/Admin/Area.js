
define(['text!Area.html', 'jquery', 'ztree'], function (Area, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("省市区管理")

        //ztree对象引用
        var resourceTree = null;

        //保存成功时的回调函数
        //parentNode:添加节点的上级节点
        //updateNode:更新的原节点
        //newObj:添加或更新操作后返回的对象
        saveSuccessCallBack = function (parentNode, updateNode, newObj) {
            //存在updateNode表示是更新操作，否则是添加节点操作
            if (updateNode != null) {
                updateNode.name = newObj.name;
                resourceTree.updateNode(updateNode);
            } else {
                resourceTree.addNodes(parentNode, -1, newObj);
            }
        }

        var formHtml = '<input type="hidden" id="id" name="id" /><input type="hidden" id="parentId" name="parentId" />' +
            '<dl><dt>上级名称</dt><dd><p><span id="parent"></span></p></dd></dl>' +
            '<dl><dt>名称</dt><dd><input type="text" required="required" class="entrytxt" id="name" placeholder="请输入名称" /><span class="erase"><i class="fa fa-times-circle-o"></i></span></dd></dl>';

        //清空数据按钮
        clearData = function () {
            $(".erase").each(function () {
                var dom = $(this);
                dom.unbind();
                dom.bind("click", function () {
                    var prev = dom.prev();
                    dom.prev().val("");
                });
            });
        }

        //根据obj加载表单数据
        loadForm = function (obj) {
            if ($("#id") && obj["id"]) { $("#id").val(obj["id"]); }
            if ($("#parentId") && obj["parentId"]) { $("#parentId").val(obj["parentId"]); }
            if ($("#parent") && obj["parent"]) { $("#parent").html(obj["parent"]); }
            if ($("#name") && obj["name"]) { $("#name").val(obj["name"]); }
        }

        //获取表单数据
        getFormData = function () {
            var obj = {
                id: $("#id").val(), parentId: $("#parentId").val(), parent: $("#parent").html(), name: $("#name").val()
            }
            return obj;
        }

        //保存方法
        saveMethod = function (parentNode, updateNode) {
            if ($("#name").val() == 0) {
                utils.showErrMsg("请输入名称");
            } else {
                var data = getFormData();
                utils.AjaxPost("/Admin/Area/SaveOrUpdate", data, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        saveSuccessCallBack(parentNode, updateNode, result.result); //更新tree节点或添加节点
                        utils.showSuccessMsg(result.msg);
                    }
                })
            }
        }

        utils.AjaxPostNotLoadding("/Admin/Area/GetList", null, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                var saveMsg = "";      //保存时提示消息
                var parentNode = null; //插入tree节点的上级节点
                var updateNode = null; //更新tree节点时的原节点

                //ztree设置
                var treesetting = {
                    data: {
                        simpleData: {
                            enable: true,
                            idKey: "id", 	              // id编号字段名
                            pIdKey: "parentId",           // 父id编号字段名
                            rootPId: null
                        },
                        key: {
                            name: "name"    //显示名称字段名
                        }
                    },
                    view: {
                        //showLine: false,            //不显示连接线
                        dblClickExpand: false       //禁用双击展开
                    },
                    callback: {
                        onClick: function (e, treeId, node) {
                            if (node.isParent) {
                                resourceTree.expandNode(node);
                            }
                        }
                    }
                };

                //初始化布局，HTML
                appView.html(Area);

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                $('#resourceLayout').layout();

                //生成树
                resourceTree = $.fn.zTree.init($("#resourceTree"), treesetting, result.list);

                //编辑按钮
                $("#editBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择编辑节点的位置");
                    } else {
                        node = nodes[0];
                        $("#prompTitle").html("编辑" + node.name);
                        $("#prompCont").empty();
                        $("#prompCont").html(formHtml);
                        clearData();
                        //组合表单数据
                        var obj = {
                            id: node.id, name: node.name, parent: "无",
                            parentId: node.parentId
                        }
                        var parent = node.getParentNode();
                        if (parent != null) {
                            obj["parent"] = parent.name;
                        }

                        parentNode = parent;
                        updateNode = node;

                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () { saveMethod(parentNode, updateNode); });

                        loadForm(obj);
                        utils.showOrHiddenPromp();
                    }
                })

                //添加同级
                $("#addSameBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择添加同级节点的位置");
                    } else {
                        node = nodes[0];
                        $("#prompTitle").html("【"+node.name+"】添加同级");
                        $("#prompCont").empty();
                        $("#prompCont").html(formHtml);
                        clearData();
                        var obj = {
                            id: "", parentId: '0', parent: "无"
                        }
                        var parent = node.getParentNode();
                        if (parent != null) {
                            obj["parent"] = parent.name;
                            obj["parentId"] = parent.id;
                        }

                        //添加节点位置的上级节点
                        updateNode = null;
                        if (obj.parentId == "0") {
                            parentNode = null;
                        } else {
                            parentNode = parent;
                        }
                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () { saveMethod(parentNode, updateNode); });

                        loadForm(obj);
                        utils.showOrHiddenPromp();
                    }
                })

                //添加下级
                $("#addSubBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择上级节点");
                    } else {
                        node = nodes[0];
                        $("#prompTitle").html("【" + node.name + " 】添加下级");
                        $("#prompCont").empty();
                        $("#prompCont").html(formHtml);
                        clearData();
                        var obj = {
                            id: "", parentId: node.id, parent: node.name
                        }

                        //添加节点位置的上级节点
                        updateNode = null;
                        parentNode = node;

                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () { saveMethod(parentNode, updateNode); });

                        loadForm(obj);
                        utils.showOrHiddenPromp()
                    }
                })

                //删除
                $("#deleteBtn").bind("click", function () {
                    var nodes = resourceTree.getSelectedNodes();
                    if (!nodes || nodes.length == 0) {
                        utils.showErrMsg("请先选择需要删除的节点");
                    } else {
                        node = nodes[0];
                        $("#prompTitle").html("确定要删除该节点吗？");
                        $("#prompCont").empty();
                        $("#sureBtn").unbind();
                        $("#sureBtn").bind("click", function () {
                            utils.AjaxPost("/Admin/Area/Delete", { id: node.id }, function (result) {
                                utils.showOrHiddenPromp();
                                if (result.msg == "删除成功") {
                                    resourceTree.removeChildNodes(node);//清空当前节点的子节点
                                    resourceTree.removeNode(node);      //清空当前节点
                                    utils.showSuccessMsg("删除成功");

                                } else {
                                    utils.showErrMsg(result.msg);
                                }
                            })
                        });
                        utils.showOrHiddenPromp();
                    }
                })
            }
        });

        controller.onRouteChange = function () {
            //销毁ztree
            if (resourceTree) {
                resourceTree.destroy();
            }
        };
    };

    return controller;
});