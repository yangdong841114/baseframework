
define(['text!EpMyBuyer.html', 'jquery'], function (EpMyBuyer, $) {

    var controller = function (sid) {
        //设置标题
        $("#title").html("回购记录")
        appView.html(EpMyBuyer);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        var statusDto = { 0: "待买家付款", 1: "待卖家收款", 2: "已完成", 3: "已取消" };
        var statusList = [{ id: -1, value: "全部" }, { id: 0, value: "待买家付款" }, { id: 1, value: "待卖家收款" }, { id: 2, value: "已完成" }, { id: 3, value: "已取消" }]

        //初始化下拉框
        utils.InitMobileSelect('#flagName', '选择状态', statusList, null, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].value);
            $("#flag").val(data[0].id);
        });

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //取消买单
        cancelDetailRecord = function (id) {
            $("#prompTitle").html("确定取消吗？");
            $("#sureBtn").html("确定取消");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/EpMyBuyer/SaveCancel", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("操作成功");
                        searchMethod();
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //确认付款
        surePay = function (id) {
            $("#prompTitle").html("确定付款吗？");
            $("#sureBtn").html("确定");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/EpMyBuyer/SaveCancel", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg("操作成功");
                        searchMethod();
                    }
                });
            });
            utils.showOrHiddenPromp();
        }


        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#EpMyBuyerdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/EpDetail/GetListPage?sid=" + sid, param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["saddTime"] = utils.changeDateFormat(rows[i]["saddTime"]);
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = statusDto[rows[i]["flag"]];

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.buyNum + '</span>';
                            if (dto.status == "已完成") {
                                html += '<span class="status ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="status noship">' + dto.status + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div><div class="allinfo">';

                            if (dto.flag == 0) {
                                html += '<div class="btnbox"><ul class="tga1">' +
                                        '<li><button class="seditbtn" onclick=\'cancelDetailRecord(' + dto.id + ')\'>取消买单</button></li>' +
                                        '</ul></div>';
                            } else if (dto.flag == 1) {
                                html += '<div class="btnbox"><ul class="tga2">' +
                                        '<li><button class="seditbtn" onclick=\'cancelDetailRecord(' + dto.id + ')\'>取消买单</button></li>' +
                                        '<li><button class="sdelbtn" onclick=\'surePay(' + dto.id + ')\'>确认付款</button></li>' +
                                        '</ul></div>';
                            }

                            html += '<dl><dt>买单单号</dt><dd>' + dto.number + '</dd></dl><dl><dt>购买日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '<dl><dt>购买数量</dt><dd>' + dto.buyNum + '</dd></dl><dl><dt>应付金额</dt><dd>' + dto.payMoney + '</dd></dl>' +
                            '<dl><dt>状态</dt><dd>' + dto.status + '</dd></dl><dl><dt>挂卖日期</dt><dd>' + dto.saddTime + '</dd></dl>' +
                            '<dl><dt>卖家编号</dt><dd>' + dto.suserId + '</dd></dl><dl><dt>手机</dt><dd>' + dto.phone + '</dd></dl>' +
                            '<dl><dt>QQ</dt><dd>' + dto.qq + '</dd></dl><dl><dt>开户行</dt><dd>' + dto.bankName + '</dd></dl>' +
                            '<dl><dt>银行卡号</dt><dd>' + dto.bankCard + '</dd></dl><dl><dt>开户名</dt><dd>' + dto.bankUser + '</dd></dl>' +
                            '<dl><dt>开户支行</dt><dd>' + dto.bankAddress + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#EpMyBuyeritemList").append(html);
                    }, function () {
                        $("#EpMyBuyeritemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#EpMyBuyeritemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["suserId"] = $("#suserId").val();
            param["flag"] = $("#flag").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});