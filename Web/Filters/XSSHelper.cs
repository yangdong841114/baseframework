﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Web.Filters
{
    public static class XSSHelper
    {
        public static string XssFilter(string html)
        {
            string str = HtmlFilter(html);
            return str;
        }
        public static string HtmlFilter(string Htmlstring)
        {
            string result = Regex.Replace(Htmlstring, @"<[^>]*>", String.Empty);
            return result;
        }
    }
}