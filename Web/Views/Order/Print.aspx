﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html lang="zh">
<head runat="server">
    <title>打印订单</title>
    <script src="/Content/js/jquery-3.2.1.min.js" id="main"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            window.print();
        });
    </script>


    <style type="text/css">
        table td {
            padding: 5px 10px;
        }

        table.t1 {
            border: 1px solid #ddd;
        }

            table.t1 th, table.t1 td {
                border: 1px solid #ddd;
                color: #454545;
            }

            table.t1 th {
                background: #E8E5E5;
                height: 26px;
                line-height: 26px;
                font-weight: bold;
                text-align: center;
            }

            table.t1 td {
                padding-left: 10px;
                height: 26px;
                line-height: 26px;
                background: #fff;
            }
    </style>
</head>
<body>
    <%
        if (ViewData["list"] != null)
        {
            List<Model.OrderHeader> list = (List<Model.OrderHeader>)ViewData["list"];
            if (list != null && list.Count > 0)
            {
                foreach (Model.OrderHeader oh in list)
                {
                    Response.Write("<table cellpadding=\"0\" cellspacing=\"0\" style='font-size:12px;'><tr><td>订单号：" + oh.id + "</td>" +
                       "<td>订单类型：复消订单</td>" +
                       "<td>总金额：" + oh.productMoney + " </td>" +
                       "<td>购买日期：" + (string.Format("{0:yyyy-MM-dd}", oh.orderDate)) + "</td></tr>" +
                       "<tr><td>会员编号：" + oh.userId + "</td>" +
                       "<td>会员姓名：" + oh.receiptName + "</td>" +
                       "<td>联系电话：" + oh.phone + "</td>" +
                       "<td>收货地址：" + oh.address + "</td></tr>" +
                       "<tr><td colspan=\"4\" style=\"font-weight: bold;\">商品列表</td></tr>" +
                       "<tr><td colspan=\"4\"><table cellpadding=\"0\" cellspacing=\"0\" class=\"t1\" width=\"100%\">" +
                       "<tr><td>商品编号</td><td>商品名称</td><td>单价</td><td>购买数量</td><td>总金额</td><td>支付方式</td></tr>"
                       );
                    List<Model.OrderItem> items = oh.items;
                    if (items != null && items.Count > 0)
                    {
                        foreach (Model.OrderItem item in items)
                        {
                            Response.Write("<tr><td>" + item.productNumber + "</td>" +
                                       "<td>" + item.productName + " </td>" +
                                       "<td>" + string.Format("{0:N2}", item.price) + " </td>" +
                                       "<td>" + item.num + "</td>" +
                                       "<td>" + string.Format("{0:N2}", item.total) + "</td>" +
                                       "<td>" + oh.payType + "</td></tr>");
                        }
                    }

                    Response.Write("</table></td></tr></table>");
                    Response.Write("<div style=\"margin: 50px 0;\"><span style=\"border: 1px dotted #ccc; display: block;\"></span></div>");
                }
            }
        }
    %>
</body>
</html>
