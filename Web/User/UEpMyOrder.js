
define(['text!UEpMyOrder.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UEpMyOrder, $) {

    var controller = function (name) {

        var statusDto = { 0: "挂卖中", 1: "部分售出", 2: "全部售出", 3: "已完成", 4: "已取消" };
        var agentJj = 0;
        var bei = 0;

       

        //初始化加载数据
        utils.AjaxPostNotLoadding("UEpMyOrder/GetAgent", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UEpMyOrder);
                //导出
                if (LoginUser && LoginUser == "system") {
                    document.getElementById("export").style.display = "block";
                }

                agentJj = result.msg;
                bei = result.result.paramValue;

                $("#xuqiu").html("挂卖数量需是"+bei+"的倍数");

                //初始化日期选择框
                $(".form-date").datetimepicker(
                {
                    language: "zh-CN",
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

                //初始化下拉框
                $("#flag").empty();
                $("#flag").append("<option value='-1'>--全部--</option>");
                $("#flag").append("<option value='0'>挂卖中</option>");
                $("#flag").append("<option value='1'>部分售出</option>");
                $("#flag").append("<option value='2'>全部售出</option>");
                $("#flag").append("<option value='3'>已完成</option>");
                $("#flag").append("<option value='4'>已取消</option>");

                //初始化按钮
                $(".easyui-linkbutton").each(function (i, ipt) {
                    $(ipt).linkbutton({});
                });

                $('#dlg').dialog({
                    title: '我要挂单',
                    width: 500,
                    height: 350,
                    cache: false,
                    modal: true
                });

                //清除错误样式
                $(".UEpMyOrderAdd").each(function () {
                    var dom = $(this);
                    dom.on("focus", function () {
                        dom.removeClass("inputError");
                    })
                });

                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var checked = true;
                    $(".UEpMyOrderAdd").each(function () {
                        var dom = $(this);
                        if (dom.val() == 0) {
                            dom.addClass("inputError");
                            checked = false;
                        }
                    });
                    var val = $("#saleNum").val();
                    if (isNaN(val) || val < 0) {
                        $("#saleNum").addClass("inputError");
                        checked = false;
                    }

                    if (checked) {
                        var data = { saleNum: val, phone: $("#phone").val(), qq: $("#qq").val() };
                        utils.AjaxPost("UEpMyOrder/SaveRecord", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                agentJj = result.msg;
                                //关闭弹出框
                                $('#dlg').dialog("close");
                                //重刷grid
                                queryGrid();

                            }
                        });
                    }
                    return false;
                })

                //初始化表格
                var grid = utils.newGrid("dg", {
                    title: '我的挂单',
                    showFooter: true,
                    columns: [[
                     { field: 'ck', title: '文本', checkbox: true, },
                     { field: 'number', title: '单号', width: '18%' },
                     { field: 'waitNum', title: '待出售数量', width: '10%' },
                     { field: 'scNum', title: '已出售数量', width: '10%' },
                     { field: 'saleNum', title: '挂卖数量', width: '10%' },
                     { field: 'status', title: '状态', width: '10%' },
                     { field: 'addTime', title: '挂卖日期', width: '15%' },
                     {
                         field: '_operate', title: '' + cz + '', width: '70', align: 'center', formatter: function (val, row, index) {
                             return '&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildTo" >交易明细</a>';
                             //'&nbsp;&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridStatusTo" >状态记录</a>';
                         }
                     }
                    ]],
                    toolbar: [{
                        text: "我要挂单",
                        iconCls: 'icon-add',
                        handler: function () {
                            //清空表单数据
                            $('#editModalForm').form('reset');
                            $("#agentJj").val(agentJj);
                            $('#dlg').dialog({ title: '我要挂单' }).dialog("open");
                        }
                    }, '-', {
                        text: "取消挂卖",
                        iconCls: 'icon-cancel',
                        handler: function () {
                            var obj = grid.datagrid("getSelected");
                            if (obj == null) { utils.showErrMsg("请选择需要取消挂卖的行！"); }
                            else if (obj.flag != 0 && obj.flag != 1) { utils.showErrMsg("该记录不是【挂卖中、部分售出】！"); }
                            else {
                                utils.confirm("您确定要取消挂卖吗？", function () { 
                                    utils.AjaxPost("UEpMyOrder/SaveCancel", {id:obj.id}, function (result) {
                                        if (result.status == "fail") {
                                            utils.showErrMsg(result.msg);
                                        } else {
                                            utils.showSuccessMsg("操作成功");
                                            agentJj = result.msg;
                                            //关闭弹出框
                                            $('#dlg').dialog("close");
                                            //重刷grid
                                            queryGrid();

                                        }
                                    });
                                })
                            }
                        }
                    }
                    ],
                    url: "UEpMyOrder/GetListPage"
                }, null, function (data) {
                    if (data && data.rows) {
                        for (var i = 0; i < data.rows.length; i++) {
                            data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                            data.rows[i]["status"] = statusDto[data.rows[i]["flag"]];
                        }
                    }
                    return data;
                }, function () {
                    //行交易明细
                    $(".gridFildTo").each(function (i, dom) {
                        dom.onclick = function () {
                            var dataId = $(dom).attr("dataId");
                            location.href = "#UEpMyOrderDetail/" + dataId;
                            return false;
                        }
                    });
                    //行状态记录
                    //$(".gridStatusTo").each(function (i, dom) {
                    //    dom.onclick = function () {
                    //        var dataId = $(dom).attr("dataId");
                    //        location.href = "#OpenMember/" + memberId;

                    //    }
                    //});
          
                });

                //查询grid
                queryGrid = function () {
                    var objs = $("#UEpMyOrderQueryForm").serializeObject();
                    grid.datagrid("options").queryParams = objs;
                    grid.datagrid("reload");
                }

                //查询按钮
                $("#submit").on("click", function () {
                    queryGrid();
                })

            }
        });


        

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});