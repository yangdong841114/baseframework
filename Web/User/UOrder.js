
define(['text!UOrder.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UOrder, $) {

    var controller = function (name) {

        appView.html(UOrder);
        //初始化物流信息
        $('#msgdlg').dialog({
            title: '查看物流信息',
            width: 600,
            height: 400,
            cache: false,
            modal: true
        })
        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });
        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        var detailGrid = undefined;

        //初始化表格
        var grid = utils.newGrid("dg", {
            title:'订单列表',
            showFooter: true,
            columns: [[
             { field: 'id', title: '订单编号', width: '160' },
             { field: 'typeId', title: '订单类型', width: '80' },
             { field: 'receiptName', title: '收货人', width: '100' },
             { field: 'phone', title: '' + lxdh + '', width: '120' },
             { field: 'address', title: '收货地址', width: '200' },
             { field: 'productNum', title: '购买总数', width: '80' },
             { field: 'productMoney', title: '总金额', width: '80' },
             { field: 'status', title: '状态', width: '80' },
             { field: 'logName', title: '物流公司', width: '100' },
             { field: 'logNo', title: '物流单号', width: '100' },
             { field: 'orderDate', title: '购买日期', width: '130' },
            { field: 'caozuo', title: '' + cz + '', width: '130' },
            ]],
            onClickRow: function (index, data) {
                detailGrid.datagrid("loadData",data.items);
            },
            url: "UOrder/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["orderDate"] = utils.changeDateFormat(data.rows[i]["orderDate"]);
                    if(data.rows[i].status == 1)
                        data.rows[i].status ='待发货';
                    if(data.rows[i].status == 2)
                        data.rows[i].status ='已发货';
                    if (data.rows[i].status == 3)
                        data.rows[i].status = '已收货';
                    if (data.rows[i].status == 4)
                        data.rows[i].status = '已取消';
                    data.rows[i].typeId = data.rows[i].typeId == 1 ? '注册订单' : '复消订单';
                    data.rows[i].address = data.rows[i].provinceName + data.rows[i].cityName + data.rows[i].areaName + data.rows[i].address;
                    data.rows[i]["caozuo"] = "<a href='javascript:void(0);'  dataId ='" + data.rows[i].id + "' class='recordLog'>查看物流</a>";
                }
            }
            return data;
        }, function () {
            //查看物流信息
            $(".recordLog").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var dataId = dom.attr("dataId");
                    utils.AjaxPost("UOrder/GetLogisticMsg", { id: dataId }, function (result) {
                        if (result.status == "success") {
                            var dto = result.result;
                            var html = "<table><tr><td colspan='2' style='font-weight:bold;'>物流公司：" + dto.company + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;物流单号：" + dto.no + "</td></tr>";
                            $("#logMsgDiv").empty();
                            if (dto.list && dto.list.length > 0) {
                                for (var i = 0; i < dto.list.length; i++) {
                                    var vo = dto.list[i];
                                    html += "<tr><td style='width:130px;'>" + utils.changeDateFormat(vo.datetime) + "</td><td>" + vo.remark + "</td></tr>";
                                    //html += '<div class="col-sm-12" style="text-align:left;" id="logMsgDiv"><div class="form-group">' +
                                    //        '<label for="exampleInputAddress1">' + utils.changeDateFormat(vo.datetime) + '</label>' +
                                    //        '<label for="exampleInputAddress1" class="text-yellow">' + vo.remark + '</label>' +
                                    //        '</div></div>';
                                }
                            }
                            html += "</tabel>"
                            $("#logMsgDiv").html(html);
                            $('#msgdlg').dialog("open");
                        } else {
                            utils.showErrMsg(result.msg);
                        }
                    });
                })
            });
        })

        detailGrid = utils.newGrid("detailGrid", {
            showFooter: true,
            pagination:false,
            columns: [[
                { field: 'hid', title: '订单编号', width: '20%' },
                { field: 'productNumber', title: '产品编码', width: '20%' },
                { field: 'productName', title: '产品名称', width: '30%' },
                { field: 'price', title: '价格', width: '10%' },
                { field: 'num', title: '数量', width: '10%' },
                { field: 'total', title: '金额', width: '10%' },
            ]]
        }, null, function (data) {
            return data;
        })


        controller.onRouteChange = function () {
            $('#msgdlg').dialog("destroy");
        };
    };

    return controller;
});
