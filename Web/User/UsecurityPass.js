
define(['text!UsecurityPass.html', 'jquery', 'j_easyui', 'zui'], function (UsecurityPass, $) {

    var controller = function (name) {
       
        appView.html(UsecurityPass);

        //失去焦点事件
        $(".form-control").each(function (index,ele) {
            var current = $(this);
            var parent = current.parent();
            var before = current.parent().parent().children().eq(0);
            current.on("focus", function (event) {
                parent.removeClass("has-error");
                utils.destoryPopover(current);
            });
        });

        var isFrist = "Y";

        InitView = function (result) {
            isFrist = result.msg;
            if (isFrist == "Y") {
                //初始化银行帐号下拉框
                $("#question").empty();
                $("#question").append("<option value=''>--请选择--</option>");
                if (cacheList["question"] && cacheList["question"].length > 0) {
                    var list = cacheList["question"];
                    for (var i = 0; i < list.length; i++) {
                        $("#question").append("<option value='" + list[i].name + "'>" + list[i].name + "</option>");
                    }
                }
                $("#oldDiv").css("display", "none");
                $("#fristQuestion").css("display", "block");
                $("#oldQuestion").css("display", "none");
            } else {
                $("#questionVal").html(result.other);
                $("#oldDiv").css("display", "block");
                $("#fristQuestion").css("display", "none");
                $("#oldQuestion").css("display", "block");
            }
        }


        utils.AjaxPostNotLoadding("UsecurityPass/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                InitView(result);
                //修密保答案
                $("#saveBtn").on('click', function () {
                    var checked = true;
                    var data = { answer: $("#answer").val() };
                    if (isFrist == "Y") {
                        if ($("#question").val() == 0) {
                            var current = $("#question");
                            var parent = current.parent();
                            parent.addClass("has-error");
                            utils.showPopover(current, current.attr("emptymsg"), "popover-danger");
                            checked = false;
                        }
                        data["question"] = $("#question").val();
                    } else {
                        if ($("#oldAnswer").val() == 0) {
                            var current = $("#oldAnswer");
                            var parent = current.parent();
                            parent.addClass("has-error");
                            utils.showPopover(current, current.attr("emptymsg"), "popover-danger");
                            checked = false;
                        }
                        data["oldAnswer"] = $("#oldAnswer").val();
                    }
                    if ($("#answer").val() == 0) {
                        var current = $("#answer");
                        var parent = current.parent();
                        parent.addClass("has-error");
                        utils.showPopover(current, current.attr("emptymsg"), "popover-danger");
                        checked = false;
                    }

                    if (checked) {
                        utils.AjaxPost("UsecurityPass/SaveSecurityPass", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("设置成功！");
                                //清空表单
                                $(".form-control").each(function (index, ele) {
                                    $(this).val("");
                                });
                                InitView(result);
                            }
                        });
                    }
                });
            }
        });

        


        controller.onRouteChange = function () {
        };
    };

    return controller;
});