
define(['text!UMTransfer.html', 'jquery', 'j_easyui', 'zui','datetimepicker'], function (UMTransfer, $) {

    var controller = function (name) {

        var dto = null;

        //设置表单默认数据
        setDefaultValue = function (dto) {
            $("#agentDz").html(dto.account.agentDz);
            $("#agentJj").html(dto.account.agentJj);
        }

        //加载会员信息
        utils.AjaxPostNotLoadding("UMTransfer/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMTransfer);
                dto = result.result;
                
                //初始化转账类型下拉框
                $("#typeId").empty();
                $("#typeId").append("<option value='0'>--请选择--</option>");
                $("#typeId2").empty();
                $("#typeId2").append("<option value='0'>--全部--</option>");
                if (cacheList["MTransfer"] && cacheList["MTransfer"].length > 0) {
                    var list = cacheList["MTransfer"];
                    for (var i = 0; i < list.length; i++) {
                        $("#typeId").append("<option value='" + list[i].id + "'>" + list[i].name + "</option>");
                        $("#typeId2").append("<option value='" + list[i].id + "'>" + list[i].name + "</option>");
                    }
                }

                //初始化日期选择框
                $(".form-date").datetimepicker(
                {
                    language: "zh-CN",
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

                //初始默认值
                setDefaultValue(dto);

                //绑定获取焦点事件
                $(".form-control").each(function (index, ele) {
                    var current = $(this);
                    var parent = current.parent();
                    current.on("focus", function (event) {
                        parent.removeClass("has-error");
                        utils.destoryPopover(current);
                    })
                })

                $("#toUserId").on("blur", function () {
                    $("#toUserName").html("");
                    var current = $("#toUserId");
                    var parent = current.parent();
                    if (current.val() != 0) {
                        utils.AjaxPostNotLoadding("UMTransfer/GetUserName", { userId: current.val() }, function (result) {
                            if (result.status == "fail" || result.msg == "" + lanager["会员不存在"] + "") {
                                parent.addClass("has-error");
                                utils.showPopover(current, result.msg, "popover-danger");
                            } else{
                                $("#toUserName").html(result.msg);
                            }
                        });
                    }
                })

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#typeId").val();
                    var current = $("#typeId");
                    var parent = current.parent();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    var isChecked = true;
                    if (val == 0) {
                        parent.addClass("has-error");
                        utils.showPopover(current, "请选择转账类型", "popover-danger");
                        isChecked = false;
                    }
                    if (!g.test($("#epoints").val())) {
                        var ct = $("#epoints");
                        var pp = ct.parent();
                        pp.addClass("has-error");
                        utils.showPopover(ct, "转账金额格式不正确", "popover-danger");
                        isChecked = false;
                    }
                    if (val == 61 && $("#toUserId").val()==0) { 
                        var ct = $("#toUserId");
                        var pp = ct.parent();
                        pp.addClass("has-error");
                        utils.showPopover(ct, "请录入转账会员", "popover-danger");
                        isChecked = false;
                    }

                    $(".form-control").each(function (index, ele) {
                        var current = $(this);
                        var parent = current.parent();
                        if(parent.hasClass("has-error")){
                            isChecked = false;
                        }
                    });

                    if(isChecked) {
                        utils.confirm("确定要转账吗？", function () {
                            var data = { typeId: $("#typeId").val(), epoints: $("#epoints").val(), toUserId: $("#toUserId").val() };
                            utils.AjaxPost("UMTransfer/SaveMTransfer", data, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("操作成功！");
                                    dto = result.result;
                                    setDefaultValue(dto);
                                    grid.datagrid("reload");
                                }
                            });
                        })
                    }

                })

                //初始化表格
                var grid = utils.newGrid("dg", {
                    title:"转账记录",
                    columns: [[
                     { field: 'typeId', title: '转账类型', width: '150' },
                     { field: 'fromUserId', title: '转出账户', width: '100' },
                     { field: 'fromUserName', title: '' + hyxm + '', width: '100' },
                     { field: 'toUserId', title: '转入账户', width: '130' },
                     { field: 'toUserName', title: '' + hyxm + '', width: '100' },
                     { field: 'epoints', title: '转账金额', width: '100' },
                     { field: 'addTime', title: '转账日期', width: '130' },
                    ]],
                    url: "UMTransfer/GetListPage"
                }, null, function (data) {
                    if (data && data.rows) {
                        for (var i = 0; i < data.rows.length; i++) {
                            data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                            data.rows[i]["typeId"] = cacheMap["MTransfer"][data.rows[i].typeId];
                        }
                    }
                    return data;
                })

                //查询grid
                queryGrid = function () {
                    var objs = $("#QueryForm").serializeObject();
                    if (objs) {
                        for (name in objs) {
                            if (name == "typeId2") {
                                objs["typeId"] = objs[name];
                            }
                        }
                    }
                    grid.datagrid("options").queryParams = objs;
                    grid.datagrid("reload");
                }

                //查询按钮
                $("#mgQueryBtn").on("click", function () {
                    queryGrid();
                })

            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});