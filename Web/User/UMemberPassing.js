
define(['text!UMemberPassing.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UMemberPassing, $) {

    var controller = function (name) {

        appView.html(UMemberPassing);

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            title:'已开通会员列表',
            showFooter: true,
            columns: [[
             { field: 'userId', title: '' + hybh + '', width: '90' },
             { field: 'userName', title: '' + hymc + '', width: '90' },
             { field: 'reName', title: '推荐人编号', width: '90' },
             { field: 'fatherName', title: '接点人编号', width: '90' },
             { field: 'uLevel', title: '' + hyjb + '', width: '80' },
             { field: 'regMoney', title: '' + zcje + '', width: '80' },
             { field: 'phone', title: '' + lxdh + '', width: '100' },
             { field: 'isLock', title: '' + sfdj + '', width: '70' },
             { field: 'addTime', title: '' + zcrq + '', width: '130' },
             {
                 field: '_operate', title: '' + cz + '', width: '100', formatter: function (val, row, index) {
                     return '&nbsp;&nbsp;<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildEdit" >' + bj + '</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildOpen0" >开通</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildDelete" >' + sc + '</a>';
                 }
             }
            ]],
            url: "UMemberPassing/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["passTime"] = utils.changeDateFormat(data.rows[i]["passTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    data.rows[i].isLock = data.rows[i].isLock == 0 ? "否" : "是";
                }
            }
            return data;
        }, function () {
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#UMemberInfo/" + memberId;
                    return false;
                }
            });

            //行开通实单
            $(".gridFildOpen0").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("该会员确定要开通吗？", function () {
                        utils.AjaxPost("UMemberPassing/OpenMember", { id: memberId, flag: 0 }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("确定要删除该会员吗？", function () {
                        utils.AjaxPost("UMemberPassing/Delete", { id: memberId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
        })



        controller.onRouteChange = function () {
        };
    };

    return controller;
});