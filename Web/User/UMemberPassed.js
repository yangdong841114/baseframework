
define(['text!UMemberPassed.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UMemberPassed, $) {

    var controller = function (name) {

        appView.html(UMemberPassed);

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            title:'已开通会员列表',
            showFooter: true,
            columns: [[
             { field: 'userId', title: '' + hybh + '', width: '100' },
             { field: 'userName', title: '' + hymc + '', width: '100' },
             { field: 'reName', title: '推荐人编号', width: '100' },
             { field: 'fatherName', title: '接点人编号', width: '100' },
             { field: 'uLevel', title: '' + hyjb + '', width: '80' },
             { field: 'regMoney', title: '' + zcje + '', width: '80' },
             { field: 'phone', title: '' + lxdh + '', width: '100' },
             //{ field: 'address', title: '联系地址', width: '150' },
             //{ field: 'qq', title: 'QQ', width: '100' },
             //{ field: 'shopName', title: '报单中心', width: '80' },
             { field: 'isFt', title: '是否复投', width: '70' },
             { field: 'isLock', title: '' + sfdj + '', width: '70' },
             //{ field: 'empty', title: '' + sfkd + '', width: '70' },
             { field: 'addTime', title: '' + zcrq + '', width: '130' },
             { field: 'passTime', title: '' + ktrq + '', width: '130' },
             //{ field: 'byopen', title: '' + czr + '', width: '80' },
            ]],
            url: "UMemberPassed/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["passTime"] = utils.changeDateFormat(data.rows[i]["passTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                    data.rows[i].isLock = data.rows[i].isLock == 0 ? "否" : "是";
                    data.rows[i].isFt = data.rows[i].isFt == 0 ? "否" : "是";
                }
            }
            return data;
        })



        controller.onRouteChange = function () {
        };
    };

    return controller;
});