﻿using Model;
using Spring.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Web
{
    // 注意: 有关启用 IIS6 或 IIS7 经典模式的说明，
    // 请访问 http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : SpringMvcApplication
    {
        protected void Application_Start()
        {
            //AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            //AuthConfig.RegisterAuth();

            AreaRegistration.RegisterAllAreas();
            log4net.Config.XmlConfigurator.Configure();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            HttpApplication happ = (HttpApplication)sender;
            HttpContext context = happ.Context;
            if (context!=null)
            {
                //string url = context.Request.RawUrl;
                

                //if (url.StartsWith("/Admin"))
                //{
                //    if (IsAdminCheckUrl(url))
                //    {
                //        Member m = (Member)HttpContext.Current.Session["MemberUser"];
                //        //Member m = (Member)Session["MemberUser"];
                //        Console.Write(url);
                //    }
                    
                //}
                //if (url.StartsWith("/User"))
                //{
                //    Console.Write(url);
                //}
                //Member m = (Member)Session["MemberUser"];
                //Member m2 = (Member)Session["LoginUser"];

                //if (m2 != null)
                //{

                //}
            }
            //if (url.Contains("module"))
            //{
            //    context.Response.StatusCode = 404;
            //    context.Response.Write("s");
            //    context.Response.End();
            //}
            
        }

    }
}