
define(['text!ShopSet.html', 'jquery', 'j_easyui'], function (ShopSet, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["设置报单中心"];
        $("#center").panel("setTitle", titles);

        var pageHtml = ShopSet;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化表单区域
        $('#dlg').dialog({
            title: '添加报单中心',
            width: 400,
            height: 200,
            cache: false,
            modal: true
        })

        //初始化编辑区input text
        $(".easyui-textbox").each(function (i, ipt) {
            $(ipt).textbox({ labelAlign: 'right', height: '28px', labelWidth: '100px' });
        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //添加表单-会员编号绑定鼠标离开事件
        $("#userId").next("span").children().eq(0).on("blur", function () {
            $("#userName").textbox("setValue", "");
            if ($("#userId").textbox("isValid")) {
                utils.AjaxPostNotLoadding("ShopSet/GetModelMsg", { userId: $("#userId").textbox("getText") }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#userName").textbox("setValue", result.msg);
                    } else {
                        $("#userName").textbox("setValue", result.map["userName"]);
                    }
                });
            }

        })

        //保存按钮
        $("#saveBtn").bind("click", function () {
            $('#editModalForm').form('submit', {
                url: 'ShopSet/Save',
                onSubmit: function () {
                    //默认校验
                    var ok = $(this).form('validate');
                    if (ok) {
                        $.messager.progress({
                            title: '请稍后',
                            msg: '正在处理中...',
                            interval: 100,
                            text: ''
                        });
                    }
                    return ok;
                },
                success: function (result) {
                    $.messager.progress("close");
                    var result = eval('(' + result + ')');
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        //关闭弹出框
                        $('#dlg').dialog("close");
                        //重刷grid
                        queryGrid();
                    }
                }
            });
            return false;
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '' + hybh + '', width: '25%' }]],
            columns: [[
                { field: 'userName', title: '' + hyxm + '', width: '25%' },
                { field: 'addTime', title: '' + lanager["设置日期"] + '', width: '25%' },
                { field: 'createUser', title: '' + czr + '', width: '25%' },
            ]],
            toolbar: [{
                text: "" + tj + "",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $('#editModalForm').form('reset');
                    $('#dlg').dialog({ title: '' + lanager["添加报单中心"] + '' }).dialog("open");
                }
            }
            ],
            url: "ShopSet/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            if (objs != null) {
                for (name in objs) {
                    if (name == "userId2") { objs["userId"] = objs[name]; }
                    else if (name == "userName2") { objs["userName"] = objs[name]; }
                }
            }
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});