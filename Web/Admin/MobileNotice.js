
define(['text!MobileNotice.html', 'jquery'], function (MobileNotice, $) {

    var controller = function (obj) {
        //设置标题
        var titles = lanager["短信通知"];
        $("#center").panel("setTitle", titles);
        
        var names = [];

        utils.AjaxPostNotLoadding("MobileNotice/InitView", {}, function (result) {
            if (result.status == "success") {

                var pageHtml = MobileNotice;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);
                //初始化赋值
                var rt = result.map;
                for (name in rt) {
                    names.push(name);
                    var obj = rt[name];
                    $("#" + name + "_cb").attr("checked", obj.flag == 1 ? true : false);
                    $("#" + name + "_msg").val(obj.msg);
                    var phoneInput = $("#" + name + "_phone");
                    if (phoneInput) {
                        phoneInput.val(obj.phone);
                    }
                }

                //保存
                $("#saveBtn").on('click', function () {
                    if (names && names.length > 0) {
                        var data = {};
                        for (var i = 0; i < names.length; i++) {
                            var code = names[i];
                            var phoneInput = $("#" + code + "_phone");          //电话号码
                            var checked = $("#" + code + "_cb")[0].checked;     //复选框
                            var flag = 0, phone = null;
                            if (checked) { flag = checked ? 1 : 0; }
                            if (phoneInput) { phone = phoneInput.val(); }
                            data["list[" + i + "].flag"] = flag;
                            data["list[" + i + "].phone"] = phone;
                            data["list[" + i + "].msg"] = $("#" + code + "_msg").val();
                            data["list[" + i + "].code"] = code;
                        }
                        utils.AjaxPost("MobileNotice/Save", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }
                })
            }
        });
        
        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});