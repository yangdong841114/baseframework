
define(['text!Article.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Article, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["公告管理"];
        $("#center").panel("setTitle", titles);
        var pageHtml = Article;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化表单区域
        $('#dlg').dialog({
            title: '发布文章',
            width: 850,
            height: 600,
            cache: false,
            modal: true
        })

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });


        //初始化编辑器
        var editor = new Quill("#editor", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //保存按钮
        $("#saveBtn").bind("click", function () {
            if ($("#title").val() == 0) {
                utils.showErrMsg("请输入主题");
            }
            else if (editor.getText() == 0) {
                utils.showErrMsg("请输入内容");
            } else {
                utils.AjaxPost("Article/SaveByUeditor", { content: utils.getEditorHtml(editor), title: $("#title").val(), id: $("#id").val() == 0 ? "" : $("#id").val() }, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("操作成功");
                        $('#dlg').dialog("close");
                        queryGrid();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            }
        })

        //打开明细
        openDetailDialog = function (row) {
            $("#title").val(row.title);
            $("#id").val(row.id);
            utils.setEditorHtml(editor,row.content)
            $('#dlg').dialog({ title: '修改公告' }).dialog("open");
        }

        //初始化表格
        var grid = utils.newGrid("dg", {
            rownumbers: false,
            columns: [[
             {
                 field: '_title', title: '主题', width: '70%', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" rowIndex ="' + index + '" class="titleOpen" >' + row.title + '</a>';
                 }
             },
             { field: 'addTime', title: '发布日期', width: '15%' },
             {
                 field: '_deleteRecord', title: '' + cz + '', width: '15%', formatter: function (val, row, index) {
                     var cont = "";
                     if (row.isTop == 1) {
                         cont = '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordTop" >置顶</a>&nbsp;&nbsp;';
                     } else {
                         cont = '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordCancelTop" >取消置顶</a>&nbsp;&nbsp;';
                     }
                     cont += '<a href="javascript:void(0);" rowIndex ="' + index + '" class="recordEdit" >' + bj + '</a>&nbsp;&nbsp;' +
                            '<a href="javascript:void(0);" dataId ="' + row.id + '" class="recordDelete" >' + sc + '</a>';
                     return cont;
                 }
             }
            ]],
            toolbar: [{
                text:"发布文章",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $("#title").val("");
                    $("#id").val("");
                    utils.setEditorHtml(editor, "")
                    $('#dlg').dialog({ title: '发布公告' }).dialog("open");
                }
            }
            ],
            url: "Article/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {

            //置顶
            $(".recordTop").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("确定置顶吗？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("Article/SaveTop", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("置顶成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })

            //取消置顶
            $(".recordCancelTop").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("确定取消置顶吗？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("Article/CancelTop", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("取消成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })

            //点击标题打开文章明细
            $(".titleOpen").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = grid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    openDetailDialog(row);
                })
            })

            //点击编辑按钮编辑
            $(".recordEdit").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    var rows = grid.datagrid("getRows");
                    var index = dom.attr("rowIndex");
                    var row = rows[index];
                    openDetailDialog(row);
                })
            })

            //删除
            $(".recordDelete").each(function (index, ele) {
                var dom = $(this);
                dom.on("click", function () {
                    utils.confirm("您确定要删除该记录？", function () {
                        var dataId = dom.attr("dataId");
                        utils.AjaxPost("Article/Delete", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg(result.msg);
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                })
            })
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            var queryObj = {};
            $.each(objs, function (name, val) {
                if (val != 0) {
                    var name = name == "title2" ? "title" : name;
                    queryObj[name] = val;
                }
            });
            grid.datagrid("options").queryParams = queryObj;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});