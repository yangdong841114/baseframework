﻿
define(['text!SiteStatistics.html', 'jquery'], function (SiteStatistics, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["前台设置"];
        $("#center").panel("setTitle", titles);


        utils.AjaxPostNotLoadding("Website/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

                var pageHtml = SiteStatistics;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);

                var dto = result.result;
                $("#WebCode").val(dto.WebCode);
                $("#WebCode").on("focus", function () {
                    $("#WebCode").removeClass("inputError");
                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#WebCode").val();
                    if (val == null || val == undefined|| val== "") {
                        $("#WebCode").addClass("inputError");
                    } else {
                        var data = { id: 1, WebCode: val };
                        utils.AjaxPost("Website/Save", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }

                })
            }
        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});