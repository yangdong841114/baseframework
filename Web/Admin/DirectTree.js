
define(['text!DirectTree.html', 'jquery', 'j_easyui'], function (DirectTree, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["直推图"];
        $("#center").panel("setTitle", titles);


        //ztree设置
        var treesetting = {
            data: {
                simpleData: {
                    enable: true,
                    idKey: "id", 	              // id编号字段名
                    pIdKey: "reId",               // 父id编号字段名
                    rootPId: null
                },
                key: {
                    name: "dtreeName"    //显示名称字段名
                }
            },
            view: {
                showLine: false,            //不显示连接线
                dblClickExpand: false       //禁用双击展开
            },
            callback: {
                onClick: function (e, treeId, node) {
                    if (node.isParent) {
                        resourceTree.expandNode(node);
                    }
                }
            }
        };

        var resourceTree = undefined; //ztree对象

        //初始化加载数据
        utils.AjaxPostNotLoadding("DirectTree/GetDirectTreeList", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                var pageHtml = DirectTree;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);
                reInitTree(result.list);

                //查询按钮
                $("#queryBtn").on("click", function () {
                    var param = $("#QueryForm").serializeObject();
                    utils.AjaxPost("DirectTree/GetDirectTreeList", param, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            //生成树
                            reInitTree(result.list);
                            $("#other").html("");
                            if (result.other && result.other.length > 0) {
                                $("#other").html("上线链路：" + result.other);
                            }
                        }
                    });
                })
            }
            
        });

        //重新生成树
        reInitTree = function (data) {
            //先销毁树
            if (resourceTree) {
                resourceTree.destroy();
            }
            //生成树
            resourceTree = $.fn.zTree.init($("#resourceTree"), treesetting, data);

            //查找根节点并自动展开
            var rootNodes = resourceTree.getNodesByFilter(function (node) {
                if (node.reId == null || node.reId == 0) {
                    return true;
                }
            });
            if (rootNodes && rootNodes.length > 0) {
                for (var i = 0; i < rootNodes.length; i++) {
                    resourceTree.expandNode(rootNodes[i], true, false, true);
                }
            }
        }



        controller.onRouteChange = function () {
            //销毁模态窗口
        };
    };

    return controller;
});