
define(['text!MemberPassing.html', 'jquery', 'j_easyui', 'datetimepicker'], function (MemberPassing, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["待开通会员"];
        $("#center").panel("setTitle", titles);

        var pageHtml = MemberPassing;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#MemberPassing", toUid: 0 }, function () { });

        //根节点类型
        var rootObj = {};

        $(".form-date").datetimepicker(
            {
                language: "zh-CN",
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });



        //初始化表格
        var grid = utils.newGrid("dg", {
            rownumbers: false,
            columns: [[
                { field: 'reName', title: '' + tjrbh + '', width: '10%' },
                { field: 'shopName', title: '' + bdzx + '', width: '10%' },
                { field: 'userName', title: '' + hymc + '', width: '10%' },
                { field: 'regMoney', title: '' + zcje + '', width: '10%' },
                { field: 'uLevel', title: '' + hyjb + '', width: '10%' },
                { field: 'phone', title: '' + lxdh + '', width: '10%' },
                { field: 'addTime', title: '' + zcrq + '', width: '10%' }


            ]],
            frozenColumns: [[{ field: 'userId', title: '' + hybh + '', width: '10%' },
            {
                field: '_operate', title: '' + cz + '', width: '20%', formatter: function (val, row, index) {
                    return '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildEdit" >' + bj + '</a>&nbsp;&nbsp;' +
                        '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildOpen1" >' + ktkd + '</a>&nbsp;&nbsp;' +
                        '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildOpen0" >' + ktsd + '</a>&nbsp;&nbsp;' +
                        '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildDelete" >' + sc + '</a>';
                }
            }
            ]],
            url: "MemberPassing/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                }
            }
            return data;
        }, function () {
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#MemberInfo/" + memberId;
                    return false;
                }
            });
            //行开通空单按钮
            $(".gridFildOpen1").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("" + isKtkd + "？", function () {
                        utils.AjaxPost("MemberPassing/OpenMember", { id: memberId, flag: 1 }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
            //行开通实单
            $(".gridFildOpen0").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("" + isKtsd + "？", function () {
                        utils.AjaxPost("MemberPassing/OpenMember", { id: memberId, flag: 0 }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("" + isSc + "？", function () {
                        utils.AjaxPost("MemberPassing/Delete", { id: memberId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "MemberPassing/ExportNotOpenExcel";
        })

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {

        };
    };

    return controller;
});