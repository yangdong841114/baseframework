
define(['text!Currency.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Currency, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["奖金查询"];
        $("#center").panel("setTitle", titles);
        var pageHtml = Currency;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            rownumbers: false,
            showFooter: true,
            frozenColumns: [[
                {
                    field: '_operate', title: '' + cz + '', width: '70', align: 'center', formatter: function (val, row, index) {
                        if (row.addDate != '合计：') {
                            return '&nbsp;<a href="javascript:void(0);" addDate="' + row.addDate + '" class="gridFildTo" >获奖明细</a>&nbsp;';
                        } else {
                            return '';
                        }
                    }
                }]],
            columns: [[
                { field: 'addDate', title: '结算日期', width: '100' },
                { field: 'cat1', title: '报单费', width: '100' },
                { field: 'cat2', title: '直推奖', width: '100' },
                { field: 'cat3', title: '层碰奖', width: '100' },
                { field: 'cat4', title: '对碰奖', width: '100' },
                { field: 'cat5', title: '管理奖', width: '100' },
                { field: 'cat6', title: '见点奖', width: '100' },
                { field: 'cat7', title: '级差奖', width: '100' },
                { field: 'yf', title: '应发', width: '100' },
                { field: 'fee1', title: '所得税', width: '100' },
                { field: 'fee2', title: '管理费', width: '100' },
                { field: 'fee3', title: '复消账户', width: '100' },
                { field: 'sf', title: '实发', width: '100' }
            ]],
            onDblClickRow: function (index, data) {
                location.href = '#CurrencyUser/' + data.addDate;
            },
            url: "Currency/GetAllSumCurrency"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addDate"] = utils.changeDateFormat(data.rows[i]["addDate"], 'date');
                    data.rows[i]["cat1"] = data.rows[i]["cat1"].toFixed(2);
                    data.rows[i]["cat2"] = data.rows[i]["cat2"].toFixed(2);
                    data.rows[i]["cat3"] = data.rows[i]["cat3"].toFixed(2);
                    data.rows[i]["cat4"] = data.rows[i]["cat4"].toFixed(2);
                    data.rows[i]["cat5"] = data.rows[i]["cat5"].toFixed(2);
                    data.rows[i]["cat6"] = data.rows[i]["cat6"].toFixed(2);
                    data.rows[i]["cat7"] = data.rows[i]["cat7"].toFixed(2);
                    data.rows[i]["yf"] = data.rows[i]["yf"].toFixed(2);
                    data.rows[i]["fee1"] = data.rows[i]["fee1"].toFixed(2);
                    data.rows[i]["fee2"] = data.rows[i]["fee2"].toFixed(2);
                    data.rows[i]["fee3"] = data.rows[i]["fee3"].toFixed(2);
                    data.rows[i]["sf"] = data.rows[i]["sf"].toFixed(2);
                }
                data.footer[0].addDate = '合计：';
            }
            return data;
        }, function () {

            //行查看明细按钮
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var addDate = $(dom).attr("addDate");
                    location.href = '#CurrencyUser/' + addDate;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});