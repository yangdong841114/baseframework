
define(['text!Password3.html', 'jquery', 'zui', 'css!../Content/css/styleAdmin.css'], function (Password3, $) {

    var controller = function (prePage) {
        //设置标题
        var titles = lanager["验证交易密码"];
        $("#center").panel("setTitle", titles);

        var pageHtml = Password3;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //焦点离开事件
        $(".form-control").each(function (index, ele) {
            var current = $(this);
            var parent = current.parent();
            var before = current.parent().parent().children().eq(0);
            current.on("focus", function (event) {
                parent.removeClass("has-error");
                utils.destoryPopover(current);
            });
        });

        //密码校验
        checkPassWord = function () {
            var current = $("#password");
            var parent = current.parent();
            var val = current.val();
            parent.removeClass("has-error");
            utils.destoryPopover(current);
            if (val == 0) {
                var current = $("#password");
                parent.addClass("has-error");
                var t1 = lanager["请录入交易密码"];
                utils.showPopover(current, t1, "popover-danger");
            } else {
                utils.AjaxPost("ManageWeb/CheckAPass3", { pass3: val }, function (result) {
                    if (result.status == "fail") {
                        parent.addClass("has-error");
                        utils.showPopover(current, result.msg, "popover-danger");
                    } else {
                        pass3 = true;
                        if (prePage.indexOf(".") != -1) {
                            var val = prePage.split(".");
                            location.href = "#" + val[0] + "/" + val[1];
                        } else {
                            location.href = "#" + prePage;
                        }
                    }
                });
            }
        }

        //回车键绑定
        document.onkeydown = function (event) {
            var e = event || window.event || arguments.callee.caller.arguments[0];
            if (e && e.keyCode == 13) {
                checkPassWord();
            }
        };

        $("#password").focus();

        $("#sureBtn").on("click", function () {
            checkPassWord();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});