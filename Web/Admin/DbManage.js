
define(['text!DbManage.html', 'jquery', 'j_easyui', 'datetimepicker'], function (DbManage, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["数据管理"];
        $("#center").panel("setTitle", titles);
        var pageHtml = DbManage;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             { field: 'fname', title: '备份文件',width:'45%'},
             { field: 'addTime', title: '备份时间', width: '45%' },
             {
                 field: '_operate', title: '' + cz + '', width: '160', align: 'center', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" fname="' + row.fname + '" class="gridFildhf" >恢复</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" fname="' + row.fname + '" class="gridFildDown" >函数下载</a>&nbsp;&nbsp;' +
                          '<a href="javascript:void(0);" fname="' + row.fname + '" class="gridFildUrlDown" >路径下载</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildDelete" >' + sc + '</a>';
                 }
             }
            ]],
            url: "DbManage/GetBackModelListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {
            //行恢复按钮
            $(".gridFildhf").each(function (i, dom) {
                dom.onclick = function () {
                    var fname = $(dom).attr("fname");
                    utils.confirm("恢复后现有数据将丢失，您确定要恢复该备份吗？", function () {
                        utils.AjaxPost("DbManage/reBackDb", { fileName: fname }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("恢复成功");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                }
            });
            //行下载按钮
            $(".gridFildDown").each(function (i, dom) {
                dom.onclick = function () {
                    var fname = $(dom).attr("fname");
                    location.href = "DbManage/DownLoadFile/" + fname;
                }
            });
            $(".gridFildUrlDown").each(function (i, dom) {
                dom.onclick = function () {
                    var fname = $(dom).attr("fname");
                    location.href = "bakup/" + fname;
                }
            });
            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("您确定要删除该备份吗？", function () {
                        utils.AjaxPost("DbManage/Delete", {id:dataId}, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("删除成功");
                                grid.datagrid("reload");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    });
                }
            });
        })

        //清空数据库按钮
        $("#clearBtn").on("click", function () {
            utils.confirm("您确定要清空数据库吗？请先做好数据库备份", function () {
                utils.AjaxPost("DbManage/ClearDb", {}, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("清除成功");
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
        })

        //备份数据库按钮
        $("#backUpBtn").on("click", function () {
            utils.confirm("您确定要备份数据库吗？", function () {
                utils.AjaxPost("DbManage/BakDb", {}, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("备份成功");
                        grid.datagrid("reload");
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});