
define(['text!FamilyTree.html', 'jquery', 'j_easyui', 'css!../Content/css/styleAdmin.css'], function (FamilyTree, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["系谱图"];
        $("#center").panel("setTitle", titles);


        var titles1 = lanager["上线链路"];
        bindTree = function (msg, other) {
            $("#treeDiv").html(msg);
            $("#other").html("");
            if (other && other.length > 0) {
                $("#other").html("" + titles1 + "：" + other);
            }
            $(".forward_to").each(function () {
                var dom = $(this);
                dom.on("click", function () {
                    var userId = dom.attr("userId");
                    var data = { userId: userId, ceng: 3 };
                    utils.AjaxPost("FamilyTree/GetFamilyTreeByCondition", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            $("#userId").val("");
                            $("#ceng").val("3");
                            bindTree(result.msg, result.other);
                        }
                    });
                })
            });
        }

        //初始化加载数据
        utils.AjaxPostNotLoadding("FamilyTree/GetFamilyTree", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                $.each(lanagerList, function (index, data) {
                    if (FamilyTree.indexOf(data.key)) {
                        var length = FamilyTree.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                FamilyTree = FamilyTree.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(FamilyTree);
                bindTree(result.msg, result.other);
                //查询按钮
                $("#queryBtn").on("click", function () {
                    var param = $("#QueryForm").serializeObject();
                    utils.AjaxPost("FamilyTree/GetFamilyTreeByCondition", param, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            bindTree(result.msg, result.other);
                        }
                    });
                })
            }
        });

        controller.onRouteChange = function () {
            //销毁模态窗口

        };
    };

    return controller;
});