﻿
define(['text!RelationEdit.html', 'jquery', 'j_easyui', 'datetimepicker'], function (RelationEdit, $) {

    var controller = function (name) {
        var treePlaceDto = { 0: "左区", 1: "右区" };
        //设置标题
        var titles = lanager["修改安置关系"];
        $("#center").panel("setTitle", titles);

        var pageHtml = RelationEdit;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化dialog
        $('#dlg').dialog({
            title: '编辑记录',
            width: 600,
            height: 380,
            cache: false,
            modal: true
        })

        //初始化编辑区input text
        $(".easyui-textbox").each(function (i, ipt) {
            $(ipt).textbox({ labelAlign: 'right', height: '28px', labelWidth: '100px' });

        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //初始化日期选择框
        //$(".form-date").datetimepicker(
        //{
        //    language: "zh-CN",
        //    weekStart: 1,
        //    todayBtn: 1,
        //    autoclose: 1,
        //    todayHighlight: 1,
        //    startView: 2,
        //    minView: 2,
        //    forceParse: 0,
        //    format: "yyyy-mm-dd"
        //});

        //查询表单-会员编号绑定鼠标离开事件
        $("#userId_show").next("span").children().eq(0).on("blur", function () {
            $("#userName_show").textbox("setValue", "");
            $("#fatherName_show").textbox("setValue", "");
            $("#treePlace_show").textbox("setValue", "");
            if ($("#userId_show").textbox("isValid")) {
                utils.AjaxPostNotLoadding("RelationEdit/GetModelMsg", { userId: $("#userId_show").textbox("getText") }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#userName_show").textbox("setValue", result.msg);
                    } else {
                        $("#userName_show").textbox("setValue", result.map["userName"]);
                        $("#fatherName_show").textbox("setValue", result.map["fatherName"]);
                        $("#treePlace_show").textbox("setValue", result.map["treePlace"]);
                    }
                });
            }

        })

        //修改方式1-会员编号绑定鼠标离开事件
        $("#userId_fs1").next("span").children().eq(0).on("blur", function () {
            $("#userName_fs1").textbox("setValue", "");
            if ($("#userId_fs1").textbox("isValid")) {
                utils.AjaxPostNotLoadding("RelationEdit/GetModelMsg", { userId: $("#userId_fs1").textbox("getText") }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#userName_fs1").textbox("setValue", result.msg);
                    } else {
                        $("#userName_fs1").textbox("setValue", result.map["userName"]);
                    }
                });
            }
        })

        $("#userId_zh_fs1").next("span").children().eq(0).on("blur", function () {
            $("#userName_zh_fs1").textbox("setValue", "");
            if ($("#userId_zh_fs1").textbox("isValid")) {
                utils.AjaxPostNotLoadding("RelationEdit/GetModelMsg", { userId: $("#userId_zh_fs1").textbox("getText") }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#userName_zh_fs1").textbox("setValue", result.msg);
                    } else {
                        $("#userName_zh_fs1").textbox("setValue", result.map["userName"]);
                    }
                });
            }
        })

        //修改方式2-会员编号绑定鼠标离开事件
        $("#userId_fs2").next("span").children().eq(0).on("blur", function () {
            $("#userName_fs2").textbox("setValue", "");
            if ($("#userId_fs2").textbox("isValid")) {
                utils.AjaxPostNotLoadding("RelationEdit/GetModelMsg", { userId: $("#userId_fs2").textbox("getText") }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#userName_fs2").textbox("setValue", result.msg);
                    } else {
                        $("#userName_fs2").textbox("setValue", result.map["userName"]);
                    }
                });
            }
        })

        $("#userId_zh_fs2").next("span").children().eq(0).on("blur", function () {
            $("#userName_zh_fs2").textbox("setValue", "");
            if ($("#userId_zh_fs2").textbox("isValid")) {
                utils.AjaxPostNotLoadding("RelationEdit/GetModelMsg", { userId: $("#userId_zh_fs2").textbox("getText") }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#userName_zh_fs2").textbox("setValue", result.msg);
                    } else {
                        $("#userName_zh_fs2").textbox("setValue", result.map["userName"]);
                    }
                });
            }
        })

        //保存按钮-修改方式1
        $("#saveBtn").bind("click", function () {
            $.messager.progress({
                title: '请稍后',
                msg: '正在处理中...',
                interval: 100,
                text: ''
            });
            var data = { userId: $("#userId_fs1").val(), userId_zh: $("#userId_zh_fs1").val(), treePlace_zh: "", fsType: "fs1" };
            utils.AjaxPost("RelationEdit/Save", data, function (result) {
                $.messager.progress("close");
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg(result.msg);
                    //关闭弹出框
                    $('#dlg').dialog("close");
                    //重刷grid
                    queryGrid();
                    if (result.msg.indexOf("添加") != -1) {
                        init();
                    }
                }
            });
        })


        //保存按钮-修改方式2
        $("#saveBtn2").bind("click", function () {
            $.messager.progress({
                title: '请稍后',
                msg: '正在处理中...',
                interval: 100,
                text: ''
            });
            var data = { userId: $("#userId_fs2").val(), userId_zh: $("#userId_zh_fs2").val(), treePlace_zh: $("#treePlace_zh_fs2").val(), fsType: "fs2" };
            utils.AjaxPost("RelationEdit/Save1", data, function (result) {
                $.messager.progress("close");
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg(result.msg);
                    //关闭弹出框
                    $('#dlg').dialog("close");
                    //重刷grid
                    queryGrid();
                    if (result.msg.indexOf("添加") != -1) {
                        init();
                    }
                }
            });
        })


        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[{ field: 'userId', title: '' + hybh + '', width: '16.5%' }]],
            columns: [[
                { field: 'userName', title: '' + hymc + '', width: '10%' },
                { field: 'old_fatherName', title: '' + lanager["旧接点人"] + '', width: '10%' },
                { field: 'old_treePlace', title: '' + lanager["旧区域"] + '', width: '10%' },
                { field: 'new_fatherName', title: '' + lanager["新接点人"] + '', width: '10%' },
                { field: 'new_treePlace', title: '' + lanager["新区域"] + '', width: '10%' },
                { field: 'addTime', title: '' + xgrq + '', width: '17.5%' },
                { field: 'createUser', title: '' + czr + '', width: '16.5%' },
            ]],
            toolbar: [{
                text: "" + tj + "",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $('#editModalForm').form('reset');
                    $('#dlg').dialog({ title: '添加记录' }).dialog("open");
                }
            }
            ],
            url: "RelationEdit/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["old_treePlace"] = treePlaceDto[data.rows[i]["old_treePlace"]];
                    data.rows[i]["new_treePlace"] = treePlaceDto[data.rows[i]["new_treePlace"]];
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            if (objs != null) {
                for (name in objs) {
                    if (name == "userId2") { objs["userId"] = objs[name]; }
                    else if (name == "userName2") { objs["userName"] = objs[name]; }
                }
            }
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});