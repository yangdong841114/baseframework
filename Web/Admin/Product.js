
define(['text!Product.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Product, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["商品管理"];
        $("#center").panel("setTitle", titles);

        var pageHtml = Product;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化日期选择框
        $(".form-date").datetimepicker(
            {
                language: "zh-CN",
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

        var mlist;
        var mlistSm;


        initSecondProductType = function () {
            var selectValue = $('#productBigTypeId').val();
            $("#productTypeId").empty();
            $("#productTypeId").append("<option value='0' dataName = '全部'>--" + lanager["全部"] + "--</option>");
            if (mlistSm != null && mlistSm.length > 0) {
                for (var i = 0; i < mlistSm.length; i++) {
                    var vo = mlistSm[i];
                    if (vo.parentId == selectValue)
                        $("#productTypeId").append("<option value='" + vo.id + "' dataName = '" + vo.name + "'>" + vo.name + "</option>");
                }
            }
        };
        //获取父类
        utils.AjaxPostNotLoadding("ProductType/GetDefaultData", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                var map = result.map;
                mlist = map.mlist;
                mlistSm = map.mlistSm;
                //初始化下拉框
                $("#productBigTypeId").empty();
                $("#productBigTypeId").append("<option value='0' >--" + lanager["全部"] + "--</option>");
                if (mlist != null && mlist.length > 0) {
                    for (var i = 0; i < mlist.length; i++) {
                        var vo = mlist[i];
                        $("#productBigTypeId").append("<option value='" + vo.id + "' >" + vo.name + "</option>");
                    }
                }

                initSecondProductType();


            }
        });

        $('#productBigTypeId').on('change', function () {
            initSecondProductType();
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            rownumbers: false,
            frozenColumns: [[
                {
                    field: '_operate', title: '' + cz + '', width: '10%', align: 'center', formatter: function (val, row, index) {
                        var text = '&nbsp;<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildEdit" >' + bj + '</a>&nbsp;&nbsp;';
                        if (row.isShelve == 1) {
                            text += '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildUp" >上架</a>&nbsp;&nbsp;';

                        } else {
                            text += '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildDown" >下架</a>&nbsp;&nbsp;';
                        }
                        text += '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildDelete" >' + sc + '</a>';
                        return text;
                    }
                }]],
            columns: [[
                { field: 'productBigTypeName', title: '' + lanager["一级类目"] + '', width: '10%' },
                { field: 'productTypeName', title: '' + lanager["二级类目"] + '', width: '10%' },
                { field: 'productCode', title: '' + lanager["商品编码"] + '', width: '15%' },
                { field: 'productName', title: '' + lanager["商品名称"] + '', width: '15%' },
                {
                    field: '_img', title: '' + lanager["商品图片"] + '', width: '10%', formatter: function (val, row, index) {
                        return '<img data-toggle="lightbox" src="' + row.imgUrl + '" data-image="' + row.imgUrl + '" data-caption="商品图片" class="img-thumbnail" alt="" width="100">';
                    }
                },
                { field: 'price', title: '' + lanager["现价"] + '', width: '7%' },
                { field: 'fxPrice', title: '' + lanager["原价"] + '', width: '7%' },
                {
                    field: 'status', title: '' + lanager["是否上架"] + '', width: '7%', align: 'center', formatter: function (val, row, index) {
                        if (row.isShelve == 1) {
                            return '<font style="color:red;">否</font>';
                        } else {
                            return '<font style="color:green;">是</font>';
                        }
                    }
                },

                { field: 'addTime', title: '' + lanager["发布日期"] + '', width: '7%' }
            ]],
            toolbar: [{
                text: "" + lanager["发布商品"] + "",
                iconCls: 'icon-pencil',
                handler: function () {
                    location.href = "#DeployProduct";
                }
            }
            ],
            url: "Product/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["status"] = data.rows[i].isShelve == 1 ? "否" : "是";

                }
            }
            return data;
        }, function () {

            $(".img-thumbnail").each(function (index, ele) {
                $(this).lightbox();
            })
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    location.href = "#DeployProduct/" + dataId;
                }
            });
            //行上架按钮
            $(".gridFildUp").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定该商品上架吗？", function () {
                        utils.AjaxPost("Product/Shelve", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("上架成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
            //行下架按钮
            $(".gridFildDown").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定该商品下架吗？", function () {
                        utils.AjaxPost("Product/CancelShelve", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("下架成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定删除该商品吗？", function () {
                        utils.AjaxPost("Product/Delete", { id: dataId }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("删除成功");
                                queryGrid();
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    })
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();

            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {

        };
    };

    return controller;
});