
define(['text!ReMemberEdit.html', 'jquery', 'j_easyui', 'datetimepicker'], function (ReMemberEdit, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["修改推荐关系"];
        $("#center").panel("setTitle", titles);

        var pageHtml = ReMemberEdit;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化dialog
        $('#dlg').dialog({
            title: '编辑记录',
            width: 400,
            height: 300,
            cache: false,
            modal: true
        })

        //初始化编辑区input text
        $(".easyui-textbox").each(function (i, ipt) {
            $(ipt).textbox({ labelAlign: 'right', height: '28px', labelWidth: '100px' });

        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //初始化日期选择框
        //$(".form-date").datetimepicker(
        //{
        //    language: "zh-CN",
        //    weekStart: 1,
        //    todayBtn: 1,
        //    autoclose: 1,
        //    todayHighlight: 1,
        //    startView: 2,
        //    minView: 2,
        //    forceParse: 0,
        //    format: "yyyy-mm-dd"
        //});

        //添加表单-会员编号绑定鼠标离开事件
        $("#userId").next("span").children().eq(0).on("blur", function () {
            $("#userName").textbox("setValue", "");
            $("#oldReName").textbox("setValue", "");
            if ($("#userId").textbox("isValid")) {
                utils.AjaxPostNotLoadding("ReMemberEdit/GetModelMsg", { userId: $("#userId").textbox("getText") }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#userName").textbox("setValue", result.msg);
                    } else {
                        $("#userName").textbox("setValue", result.map["userName"]);
                        $("#oldReName").textbox("setValue", result.map["reName"]);
                    }
                });
            }

        })

        //添加表单-新推荐人编号绑定鼠标离开事件
        $("#newReName").next("span").children().eq(0).on("blur", function () {
            $("#newReUserName").textbox("setValue", "");
            if ($("#newReName").textbox("isValid")) {
                utils.AjaxPostNotLoadding("ReMemberEdit/GetModelMsg", { userId: $("#newReName").textbox("getText") }, function (result) {
                    if (result.status == "fail" && result.msg == "" + lanager["会员不存在"] + "") {
                        $("#newReUserName").textbox("setValue", result.msg);
                    } else {
                        $("#newReUserName").textbox("setValue", result.map["userName"]);
                    }
                });
            }
        })

        //保存按钮
        $("#saveBtn").bind("click", function () {
            $('#editModalForm').form('submit', {
                url: 'ReMemberEdit/Save',
                onSubmit: function () {
                    var ok = $(this).form('validate');
                    if (ok) {
                        $.messager.progress({
                            title: '请稍后',
                            msg: '正在处理中...',
                            interval: 100,
                            text: ''
                        });
                    }
                    return ok;
                },
                success: function (result) {
                    $.messager.progress("close");
                    var result = eval('(' + result + ')');
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showSuccessMsg(result.msg);
                        //关闭弹出框
                        $('#dlg').dialog("close");
                        //重刷grid
                        queryGrid();
                        if (result.msg.indexOf("" + tj + "") != -1) {
                            init();
                        }
                    }
                }
            });
            return false;
        })


        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[{ field: 'userId', title: '' + hybh + '', width: '16.5%' }]],
            columns: [[
                { field: 'userName', title: '' + hymc + '', width: '16.5%' },
                { field: 'oldReName', title: '' + jtjr + '', width: '16.5%' },
                { field: 'newReName', title: '' + xtjr + '', width: '16.5%' },
                { field: 'addTime', title: '' + xgrq + '', width: '17.5%' },
                { field: 'createUser', title: '' + czr + '', width: '16.5%' },
            ]],
            toolbar: [{
                text: "" + tj + "",
                iconCls: 'icon-add',
                handler: function () {
                    //清空表单数据
                    $('#editModalForm').form('reset');
                    $('#dlg').dialog({ title: '添加记录' }).dialog("open");
                }
            }
            ],
            url: "ReMemberEdit/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            if (objs != null) {
                for (name in objs) {
                    if (name == "userId2") { objs["userId"] = objs[name]; }
                    else if (name == "userName2") { objs["userName"] = objs[name]; }
                }
            }
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});