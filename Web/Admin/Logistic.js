﻿
define(['text!Logistic.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Logistic, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["物流商"];
        $("#center").panel("setTitle", titles);

        var pageHtml = Logistic;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        $("#saveBtn").bind("click", function () {
            utils.confirm("您确定从第三方接口同步吗，这个操作比较耗时？", function () {
                utils.AjaxPost("Logistic/Save", {}, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("操作成功");
                        queryGrid();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             { field: 'logNo', title: '物流商编号', width: '25%' },
             { field: 'logName', title: '物流商名称', width: '25%' },
             { field: 'addTime', title: '同步时间', width: '25%' },
            ]],
            url: "Logistic/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#queryBtn").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});