
define(['text!ShopPassing.html', 'jquery', 'j_easyui'], function (ShopPassing, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["待开通报单中心"];
        $("#center").panel("setTitle", titles);

        var pageHtml = ShopPassing;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#ShopPassing", toUid: 0 }, function () { });

        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '' + hybh + '', width: '18%' },
            {
                field: '_operate', title: '' + cz + '', width: '105', formatter: function (val, row, index) {
                    return '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildEdit" >确认</a>&nbsp;&nbsp;' +
                        '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildDelete" >' + sc + '</a>';
                }
            }]],
            columns: [[
                { field: 'userName', title: '' + hymc + '', width: '18%' },
                { field: 'uLevel', title: '' + hyjb + '', width: '18%' },
                { field: 'regAgentmoney', title: '' + lanager["汇款金额"] + '', width: '18%' },
                { field: 'applyAgentTime', title: '' + lanager["申请日期"] + '', width: '18%' }
            ]],
            url: "ShopPassing/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["applyAgentTime"] = utils.changeDateFormat(data.rows[i]["applyAgentTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                }
            }
            return data;
        }, function () {
            //行开通确认按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("该会员确定要开通报单中心吗？", function () {
                        utils.AjaxPost("ShopPassing/Save", { memberId: memberId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("确定要删除该记录吗？", function () {
                        utils.AjaxPost("ShopPassing/Delete", { memberId: memberId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {

        };
    };

    return controller;
});