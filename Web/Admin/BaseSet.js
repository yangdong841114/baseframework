
define(['text!BaseSet.html', 'jquery'], function (BaseSet, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["基础设置"];
        $("#center").panel("setTitle", titles);
        

        utils.AjaxPostNotLoadding("BaseSet/InitView", {}, function (result) {
            if (result.status=="fail") {
                utils.showErrMsg(result.msg);
            } else {
                var pageHtml = BaseSet;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);

                var map = result.map;
                //基础设置
                var dto = map["baseSet"];
                if (dto && dto != null) {
                    for (name in dto) {
                        if ($("#"+ name) ) {
                            $("#" + name).val(dto[name]);
                        }
                    }
                }

                //广告图
                var banners = map["banners"];
                if (banners && banners.length > 0) {
                    for (var i = 0; i < banners.length; i++) {
                        var ban = banners[i];
                        if ($("#banner" + ban.id)) {
                            var imgDom = $("#banner" + ban.id);
                            imgDom.attr("src", ban.imgUrl + "?etc=" + (new Date()).getTime());
                            imgDom.attr("data-image", ban.imgUrl + "?etc=" + (new Date()).getTime());
                        }
                    }
                }

                $(".img-thumbnail").each(function (index, ele) {
                    $(this).lightbox();
                })

                //广告图上传按钮
                $(".uploadBtn").each(function (index, ele) {
                    var id = this.id;
                    var idx = id.substring(id.indexOf("upload") + 6, id.length);
                    $(this).on('click', function () {
                        var img = $("#banner" + idx);
                        var file = $("#file" + idx);
                        var parent = file.parent();
                        if (file.val() == 0) {
                            parent.addClass("has-error");
                            utils.showPopover(file, "请选择上传的文件", "popover-danger");
                        } else {
                            var formdata = new FormData();
                            formdata.append("id", idx);
                            formdata.append("img", file[0].files[0]);

                            utils.AjaxPostForFormData("BaseSet/UploadBanner", formdata, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("上传成功！");
                                    file.val("");
                                    var url = result.msg + "?etc=" + (new Date()).getTime();
                                    img.remove();
                                    var newImg = '<img data-toggle="lightbox" id="banner' + idx + '" src="' + url + '" data-image="' + url + '" data-caption="广告' + idx + '" class="img-thumbnail" alt="">';
                                    $("#div" + idx).append($(newImg));
                                    $("#banner" + idx).lightbox();
                                }
                            });

                        }
                    });
                });

                //获取焦点事件
                $(".form-control").each(function (index, obj) {
                    var current = $(this);
                    var parent = current.parent();
                    current.on("focus", function () {
                        parent.removeClass("has-error");
                        utils.destoryPopover(current);
                    });
                })

                //保存
                $("#saveBtn").on('click', function () {
                    var objs = $("#BaseSetForm").serializeObject();
                    var checked = true;
                    //非空校验
                    $.each(objs, function (name, val) {
                        var current = $("#" + name);
                        var parent = current.parent();
                        if (val == 0) {
                            parent.addClass("has-error");
                            utils.showPopover(current, "不能为空", "popover-danger");
                        }
                        if (parent.hasClass("has-error")) {
                            checked = false;
                        }
                    });
                    //提交表单
                    if (checked) {
                        utils.AjaxPost("BaseSet/Save", objs, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                         });

                    }
                })
            }
        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});