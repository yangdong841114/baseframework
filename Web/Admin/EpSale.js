
define(['text!EpSale.html', 'jquery', 'j_easyui', 'datetimepicker'], function (EpSale, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["挂卖记录"];
        $("#center").panel("setTitle", titles);
        var pageHtml = EpSale;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        var statusDto = { 0: "挂卖中", 1: "部分售出", 2: "全部售出", 3: "已完成", 4: "已取消" };

        //初始化下拉框
        $("#flag").empty();
        $("#flag").append("<option value='-1'>--全部--</option>");
        $("#flag").append("<option value='0'>挂卖中</option>");
        $("#flag").append("<option value='1'>部分售出</option>");
        $("#flag").append("<option value='2'>全部售出</option>");
        $("#flag").append("<option value='3'>已完成</option>");

        //初始化日期选择框
        $(".form-date").datetimepicker(
            {
                language: "zh-CN",
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

        $('#dlg2').dialog({
            title: '批量购买',
            width: 644,
            height: 475,
            cache: false,
            modal: true
        });

        //批量购买保存按钮
        $("#saveBatchBtn").bind("click", function () {

            var rows = batchGrid.datagrid('getRows');
            if (!rows || rows.length == 0) {
                utils.showErrMsg("您还没有勾选任何挂卖记录");
            } else {
                var checked = true
                var data = {};
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    var val = $("#buy" + row.id).val();
                    if (val == 0 || isNaN(val) || val < 0) {
                        utils.showErrMsg("第" + (i + 1) + "行的购买数量格式错误");
                        checked = false;
                        return;
                    } else if (row.waitNum < val) {
                        utils.showErrMsg("第" + (i + 1) + "行的购买数量超出待售数量");
                        checked = false;
                        return;
                    }
                    data["list[" + i + "].sid"] = row.id;
                    data["list[" + i + "].buyNum"] = val;
                    data["list[" + i + "].snumber"] = row.number;
                }
                if (checked) {
                    utils.AjaxPost("EpSale/SaveRecordBatch", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg("批量购买成功");
                            //关闭弹出框
                            $('#dlg2').dialog("close");
                            //重刷grid
                            queryGrid();

                        }
                    });
                }
            }
        })

        //批量购买按钮
        $("#batchBuyBtn").bind("click", function () {
            var rows = grid.datagrid("getSelections");
            if (rows == null || rows.length == 0) { utils.showErrMsg("请选择需要购买的挂卖记录！"); }
            else {
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i].waitNum == 0) {
                        utils.showErrMsg("挂卖单号【" + rows[i].number + "】待售数量为0！");
                        return;
                    }
                    rows[i]["buyNum"] = rows[i].waitNum;
                }
                batchGrid.datagrid('loadData', rows);

                $('#dlg2').dialog("open");
            }
        })

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //初始化批量购买表格
        var batchGrid = utils.newGrid("dialogGird", {
            title: '勾选的挂卖记录',
            showFooter: false,
            pagination: false,
            columns: [[
                { field: 'number', title: '挂卖单号', width: '170' },
                { field: 'userId', title: '卖家编号', width: '100' },
                {
                    field: 'buyNum', title: '' + lanager["商品购买数量名称"] + '', width: '100', formatter: function (val, row, index) {
                        return '<input type="text" class="form-control" id="buy' + row.id + '"  style="width:98%;color:red;height:23px;" value="' + row.buyNum + '"/>';
                    }
                },
                //{ field: 'buyNum', title: '购买数量', width: '90' },
                { field: 'waitNum', title: '待出售数量', width: '90' },
                { field: 'addTime', title: '挂卖日期', width: '130' },
            ]],
        });

        //初始化表格
        var grid = utils.newGrid("dg", {
            singleSelect: false, //禁用单选
            frozenColumns: [[
                {
                    field: '_operate', title: '' + cz + '', width: '160', align: 'center', formatter: function (val, row, index) {
                        var ss = "";
                        if (row.flag == 0 || row.flag == 1) {
                            ss = '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildEdit" >取消挂单</a>&nbsp;&nbsp;';
                        }
                        ss += '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildTo" >交易明细</a>';
                        return ss;
                    }
                }]],
            columns: [[
                { field: 'ck', title: '文本', checkbox: true, },
                { field: 'number', title: '挂卖单号', width: '170' },
                { field: 'userId', title: '卖家编号', width: '100' },
                { field: 'phone', title: '手机号', width: '100' },
                { field: 'qq', title: 'QQ', width: '100' },
                { field: 'saleNum', title: '挂卖数量', width: '70' },
                { field: 'waitNum', title: '待售数量', width: '70' },
                { field: 'scNum', title: '已售数量', width: '70' },
                { field: 'addTime', title: '挂卖日期', width: '130' },
                { field: 'status', title: '状态', width: '80' }
            ]],
            onDblClickRow: function (index, data) {
                location.href = '#EpDetail/' + data.id;
            },
            url: "EpSale/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["status"] = statusDto[data.rows[i]["flag"]];
                }
            }
            return data;
        }, function () {
            //行交易明细
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("您确定要取消挂卖吗？", function () {
                        utils.AjaxPost("EpSale/SaveCancel", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                queryGrid();

                            }
                        });
                    });
                }
            });
            //行交易明细
            $(".gridFildTo").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    var lc = "#EpDetail/" + dataId;
                    location.href = "#EpDetail/" + dataId;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#EpSaleQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});