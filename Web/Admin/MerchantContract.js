
define(['text!MerchantContract.html', 'jquery'], function (MerchantContract, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["商家合同"];
        $("#center").panel("setTitle", titles);
        

        utils.AjaxPostNotLoadding("MerchantContract/InitView", {}, function (result) {
            if (result.status == "success") {

                var pageHtml = MerchantContract;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);

                var cont = "";
                if (result.result) {
                    cont = result.result.content;
                    $("#title").val(result.result.title);
                }

                //初始化编辑器
                $("#editor").height(document.body.offsetHeight - 270);
                var editor = new Quill("#editor", {
                    modules: {
                        toolbar: utils.getEditorToolbar()
                    },
                    theme: 'snow'
                });
                utils.setEditorHtml(editor, cont)

                $("#saveBtn").on("click", function () {
                    if ($("#title").val() == 0) {
                        utils.showErrMsg("请输入主题");
                    }
                    else if (editor.getText()==0) {
                        utils.showErrMsg("请输入内容");
                    } else {
                        utils.AjaxPost("MerchantContract/SaveByUeditor", { content: utils.getEditorHtml(editor), title: $("#title").val() }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("保存成功");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }
                })

            }
        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});