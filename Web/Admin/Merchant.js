
define(['text!Merchant.html', 'jquery', 'j_easyui', 'datetimepicker'], function (Merchant, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["加盟商家"];
        $("#center").panel("setTitle", titles);

        var pageHtml = Merchant;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        //初始化表单区域
        $('#dlgMerchantbh').dialog({
            title: '驳回原因',
            width: 850,
            height: 200,
            cache: false,
            modal: true
        })
        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //打开明细
        openDetailDialogMerchantbh = function (id) {
            $("#id").val(id);
            $('#dlgMerchantbh').dialog({ title: '驳回原因' }).dialog("open");
        }

        //驳回
        $("#saveBtnMerchantbh").bind("click", function () {
            if ($("#refuseReason").val() == 0) {
                utils.showErrMsg("请输入驳回原因");
            }
           else {
                utils.AjaxPost("Merchant/SaveRefuse", { refuseReason: $("#refuseReason").val(), id: $("#id").val() }, function (result) {
                    if (result.status == "success") {
                        utils.showSuccessMsg("驳回成功");
                        $('#dlgMerchantbh').dialog("close");
                        queryGrid();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            }
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            rownumbers: false,
            title:'加盟商家列表',
            columns: [[
                 {
                     field: 'imgUrl', title: '商家图片', width: '100', formatter: function (val, row, index) {
                         return '<img data-toggle="lightbox" src="' + row.imgUrl + '" data-image="' + row.imgUrl + '" data-caption="商家图片" class="img-thumbnail" alt="" width="100">';
                     }
                 },
                 {
                     field: 'imgYyzzUrl', title: '营业执照', width: '100', formatter: function (val, row, index) {
                         return '<img data-toggle="lightbox" src="' + row.imgYyzzUrl + '" data-image="' + row.imgYyzzUrl + '" data-caption="营业执照" class="img-thumbnail" alt="" width="100">';
                     }
                 },
                 {
                     field: 'imgSfzzmUrl', title: '身份证正面', width: '100', formatter: function (val, row, index) {
                         return '<img data-toggle="lightbox" src="' + row.imgSfzzmUrl + '" data-image="' + row.imgSfzzmUrl + '" data-caption="身份证正面" class="img-thumbnail" alt="" width="100">';
                     }
                 },
                 {
                     field: 'imgSfzfmUrl', title: '身份证反面', width: '100', formatter: function (val, row, index) {
                         return '<img data-toggle="lightbox" src="' + row.imgSfzfmUrl + '" data-image="' + row.imgSfzfmUrl + '" data-caption="身份证反面" class="img-thumbnail" alt="" width="100">';
                     }
                 },
             { field: 'userId', title: '节点编号', width: '100' },
             { field: 'userName', title: '昵称', width: '100' },
             { field: 'name', title: '商家名称', width: '100' },
             { field: 'linkMan', title: '联系人', width: '100' },
             { field: 'linkPhone', title: '' + lxdh + '', width: '100' },
             { field: 'linkAddress', title: '联系地址', width: '200' },
             { field: 'businessScope', title: '经营范围', width: '100' },
             { field: 'addTime', title: '' + lanager["申请日期"] + '', width: '130' },
             { field: 'status', title: '状态', width: '80' },
             { field: 'opUserId', title: '' + czr + '', width: '80' },
             {
                 field: '_operate', title: '' + cz + '', width: '120', formatter: function (val, row, index) {
                     var tt = "";
                     if (row.flag == 1) {
                         tt += '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildAduit" >确认</a>&nbsp;&nbsp;' +
                              '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildRefuse" >驳回</a>&nbsp;&nbsp;'+
                               '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildDelete" >' + sc + '</a>&nbsp;&nbsp;';
                     }
                     tt += '<a href="javascript:void(0);" dataUid="' + row.uid + '" class="gridFildList" >商品列表</a>';
                     return tt;
                }
             }
            ]],
            url: "Merchant/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i]["status"] = data.rows[i]["flag"] == 1 ? "待审" : "已审核";
                    data.rows[i]["linkAddress"] = data.rows[i]["province"] + "" + data.rows[i]["city"] + "" + data.rows[i]["area"] + "" + data.rows[i]["address"];
                }
            }
            return data;
        }, function () {
            $(".img-thumbnail").each(function (index, ele) {
                $(this).lightbox();
            });
            //行确认按钮
            $(".gridFildAduit").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定审核通过吗？", function () {
                        utils.AjaxPost("Merchant/AuditRecord", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                }
            });

            //点击驳回
            $(".gridFildRefuse").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    openDetailDialogMerchantbh(dataId);
                }
            })

            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("确定要删除吗？", function () {
                        utils.AjaxPost("Merchant/Delete", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("操作成功");
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                }
            });

            //商品列表
            $(".gridFildList").each(function (i, dom) {
                dom.onclick = function () {
                    var dataUid = $(dom).attr("dataUid");
                    location.href = "#MProduct/" + dataUid;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#MerchantQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        
        controller.onRouteChange = function () {
        };
    };

    return controller;
});