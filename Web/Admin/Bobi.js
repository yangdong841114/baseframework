
define(['text!Bobi.html', 'jquery', 'j_easyui'], function (Bobi, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["拨比查询"];
        $("#center").panel("setTitle", titles);
        

        utils.AjaxPostNotLoadding("Bobi/GetTotalBobi", {}, function (result) {
            if (result.status == 'success') {

                var pageHtml = Bobi;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);

                //初始化总拨比表格
                var tgrid = utils.newGrid("totalDg", {
                    pagination:false,
                    columns: [[
                        { field: 'income', title: '总收入', width: '25%' },
                        { field: 'outlay', title: '总支出', width: '25%' },
                        { field: 'profit', title: '总盈利', width: '25%' },
                        { field: 'bili', title: '总拨出比率', width: '25%' },
                    ]],
                }, null)
                var dto = result.result;
                if (dto) {
                    tgrid.datagrid("loadData", [dto]);
                }

                //初始化日拨比表格
                var grid = utils.newGrid("dg", {
                    columns: [[
                        { field: 'jstime', title: '结算日期', width: '20%' },
                        { field: 'income', title: '本日收入', width: '20%' },
                        { field: 'outlay', title: '本日支出', width: '20%' },
                        { field: 'profit', title: '本日盈利', width: '20%' },
                        { field: 'bili', title: '日拨出比率', width: '20%' },
                    ]],
                    url: "Bobi/GetBoBiListPage"
                }, null, function (data) {
                    if (data && data.rows) {
                        for (var i = 0; i < data.rows.length; i++) {
                            data.rows[i]["jstime"] = utils.changeDateFormat(data.rows[i]["jstime"], 'date');
                        }
                    }
                    return data;
                })
            }
        })
        
        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});