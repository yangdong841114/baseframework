
define(['text!Website.html', 'jquery'], function (Website, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["前台设置"];
        $("#center").panel("setTitle", titles);
        

        utils.AjaxPostNotLoadding("Website/InitView", {}, function (result) {
            if (result.status=="fail") {
                utils.showErrMsg(result.msg);
            } else {

                var pageHtml = Website;
                $.each(lanagerList, function (index, data) {
                    if (pageHtml.indexOf(data.key)) {
                        var length = pageHtml.split(data.key).length;
                        if (length > 1) {
                            for (var i = 0; i < length; i++) {
                                pageHtml = pageHtml.replace(data.key, data.value);
                            }
                        }
                    }
                });
                appView.html(pageHtml);

                var dto = result.result;
                $("#msg").val(dto.msg);
                if (dto.isclose == 1) {
                    $("#isclose").attr("checked", true);
                }

                $("#msg").on("focus", function () {
                    $("#msg").removeClass("inputError");
                });

                //保存
                $("#saveBtn").on('click', function () {
                    var checked = $("#isclose")[0].checked;     //复选框
                    var val = $("#msg").val();
                    if (checked==false && val == 0) {
                        $("#msg").addClass("inputError");
                    } else {
                        var data = { id: 1, isclose: (checked ? 1 : 2), msg: (val == 0 ? "" : val) };
                        utils.AjaxPost("Website/Save", data, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }
                    
                })
            }
        });

        controller.onRouteChange = function () {
        };
    };

    return controller;
});