
define(['text!DownLoadFile.html', 'jquery', 'j_easyui', 'datetimepicker'], function (DownLoadFile, $) {

    var controller = function (name) {
        //设置标题
        var titles = lanager["下载专区"];
        $("#center").panel("setTitle", titles);
        var pageHtml = DownLoadFile;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);

        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //初始化按钮
        $(".easyui-linkbutton").each(function (i, ipt) {
            $(ipt).linkbutton({});
        });

        //保存按钮
        $("#saveBtn").bind("click", function () {
            var checked = true;
            if ($("#title").val() == 0) {
                utils.showErrMsg("主题不能为空");
            }
            else if($("#file").val() == 0) {
                utils.showErrMsg("请选择上传文件");
            }
            else{
                var formdata = new FormData();
                formdata.append("title", $("#title").val());
                formdata.append("file", $("#file")[0].files[0]);

                $.ajax({
                    url: "DownLoadFile/UploadFile",           //请求地址
                    type: "POST",       //POST提交
                    data: formdata,         //数据参数
                    cache: false,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        //显示全屏loadding遮罩层
                        utils.showFullScreenLoadding();
                    },
                    success: function (result) {
                        //隐藏全屏loadding遮罩层
                        utils.hideFullScreenLoadding();
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg("操作成功！");
                            //关闭弹出框
                            $('#dlg').dialog("close");
                            queryGrid();
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        utils.hideFullScreenLoadding();//隐藏loadding遮罩层
                        utils.showErrMsg("操作失败！");
                    }
                });
            }
        })

        //弹出框
        $('#dlg').dialog({
            title: '上传文件',
            width: 400,
            height: 250,
            cache: false,
            modal: true
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            columns: [[
             {
                 field: '_downTitle', title: '主题', width: '500', formatter: function (val, row, index) {
                     return '<a href="' + row.fileUrl + '" >' + row.title + '</a>';
                 }
             },
             { field: 'addUser', title: '上传会员', width: '150' },
             { field: 'addTime', title: '上传日期', width: '150' },
             {
                 field: '_operate', title: '' + cz + '', width: '60', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" dataId="' + row.id + '" class="gridFildDelete" >' + sc + '</a>';
                }
             }
            ]],
            toolbar: [{
                text: "上传文件",
                iconCls: 'icon-add',
                handler: function () {
                    $("#title").val("");
                    $("#file").val("");
                    $('#dlg').dialog("open");
                }
            }
            ],
            url: "DownLoadFile/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                }
            }
            return data;
        }, function () {
            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var dataId = $(dom).attr("dataId");
                    utils.confirm("您确定要删除吗？", function () {
                        utils.AjaxPost("DownLoadFile/Delete", { id: dataId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("删除成功！");
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            if (objs) {
                for (name in objs) {
                    if (name == "title2") { objs["title"] = objs[name]; }
                }
            }
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#dlg').dialog("destroy");
        };
    };

    return controller;
});