
define(['text!NotFound.html', 'jquery', 'zui', 'css!../Content/css/styleAdmin.css'], function (NotFound, $) {

    var controller = function (name) {

        var pageHtml = NotFound;
        $.each(lanagerList, function (index, data) {
            if (pageHtml.indexOf(data.key)) {
                var length = pageHtml.split(data.key).length;
                if (length > 1) {
                    for (var i = 0; i < length; i++) {
                        pageHtml = pageHtml.replace(data.key, data.value);
                    }
                }
            }
        });
        appView.html(pageHtml);
        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});