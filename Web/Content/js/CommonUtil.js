
//一些公用JS
define(['jquery', 'quill', 'j_easyui'], function ($,Quiil) {

    var utils = function () {
        
        var self = this;

        this.popList = [];

        //获取富文本内容
        this.getEditorHtml = function (editor) {
            return editor.container.firstChild.innerHTML;
        }

        //设置富文本内容
        this.setEditorHtml = function (editor,cont) {
            editor.container.firstChild.innerHTML = cont;
        }

        //获取富文本标题头部
        this.getEditorToolbar = function(){
            return [['bold', 'italic', 'underline', 'strike'], [{ 'size': ['small', false, 'large', 'huge'] }], [{ 'color': [] }, { 'background': [] }], ['link', 'image'], [{ 'align': [] }]];
        }

        //确认对话框
        //msg：提示消息
        //okCallBack:用户点击确认后的回调函数
        //cancelCallBack:用户点击取消后的回调函数
        this.confirm = function (msg,okCallBack,cancelCallBack) {
            $.messager.confirm({
                ok: "确定",
                cancel: "取消",
                title: "提示",
                msg: msg,
                fn: function (ok) {
                    if (ok) { okCallBack(); }
                    else { cancelCallBack(); }
                }
            });
        }

        //如需要文件上传请用此方法
        //post ajax请求
        //url:请求地址
        //data:请求参数
        //successCallBack:请求成功的回调函数
        //errorCallBack:请求失败的回调函数
        this.AjaxPostForFormData = function (url, data, successCallBack, errorCallBack) {
            $.ajax({
                url: url,           //请求地址
                type: "POST",       //POST提交
                data: data,         //数据参数
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    //显示全屏loadding遮罩层
                    self.showFullScreenLoadding();
                },
                success: function (result) {
                    //隐藏全屏loadding遮罩层
                    self.hideFullScreenLoadding();
                    if (result == "NotPermission") { location.href = "#NoPermission";}    //无权限访问
                    else if (result == "NotAdminLogin") { location.href = "/ManageLogin.html"; } //需要后台登录
                    else if (result == "NotUserLogin") { location.href = "/Home/Index"; } //需要前台登录
                    else {
                        //如有回调函数则调用
                        //没有回调函数默认显示操作成功消息框
                        if (successCallBack) {
                            successCallBack(result);
                        } else {
                            self.showSuccessMsg("操作成功！")
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    self.hideFullScreenLoadding();//隐藏loadding遮罩层
                    //如有回调函数则调用
                    //没有回调函数默认显示操作失败消息框
                    if (errorCallBack) {
                        errorCallBack(XMLHttpRequest, textStatus, errorThrown);
                    } else {
                        self.showErrMsg("操作失败！");
                    }
                }
            });
        }

        //post ajax请求
        //url:请求地址
        //data:请求参数
        //successCallBack:请求成功的回调函数
        //errorCallBack:请求失败的回调函数
        this.AjaxPost = function (url,data,successCallBack,errorCallBack) {
            $.ajax({
                url: url,           //请求地址
                type: "POST",       //POST提交
                data: data,         //数据参数
                beforeSend: function () {
                    //显示全屏loadding遮罩层
                    self.showFullScreenLoadding(); 
                },
                success: function (result) {
                    //隐藏全屏loadding遮罩层
                    self.hideFullScreenLoadding();
                    //如有回调函数则调用
                    //没有回调函数默认显示操作成功消息框
                    if (result == "NotPermission") { location.href = "#NoPermission";}    //无权限访问
                    else if (result == "NotAdminLogin") { location.href = "/ManageLogin.html"; } //需要后台登录
                    else if (result == "NotUserLogin") { location.href = "/Home/Index"; } //需要前台登录
                    else {
                        if (successCallBack) {
                            successCallBack(result);
                        } else {
                            self.showSuccessMsg("操作成功！")
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    self.hideFullScreenLoadding();//隐藏loadding遮罩层
                    //如有回调函数则调用
                    //没有回调函数默认显示操作失败消息框
                    if (errorCallBack) {
                        errorCallBack(XMLHttpRequest, textStatus, errorThrown);
                    } else {
                        self.showErrMsg("操作失败！");
                    }
                }
            });
        }

        this.AjaxPostNotLoadding = function (url, data, successCallBack, errorCallBack) {
            $.ajax({
                url: url,           //请求地址
                type: "POST",       //POST提交
                data: data,         //数据参数
                success: function (result) {
                    //如有回调函数则调用
                    //没有回调函数默认显示操作成功消息框
                    if (result == "NotPermission") { location.href = "#NoPermission";}    //无权限访问
                    else if (result == "NotAdminLogin") { location.href = "/ManageLogin.html"; } //需要后台登录
                    else if (result == "NotUserLogin") { location.href = "/Home/Index"; } //需要前台登录
                    else {
                        if (successCallBack) {
                            successCallBack(result);
                        } else {
                            self.showSuccessMsg("操作成功！")
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //如有回调函数则调用
                    //没有回调函数默认显示操作失败消息框
                    if (errorCallBack) {
                        errorCallBack(XMLHttpRequest, textStatus, errorThrown);
                    } else {
                        self.showErrMsg("操作失败！");
                    }
                }
            });
        }

        //创建表格
        //domId对应dom节点ID
        //options:明细参数
        //loader:自定义加载方法
        //loadFilter:加载时数据过滤方法
        //详细信息请参考easyui中的datagird.地址：http://www.jeasyui.net/plugins/183.html
        this.newGrid = function (domId, options, loader, loadFilter,loadSuccess) {
            if (!options) {
                options = {};
            }
            options["loadMsg"] = "数据正在加载中..."; //加载时显示的信息
            if (options.rownumbers != false ) {
                options["rownumbers"] = true; //显示行号
            }
            
            if(loader){
                options["loader"] = loader; //自实现请求加载方法
            }
            if (loadFilter) {
                options["loadFilter"] = loadFilter; //数据过滤函数
            }
            
            options["striped"] = true;  //隔行换色
            if (options["singleSelect"]==undefined) {
                options["singleSelect"] = true;  //是否单选
            }
            if (options["pagination"] != false) {
                options["pagination"] = true; //分页控件
            }
            var size = 30;
            if (!options["pageSize"]) {
                options["pageSize"] = size;
            }
            size = options["pageSize"];

            options["onLoadSuccess"] = function (data) {
                if (loadSuccess) {
                    loadSuccess(data);
                }
            };
            options["onLoadError"] = function (data) {
                if (data.responseText == "NotPermission") { location.href = "#NoPermission"; }    //无权限访问
                else if (data.responseText == "NotAdminLogin") { location.href = "/ManageLogin.html"; } //需要后台登录
                else if (data.responseText == "NotUserLogin") { location.href = "/Home/Index"; } //需要前台登录
            }
            
            //options["fitColumns"] = true;
            options["nowrap"] = true;
            
            var grid = $('#' + domId).datagrid(options);
            if (options["pagination"] != false) {
                $('#' + domId).datagrid('getPager').pagination({
                    pageSize: size,
                    pageNumber: 1,
                    pageList: [10, 20, 30, 40, 50],
                    beforePageText: '第',//页数文本框前显示的汉字   
                    afterPageText: '页    共 {pages} 页',
                    displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录',
                });
            }
            return grid;
        }

        //显示全屏loadding
        this.showFullScreenLoadding = function () {
            $.messager.progress({
                title: '请稍后',
                msg: '正在处理中...',
                interval: 100,
                text: ''
            });
            //$('#mySmModal').modal('show');
        }

        //隐藏全屏loadding
        this.hideFullScreenLoadding = function () {
            $.messager.progress("close");
            //$('#mySmModal').modal('hide');
        }

        //显示红色失败边线框，obj为显示的dom
        this.addErrorClass = function (obj) {
            obj.removeClass("has-success");
            obj.addClass("has-error");
        }

        //显示绿色成功边线框，obj为显示的dom
        this.addSuccessClass = function (obj) {
            obj.removeClass("has-error");
            obj.addClass("has-success");
        }

        //销毁弹出层，obj为所属层dom
        this.destoryPopover = function (obj) {
            if (obj.popover) {
                obj.popover("destroy");
            }
        }

        //清空所有弹出层
        this.DestoryAllPopover = function(){
            if (self.popList && self.popList.length > 0) {
                for (var i = 0; i < self.popList.length; i++) {
                    self.destoryPopover(self.popList[i]);
                }
                self.popList = [];
            }
        }

        //显示弹出层,obj为所属层dom,msg:提示信息,cls:红色或绿色，默认绿色
        //有关详细信息请参考ZUI.js中的popover（弹出面板）章节
        this.showPopover = function (obj, msg, cls, container) {
            if (!cls) {
                cls = "popover-success";
            }
            //if (!container) {
            //    container = 'body';
            //}
            var color = cls == "popover-success" ? "green" : "red";
            var options = {
                container: 'body',  //在body层显示
                tipClass: cls,      //样式，popover-success：绿色，popover-danger：红色
                trigger: 'manual',  //需手动显示
                template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content" style="color:' + color + '; width:max-content;"></div></div>',
                content: msg        //显示文本
            };
            if (container || container==false) {
                options["container"] = container;
            }
            var popDiv = obj.popover(options);   //构建
            popDiv.popover("show");    //显示
            self.popList.push(popDiv);
        }

        //显示错误消息
        this.showErrMsg = function (msg) {
            var msgbox = new $.zui.Messager('提示消息：'+msg, {
                type: 'danger',
                icon: 'warning-sign',
                placement: 'center',
                parent:'body',
                close: true
            });
            msgbox.show();
        }

        //显示成功消息
        this.showSuccessMsg = function (msg) {
            var msgbox = new $.zui.Messager('提示消息：' + msg, {
                type: 'success',
                icon: 'check',
                placement: 'center',
                parent: 'body',
                close: true
            });
            msgbox.show();
        }

        //asp.net mvc返回的datatime转换显示格式
        //val：需转换的值
        //format: time-> yyyy-mm-dd hh:mi:ss   date-> yyyy-mm-dd
        this.changeDateFormat = function(val,format) {
            if (val != null) {
                if (!format)
                    format = "time";
                var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
                //月份为0-11，所以+1，月份小于10时补个0
                var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
                var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                var dd = "";
                if (format == "time") {
                    var hour = date.getHours();
                    var minute = date.getMinutes();
                    var second = date.getSeconds();
                    var hs = hour > 9 ? hour : ("0" + hour);
                    var ms = minute > 9 ? minute : ("0" + minute);
                    var ss = second > 9 ? second : ("0" + second);
                    dd = date.getFullYear() + "-" + month + "-" + currentDate + " " + hs + ":" + ms + ":" + ss;
                } else if (format == "date") {
                    dd = date.getFullYear() + "-" + month + "-" + currentDate;
                }
                
                return dd;
            }

            return "";
        }


        //序列化表单，包含radio,checkbox
        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            var $radio = $('input[type=radio],input[type=checkbox]', this);
            $.each($radio, function () {
                if (!o.hasOwnProperty(this.name)) {
                    o[this.name] = '';
                }
            });
            return o;
        };

        //扩展easyui validatebox 验证规则
        $.extend($.fn.validatebox.defaults.rules, {
            idcard: {// 验证身份证
                validator: function (value) {
                    return /^\d{15}(\d{2}[A-Za-z0-9])?$/i.test(value);
                },
                message: '身份证号码格式不正确'
            },
            minLength: {
                validator: function (value, param) {
                    return value.length >= param[0];
                },
                message: '请输入至少（2）个字符.'
            },
            length: {
                validator: function (value, param) {
                    var len = $.trim(value).length;
                    return len >= param[0] && len <= param[1];
                },
                message: "输入内容长度必须介于{0}和{1}之间."
            },
            phone: {// 验证电话号码
                validator: function (value) {
                    return /^((\d2,3)|(\d{3}\-))?(0\d2,3|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value);
                },
                message: '格式不正确,请使用下面格式:020-88888888'
            },
            mobile: {// 验证手机号码
                validator: function (value) {
                    return /^(13|15|18)\d{9}$/i.test(value);
                },
                message: '手机号码格式不正确'
            },
            intOrFloat: {// 验证整数或小数
                validator: function (value) {
                    return /^\d+(\.\d+)?$/i.test(value);
                },
                message: '请输入数字，并确保格式正确'
            },
            currency: {// 验证货币
                validator: function (value) {
                    return /^\d+(\.\d+)?$/i.test(value);
                },
                message: '货币格式不正确'
            },
            noZeroNumber: {// 非0的数字
                validator: function (value) {
                    return /^(\+|-)?([1-9][0-9]*(\.\d+)?|(0\.(?!0+$)\d+))$/i.test(value);
                },
                message: '金额格式不正确'
            },
            qq: {// 验证QQ,从10000开始
                validator: function (value) {
                    return /^[1-9]\d{4,9}$/i.test(value);
                },
                message: 'QQ号码格式不正确'
            },
            integer: {// 验证整数 可正负数
                validator: function (value) {
                    //return /^[+]?[1-9]+\d*$/i.test(value);

                    return /^([+]?[0-9])|([-]?[0-9])+\d*$/i.test(value);
                },
                message: '请输入整数'
            },
            age: {// 验证年龄
                validator: function (value) {
                    return /^(?:[1-9][0-9]?|1[01][0-9]|120)$/i.test(value);
                },
                message: '年龄必须是0到120之间的整数'
            },

            chinese: {// 验证中文
                validator: function (value) {
                    return /^[\Α-\￥]+$/i.test(value);
                },
                message: '请输入中文'
            },
            english: {// 验证英语
                validator: function (value) {
                    return /^[A-Za-z]+$/i.test(value);
                },
                message: '请输入英文'
            },
            unnormal: {// 验证是否包含空格和非法字符
                validator: function (value) {
                    return /.+/i.test(value);
                },
                message: '输入值不能为空和包含其他非法字符'
            },
            username: {// 验证用户名
                validator: function (value) {
                    return /^[a-zA-Z][a-zA-Z0-9_]{5,15}$/i.test(value);
                },
                message: '用户名不合法（字母开头，允许6-16字节，允许字母数字下划线）'
            },
            faxno: {// 验证传真
                validator: function (value) {
                    //            return /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/i.test(value);
                    return /^((\d2,3)|(\d{3}\-))?(0\d2,3|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value);
                },
                message: '传真号码不正确'
            },
            zip: {// 验证邮政编码
                validator: function (value) {
                    return /^[1-9]\d{5}$/i.test(value);
                },
                message: '邮政编码格式不正确'
            },
            ip: {// 验证IP地址
                validator: function (value) {
                    return /d+.d+.d+.d+/i.test(value);
                },
                message: 'IP地址格式不正确'
            },
            name: {// 验证姓名，可以是中文或英文
                validator: function (value) {
                    return /^[\Α-\￥]+$/i.test(value) | /^\w+[\w\s]+\w+$/i.test(value);
                },
                message: '请输入姓名'
            },
            date: {// 验证姓名，可以是中文或英文
                validator: function (value) {
                    //格式yyyy-MM-dd或yyyy-M-d
                    return /^(?:(?!0000)[0-9]{4}([-]?)(?:(?:0?[1-9]|1[0-2])\1(?:0?[1-9]|1[0-9]|2[0-8])|(?:0?[13-9]|1[0-2])\1(?:29|30)|(?:0?[13578]|1[02])\1(?:31))|(?:[0-9]{2}(?:0[48]|[2468][048]|[13579][26])|(?:0[48]|[2468][048]|[13579][26])00)([-]?)0?2\2(?:29))$/i.test(value);
                },
                message: '清输入合适的日期格式'
            },
            msn: {
                validator: function (value) {
                    return /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
                },
                message: '请输入有效的msn账号(例：abc@hotnail(msn/live).com)'
            },
            same: {
                validator: function (value, param) {
                    return value == $(param[0]).val();
                    //if ($("#" + param[0]).val() != "" && value != "") {
                    //    return $("#" + param[0]).val() == value;
                    //} else {
                    //    return true;
                    //}
                },
                message: '两次输入的密码不一致！'
            }

        });
    };

    return new utils();
});