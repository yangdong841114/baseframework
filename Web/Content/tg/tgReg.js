


'use strict';

(function (win) {
    //配置baseUrl
    //var baseUrl = document.getElementById('main').getAttribute('data-baseurl');

    /*
     * 文件依赖
     */
    var config = {
        map: {
            '*': {
                'css': '../../Content/js/css.min'
            }
        },
        paths: {
            jquery: '../../Content/js/jquery-3.2.1.min',
            zui: '../../Content/js/zui',
            j_easyui: '../../Content/easyui/jquery.easyui.min'
        },
        shim: {                     //引入没有使用requirejs模块写法的类库。
            jquery: {
                exports: '$'
            },
            j_easyui: {
                exports: 'j_easyui',
                deps: ['jquery',
                    'css!../../Content/easyui/themes/material/easyui.css',
                    'css!../../Content/easyui/themes/icon.css',
                ]
            },
            zui: {
                exports: 'zui',
                deps: ['jquery', 'css!../../Content/css/zui2.css']
            },
            datetimepicker: {
                exports: 'datetimepicker',
                deps: ['jquery', 'zui', 'css!../../Content/css/datetimepicker.min.css']
            },
        }
    };

    require.config(config);
    require(['jquery', '../../Content/js/CommonUtil', 'j_easyui', 'zui'], function ($, utils) {

        function calRowMoney(grid,obj, index) {
            var re = /^[0-9]+$/;
            var rows = grid.datagrid("getRows");
            var row = rows[index];

            if (re.test(obj.value)) {
                row.total = obj.value * row.price;
            } else {
                row.total = 0;
            }
            $("#totalF" + row.id).html(row.total);
        }

        utils.AjaxPostNotLoadding("/Home/GetRegBank", {}, function (result) {

            if (result.status == "success") {

                //银行下啦框
                var bankList = result.map.bankList;
                $("#bankName").empty();
                $("#bankName").append("<option value=''>--请选择--</option>");
                if (bankList && bankList.length > 0) {
                    for (var i = 0; i < bankList.length; i++) {
                        $("#bankName").append("<option value='" + bankList[i].id + "'>" + bankList[i].name + "</option>");
                    }
                }

                var areaMap = null;
                var areaData = null;
                //区域
                areaMap = result.map["area"];
                if (areaMap) {
                    areaData = areaMap[0];
                    for (var i = 0; i < areaData.length; i++) {
                        var dto = areaData[i];
                        if (areaMap[dto.id]) {
                            dto["childs"] = areaMap[dto.id];
                            for (var j = 0; j < dto["childs"].length; j++) {
                                var child = dto["childs"][j];
                                if (areaMap[child.id]) {
                                    child["childs"] = areaMap[child.id];
                                }
                            }
                        }
                    }
                }

                //初始化省、市、区下拉框
                $("#city").empty();
                $("#city").append("<option value='0'>--请选择--</option>");
                $("#area").empty();
                $("#area").append("<option value='0' selected='true'>--请选择--</option>");
                $("#province").empty();
                $("#province").append("<option value='0'>--请选择--</option>");
                if (areaData && areaData.length > 0) {
                    for (var i = 0; i < areaData.length; i++) {
                        var pro = areaData[i];
                        $("#province").append("<option value='" + pro.value + "' dataId='" + pro.id + "' >" + pro.value + "</option>");
                    }
                }

                //省改变事件
                $("#province").bind("change", function (a, b, c) {
                    //获取自定义属性的值
                    var dataId = $(this).find("option:selected").attr("dataId");
                    $("#city").empty();
                    $("#city").append("<option value='0'>--请选择--</option>");
                    $("#area").empty();
                    $("#area").append("<option value='0'>--请选择--</option>");
                    if (areaMap[dataId] && areaMap[dataId].length > 0) {
                        for (var i = 0; i < areaMap[dataId].length; i++) {
                            var child = areaMap[dataId][i];
                            $("#city").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                        }
                    }
                });

                //市改变事件
                $("#city").bind("change", function (a, b, c) {
                    //获取自定义属性的值
                    var dataId = $(this).find("option:selected").attr("dataId");
                    $("#area").empty();
                    $("#area").append("<option value='0'>--请选择--</option>");
                    if (areaMap[dataId] && areaMap[dataId].length > 0) {
                        for (var i = 0; i < areaMap[dataId].length; i++) {
                            var child = areaMap[dataId][i];
                            $("#area").append("<option value='" + child.value + "' dataId='" + child.id + "'>" + child.value + "</option>");
                        }
                    }
                })


                //注册协议
                $('#regModal').bind('click', function () {
                    $('#regContent').dialog({
                        width: 700,
                        height: 600,
                        closed: false,
                        cache: false,
                        modal: true
                    });
                    return false;
                });

                //绑定获取焦点事件
                var inputs = $("#registerForm").serializeObject();
                $.each(inputs, function (name, val) {
                    var current = $("#" + name);
                    current.on("focus", function (event) {
                        current.removeClass("inputError");
                        utils.destoryPopover(current);
                    });
                    //ajax检查
                    if (current.attr("checkflag")) {
                 
                    } else {
                        current.on("blur", function (event) {
                            if (current.val() && current.val() != 0) {
                                current.removeClass("inputError");
                                utils.destoryPopover(current);
                            }
                        });
                    }
                });

                //重置表单
                function resetForm() {
                    $('#registerForm')[0].reset();
                    setDefaultData(pmap);
                };


                /********************************************** 编辑表格start *******************************************************/
                
                

                //初始化商品表格
                var grid = utils.newGrid("dg", {
                    title: "选择商品",
                    rownumbers: false,
                    singleSelect: false,
                    checkOnSelect: false,
                    pageSize: 10,
                    columns: [[
                        { field: 'ck', title: '文本', checkbox: true, },
                        {
                            field: '_img', title: '商品图片', width: '16%', formatter: function (val, row, index) {
                                return '<img data-toggle="lightbox" src="' + row.imgUrl + '" data-image="' + row.imgUrl + '" data-caption="商品图片" class="img-thumbnail" alt="" >';
                            }
                        },
                        { field: 'productCode', title: '商品编码', width: '16%' },
                        { field: 'productName', title: '商品名称', width: '16%' },
                        { field: 'price', title: '报单价', width: '16%' },
                        {
                            field: 'buyNum', title: '购买数量', width: '16%', formatter: function (val, row, index) {
                                return '<input type="text" class="calNum" id="buy' + row.id + '" dataIndex="'+index+'"  style="width:90%"/>';
                            }, styler: function (value, row, index) { return 'background-color:#ffee00;color:red;'; }
                        },
                        {
                            field: 'total', title: '购买金额', width: '16%', formatter: function (val, row, index) {
                                if (!row.total) {
                                    return '<font style="color:red;font-weight:bold" id="totalF' + row.id + '">0</font>';
                                } else {
                                    return '<font style="color:red;font-weight:bold" id="totalF' + row.id + '">' + row.total + '</font>';
                                }
                            }
                        },
                    ]],
                    url: "/Home/GetProductListPage"
                }, null, null, function () {
                    $(".img-thumbnail").each(function (index, ele) {
                        $(this).lightbox();
                    })
                    $(".calNum").each(function (index, ele) {
                        var d = this;
                        var index = $(this).attr("dataIndex");
                        $(this).bind("blur", function () {
                            calRowMoney(grid,d, index);
                        })
                    })
                });

                /********************************************** 编辑表格end *******************************************************/

                //保存start
                $("#saveBtn").on('click', function () {
                    var fs = $("#registerForm").serializeObject();
                    var checked = true;
                    var focus = false;
                    //非空校验
                    $.each(fs, function (name, val) {
                        var current = $("#" + name);  //被验证的input id
                        var before = current.parent().parent().children().eq(0); //是否必填
                        //userId需特殊处理
                        if (name == "userId") {
                            before = current.parent().parent().parent().children().eq(0);
                        }
                        var emptyMsg = current.attr("emptyMsg");
                        if (before.hasClass("required")) {
                            if (val == 0) {
                                //验证不通过的先获取焦点，再添加错误提示
                                if (!focus) {
                                    focus = true;
                                    current.focus();
                                }
                                checked = false;
                                current.addClass("inputError");
                                utils.showPopover(current, emptyMsg, "popover-danger");
                            }
                        }
                        if (current.hasClass("inputError")) {
                            checked = false;
                        }
                    });

                    if (checked) {
                        //手机号码验证
                        var ptext = /^1(3|4|5|7|8)\d{9}$/;
                        if (!ptext.test($("#phone").val())) {
                            var current = $("#phone");
                            //验证不通过的先获取焦点，再添加错误提示
                            if (!focus) {
                                focus = true;
                                current.focus();
                            }
                            current.addClass("inputError");
                            utils.showPopover(current, "手机号码格式错误", "popover-danger");
                            checked = false;
                        }
                        if ($("#passwordRe").val() != $("#password").val()) {
                            var current = $("#passwordRe");
                            //验证不通过的先获取焦点，再添加错误提示
                            if (!focus) {
                                focus = true;
                                current.focus();
                            }
                            current.addClass("inputError");
                            utils.showPopover(current, "确认登录密码与登录密码不一致", "popover-danger");
                            checked = false;
                        }
                        if ($("#passOpenRe").val() != $("#passOpen").val()) {
                            var current = $("#passOpenRe");
                            //验证不通过的先获取焦点，再添加错误提示
                            if (!focus) {
                                focus = true;
                                current.focus();
                            }
                            current.addClass("inputError");
                            utils.showPopover(current, "确认登录密码与登录密码不一致", "popover-danger");
                            checked = false;
                        }
                        if ($("#threepassRe").val() != $("#threepass").val()) {
                            var current = $("#threepassRe");
                            //验证不通过的先获取焦点，再添加错误提示
                            if (!focus) {
                                focus = true;
                                current.focus();
                            }
                            current.addClass("inputError");
                            utils.showPopover(current, "确认登录密码与登录密码不一致", "popover-danger");
                            checked = false;
                        }
                        if (checked) {
                            var objs = grid.datagrid("getChecked");
                            if (!objs || objs.length == 0) {
                                utils.showErrMsg("请选择购买商品");
                            } else {
                                //检查购买商品数量、金额
                                var totalMoney = 0; //购物总金额
                                for (var i = 0; i < objs.length; i++) {
                                    var row = objs[i];
                                    var buyNum = $("#buy" + row.id).val();
                                    var total = parseFloat($("#totalF" + row.id).html());
                                    if (!buyNum || buyNum <= 0) {
                                        utils.showErrMsg("请录入勾选商品【" + row.productName + "】的购买数量");
                                        checked = false;
                                        return;
                                    }
                                    totalMoney += total;
                                    //封装购买商品参数
                                    fs["orderItems[" + i + "].productId"] = row.id;
                                    fs["orderItems[" + i + "].num"] = buyNum;
                                    fs["orderItems[" + i + "].productName"] = row.productName;
                                }
                                if (totalMoney < parseFloat($("#pk").html())) {
                                    utils.showErrMsg("购买商品的金额不能成为普卡会员");
                                    checked = false;
                                }
                                if (checked) {
                                    fs["userId"] = $("#qzUserId").html() + fs["userId"];
                                    fs["reName"] = $("#reName").val();
                                    if ($("#read")[0].checked) { fs["read"] = "read"; }
                                    utils.AjaxPost("/Home/RegisterMember", fs, function (result) {
                                        if (result.status == "fail") {
                                            utils.showErrMsg(result.msg);
                                        } else {
                                            utils.showSuccessMsg("注册成功！");
                                            //清除弹出层
                                            utils.DestoryAllPopover();
                                            //清空表单
                                            resetForm();
                                            //登录页面
                                            location.href = "/Home/Index";
                                        }
                                    });
                                }
                            }
                        }
                    }
                }); ////保存end
            }
        })
    });


})(window);
