﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 登录历史实体
    /// </summary>
    [Serializable]
    public class LoginHistory : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }

        //用户ID
        public virtual int? uid { get; set; }

        //用户名称
        public virtual string userId { get; set; }

        //登录IP
        public virtual string loginIp { get; set; }

        //登录时间
        public virtual DateTime? loginTime { get; set; }

        //状态，0：成功，1：失败（验证失败），2：（失败）未开通，3：（失败）被锁定
        public virtual int? flag { get; set; }

        //0：前台登录 1：后台登录，
        public virtual int? isMan { get; set; }

        //描述
        public virtual string mulx { get; set; }

        //冗余字段
        public virtual string userName { get; set; }


        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
    }
}
