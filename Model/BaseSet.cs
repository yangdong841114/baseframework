﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 基础设置表实体
    /// </summary>
    [Serializable]
    public class BaseSet : DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //网站是否关闭，1：开启，2：关闭
        public virtual int? isclose { get; set; }
        //网站关闭时显示信息
        public virtual string WebCode { get; set; }
        public virtual string msg { get; set; }
        //QQ1号
        public virtual string qq1 { get; set; }
        public virtual string qName1 { get; set; }
        public virtual string qq2 { get; set; }
        public virtual string qName2 { get; set; }
        public virtual string qq3 { get; set; }
        public virtual string qName3 { get; set; }
        public virtual string qq4 { get; set; }
        public virtual string qName4 { get; set; }
        public virtual string video { get; set; }
        public virtual int? maxmember { get; set; }
        public virtual string sitename { get; set; }
        public virtual string copyright { get; set; }
    }
}
