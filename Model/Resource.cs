﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 资源实体
    /// </summary>
    [Serializable]
    public class Resource : DtoData
    {
        //主键，资源ID
        public virtual string id { get; set; }

        //资源名称
        public virtual string resourceName { get; set; }

        //父资源ID
        public virtual string parentResourceId { get; set; }

        //资源URL
        public virtual string curl { get; set; }

        //小图标URL
        public virtual string icon { get; set; }

        //前台显示的图片
        public virtual string imgUrl { get; set; }

        //是否菜单
        public virtual string isMenu { get; set; }

        //是否显示 0：显示，1：不显示
        public virtual int? isShow { get; set; }

        //是否需要验证安全、交易密码，2：验证安全密码，3：验证交易密码
        public virtual int? pass { get; set; }
    }
}
