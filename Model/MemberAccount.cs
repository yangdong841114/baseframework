﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 用户账户表实体
    /// </summary>
    [Serializable]
    public class MemberAccount : Page,DtoData
    {
        //用户ID
        public virtual int? id { get; set; }
        //电子币
        public virtual double? agentDz { get; set; }
        //奖金币
        public virtual double? agentJj { get; set; }
        //购物币
        public virtual double? agentGw { get; set; }
        //复投币
        public virtual double? agentFt { get; set; }
        //累计奖金
        public virtual double? agentTotal { get; set; } 

        //冗余
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
       
    }
}
